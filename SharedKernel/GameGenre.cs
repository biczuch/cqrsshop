﻿using System.ComponentModel;

namespace SharedKernel
{
    public enum GameGenre
    {
        [Description("Action")]
        Action = 1 << 0,
        [Description("Action-Adventure")]
        ActionAdventure = 1 << 1,
        [Description("Adventure")]
        Adventure = 1 << 2,
        [Description("RPG")]
        Rpg = 1 << 3,
        [Description("MMORPG")]
        MmoRpg = 1 << 4,
        [Description("Simulation")]
        Simulation = 1 << 5,
        [Description("Strategy")]
        Strategy = 1 << 6,
        [Description("Sports")]
        Sports = 1 << 7,
        [Description("FPS")]
        Fps = 1 << 8,
        [Description("TPP")]
        Tpp = 1 << 9,
        [Description("RTS")]
        Rts = 1 << 10,
        [Description("Turn based game")]
        TurnBased = 1 << 11,
        [Description("Racing")]
        Racing = 1 << 12,
        [Description("Music game")]
        MusicGame = 1 << 13
    }
}
