﻿using System;

namespace SharedKernel
{
    public static class EndpointNames
    {
        public const string ORDERS_MANAGEMENT_WRITE = "OrdersManagementWrite";
        public const string ORDERS_MANAGMENT_READ = "OrdersManagementRead";
        public const string PRODUCT_MANAGEMENT = "ProductManagement";
        public const string ORDER_SAGAS_MANAGER = "OrderSagasManager";

        public const string STANDARD_ARCHITECTURE_EVENT_RECEIVER = "StandardArchitectureEventReceiver";
        public const string STANDARD_ARCHITECTURE_ORDERS_MANAGEMENT = "StandardArchitectureOrdersManagement";
    }
}
