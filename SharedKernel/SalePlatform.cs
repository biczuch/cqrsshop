﻿namespace SharedKernel
{
    public enum SalePlatform
    {
        Steam,
        Origin,
        UPlay,
        GoG
    }
}
