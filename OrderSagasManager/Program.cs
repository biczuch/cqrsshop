﻿using Automatonymous;
using MassTransit;
using MassTransit.EntityFrameworkIntegration;
using MassTransit.EntityFrameworkIntegration.Saga;
using MassTransit.RabbitMqTransport;
using OrderSagaManager.Persistance.Mappings;
using OrderSagaManager.Sagas;
using OrderSagaManager.Sagas.SagaInstances;
using System;
using System.Configuration;
using Topshelf;

namespace OrderSagasManager
{
    class Program
    {
        static int Main(string[] args)
        {
            TopshelfExitCode exitCode = HostFactory.Run(cfg => cfg.Service(x => new EventConsumerService()));
            return (int)exitCode;
        }
    }

    public class EventConsumerService : ServiceControl
    {
        IBusControl _bus;

        public object EndpointNames { get; private set; }

        public bool Start(HostControl hostControl)
        {
            _bus = ConfigureBus();
            _bus.Start();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _bus?.Stop(TimeSpan.FromSeconds(30));
            return true;
        }

        private IBusControl ConfigureBus()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrderSagas"].ConnectionString;
            SagaDbContextFactory contextFactory = () =>  new SagaDbContext<OrderSagaInstance, OrderSagaInstanceMap>(connectionString);
            var repository = new EntityFrameworkSagaRepository<OrderSagaInstance>(contextFactory, optimistic: false);

            var orderStateMachine = new OrderStateMachine();

            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                IRabbitMqHost host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.ReceiveEndpoint(host, SharedKernel.EndpointNames.ORDER_SAGAS_MANAGER, e =>
                {

                    e.PrefetchCount = 8;
                    e.StateMachineSaga(orderStateMachine, repository);
                });

                cfg.UseInMemoryScheduler();
            });
        }
    }
}
