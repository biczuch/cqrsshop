﻿using System;
using Automatonymous;

namespace OrderSagaManager.Sagas.SagaInstances
{
    public class OrderSagaInstance : SagaStateMachineInstance
    {
        public OrderSagaInstance(Guid correlationId)
        {
            CorrelationId = correlationId;
        }

        protected OrderSagaInstance() { }

        public Guid OrderId { get; set; }
        public decimal AmountToPay { get; set; }
        public decimal AmountPaid { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string CurrentState { get; set; }
        public Guid CorrelationId { get; set; }
        public Guid? ExpirationId { get; set; }
        public byte[] RowVersion { get; set; } // For Optimistic Concurrency

        public bool IsPaid
        {
            get
            {
                return AmountPaid >= AmountToPay;
            }
        }
    }
}