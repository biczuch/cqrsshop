﻿using Automatonymous;
using Orders.Contracts.Order;
using OrderSagaManager.Sagas.SagaInstances;
using System;

namespace OrderSagaManager.Sagas
{
    public class OrderStateMachine : MassTransitStateMachine<OrderSagaInstance>
    {
        public State Active { get; private set; }

        public Event<OrderSubmittedEvent> OrderSubmittedEvent { get; private set; }
        public Event<PaymentMadeEvent> PaymentMadeEvent { get; private set; }
        public Schedule<OrderSagaInstance, OrderExpiredEvent> OrderExpiredEvent { get; private set; }

        public OrderStateMachine()
        {
            InstanceState(x => x.CurrentState);
            Event(
                () => OrderSubmittedEvent,
                x => x.CorrelateById(cart => cart.CorrelationId, context => context.Message.OrderId).SelectId(context => context.Message.OrderId));

            Event(
                () => PaymentMadeEvent,
                x => x.CorrelateById(cart => cart.Message.OrderId));

            Schedule(
                () => OrderExpiredEvent,
                x => x.ExpirationId,
                x =>
                {
                    x.Delay = TimeSpan.FromMinutes(1);
                    x.Received = e => e.CorrelateById(context => context.Message.OrderId);
                });

            Initially(
                When(OrderSubmittedEvent)
                    .Then(context =>
                    {
                        context.Instance.OrderId = context.Data.OrderId;
                        context.Instance.CreatedDate = context.Data.SubmittedDate;
                        context.Instance.UpdatedDate = DateTime.Now;
                        context.Instance.AmountToPay = context.Data.TotalAmount;
                    })
                    .ThenAsync(context => Console.Out.WriteLineAsync($"Order {context.Data.OrderId} placed for {context.Data.TotalAmount}"))
                    .Schedule(OrderExpiredEvent, context => new OrderExpiredEvent() { OrderId = context.Instance.OrderId })
                    .TransitionTo(Active));

            During(Active,
                When(PaymentMadeEvent)
                .Then(context => {
                    context.Instance.UpdatedDate = DateTime.Now;
                    context.Instance.AmountPaid += context.Data.PaymentAmount;
                })
                .If(context => context.Instance.IsPaid, context => context
                    .ThenAsync(x => Console.Out.WriteLineAsync($"Order {x.Data.OrderId} fully paid ({x.Instance.AmountPaid})"))
                    .Unschedule(OrderExpiredEvent)
                    .Publish(x => new OrderFullyPaidEvent() { OrderId = x.Instance.OrderId })
                    .Finalize())
                .If(context => !context.Instance.IsPaid, context => context
                    .ThenAsync(x => Console.Out.WriteLineAsync($"Order {x.Data.OrderId} paid for {x.Instance.AmountPaid}. " +
                            $"Remaining: {x.Instance.AmountToPay - x.Instance.AmountPaid}"))
                    ));

            During(Active,
               When(OrderExpiredEvent.Received)
               .ThenAsync(context => Console.Out.WriteLineAsync($"Order { context.Instance.OrderId } expired."))
               .Unschedule(OrderExpiredEvent)
               .Publish(context => new OrderExpiredEvent {
                   OrderId = context.Instance.OrderId
               })
               .Finalize());

           SetCompletedWhenFinalized();
        }
    }
}
