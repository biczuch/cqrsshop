﻿using MassTransit.EntityFrameworkIntegration;
using OrderSagaManager.Sagas.SagaInstances;

namespace OrderSagaManager.Persistance.Mappings
{
    class OrderSagaInstanceMap : SagaClassMapping<OrderSagaInstance>
    {
        public OrderSagaInstanceMap()
        {
            Property(x => x.CurrentState);
            Property(x => x.OrderId);
            Property(x => x.CreatedDate);
            Property(x => x.AmountToPay);
            Property(x => x.UpdatedDate);
            Property(x => x.AmountPaid);
            Property(x => x.ExpirationId);
            Property(x => x.RowVersion).IsRowVersion(); // For Optimistic Concurrency
        }
    }
}
