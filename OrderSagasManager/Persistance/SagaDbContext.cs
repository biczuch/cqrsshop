﻿using OrderSagaManager.Sagas.SagaInstances;
using System.Data.Entity;

namespace OrderSagasManager.Persistance
{
    class SagaDbContext : DbContext
    {
        public virtual DbSet<OrderSagaInstance> OrderSagas { get; set; }
    }
}
