﻿using System.Threading.Tasks;
using MassTransit;
using ProductManagement.Core.Repositories;
using ProductManagement.Core;
using Orders.Contracts.Product;

namespace ProductManagementService.Event_handlers
{
    class ProductCodeSoldEventHandler : IConsumer<ProductCodeSoldEvent>
    {
        ProductRepository _repository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductCodeSoldEvent> context)
        {
            var message = context.Message;

            Product product = _repository.GetById(message.ProductId);

            product.MarkCodeAsSold(message.CodeId, message.SoldPrice, message.MarginPercentage);

            _repository.Save(product);

            return Task.CompletedTask;
        }
    }
}
