﻿using MassTransit;
using ProductManagement.Core.Persistance;
using ProductManagementService.Event_handlers;
using SharedKernel;
using System;
using System.Configuration;
using Topshelf;

namespace ProductManagementService
{
    class Program
    {
        static int Main(string[] args)
        {
            TopshelfExitCode exitCode = HostFactory.Run(cfg => cfg.Service(x => new EventConsumerService()));
            return (int)exitCode;
        }
    }

    public class EventConsumerService : ServiceControl
    {
        IBusControl _bus;

        public bool Start(HostControl hostControl)
        {
            var productManagementConfig = ConfigurationManager.ConnectionStrings["ProductManagement"];

            ProductManagementSessionFactory.GetInstance(productManagementConfig.ConnectionString);

            _bus = ConfigureBus();
            _bus.Start();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _bus?.Stop(TimeSpan.FromSeconds(30));
            return true;
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
           {
               var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
               {
                   h.Username("guest");
                   h.Password("guest");
               });

               cfg.ReceiveEndpoint(host, EndpointNames.PRODUCT_MANAGEMENT, e =>
               {
                   e.Consumer<ProductCodeSoldEventHandler>();
               });
           });
        }
    }
}
