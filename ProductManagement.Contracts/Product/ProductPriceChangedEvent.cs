﻿namespace ProductManagement.Contracts.Product
{
    public class ProductPriceChangedEvent : ProductEvent
    {
        public decimal UnitPrice { get; set; }
        public decimal MarginPercentage { get; set; }
    }
}
