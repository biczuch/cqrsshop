﻿using DddCommon;
using System;

namespace ProductManagement.Contracts.Product
{
    public abstract class ProductEvent : IDomainEvent
    {
        public Guid ProductId { get; set; }
    }
}
