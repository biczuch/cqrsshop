﻿using SharedKernel;
using System.Collections.Generic;

namespace ProductManagement.Contracts.Product
{
    public class ProductInformationChangedEvent : ProductEvent
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public GameGenre GameGenre { get; set; }
        public IEnumerable<string> ImageUris { get; set; }
    }
}
