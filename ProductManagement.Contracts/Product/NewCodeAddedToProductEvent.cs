﻿using System;

namespace ProductManagement.Contracts.Product
{
    public class NewCode
    {
        public Guid CodeId { get; set; }
        public string Code { get; set; }
    }


    public class NewCodeAddedToProductEvent : ProductEvent
    {
        public NewCode[] NewCodes { get; set; }
    }
}
