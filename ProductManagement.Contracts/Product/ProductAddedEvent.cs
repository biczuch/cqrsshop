﻿using SharedKernel;
using System.Collections.Generic;

namespace ProductManagement.Contracts.Product
{
    public class ProductAddedEvent : ProductEvent
    {
        public string ProductName { get; set; }
        public string Description { get; set; }

        public string ShortDescription { get; set; }

        public SalePlatform SalePlatform { get; set; }
        public GameGenre GameGenre { get; set; }

        public decimal UnitPrice { get; set; }
        public decimal MarginPercentage { get; set; }

        public IEnumerable<string> ImageUris { get; set; }
        public bool IsActive { get; set; }
    }
}
