﻿using System;

namespace Orders.Commands.Cart
{
    public class ChangeProductQuantity : Command
    {
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        public int NewQuantity { get; set; }
    }
}
