﻿using System;

namespace Orders.Commands.Cart
{
    public class CheckoutCart : Command
    {
        public Guid CartId { get; set; }
    }
}
