﻿using System;

namespace Orders.Commands.Cart
{
    public class RemoveItemFromCart : Command
    {
        public Guid CartGuid { get; set; }
        public Guid ProductId { get; set; }
    }
}
