﻿using System;

namespace Orders.Commands.Cart
{
    public class AddItemToCart : Command
    {
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
        public int Quantity { get; set; }
    }
}
