﻿using System;

namespace Orders.Commands.Cart
{
    public class DisableCart : Command
    {
        public Guid CartId { get; set; }
    }
}
