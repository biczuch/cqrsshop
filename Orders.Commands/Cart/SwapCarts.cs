﻿using System;

namespace Orders.Commands.Cart
{
    public class SwapCarts : Command
    {
        public Guid CartToRemove { get; set; }
        public Guid CartToAssign { get; set; }
        public Guid NewUserId { get; set; }
    }
}
