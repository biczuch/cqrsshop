﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Order;

namespace QueryRepository.Mappers.Order
{
    class OrderListEntryProductMapper : ClassMap<OrderListEntryProduct>
    {
        public OrderListEntryProductMapper()
        {
            Table("OrderListEntryProducts");

            Id(x => x.Id);

            Map(x => x.ProductName);
            Map(x => x.Quantity);

            References(x => x.OrderListEntry);
        }
    }
}
