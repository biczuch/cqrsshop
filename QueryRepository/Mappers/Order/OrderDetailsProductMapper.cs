﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Order;

namespace QueryRepository.Mappers.Order
{
    class OrderDetailsProductMapper : ClassMap<OrderDetailsProduct>
    {
        public OrderDetailsProductMapper()
        {
            Id(m => m.Id);

            Map(m => m.ProductGuid);
            Map(m => m.ProductName);
            Map(m => m.ThumbnailUri).Nullable();

            References(m => m.OrderDetails);

            HasMany<string>(m => m.Codes)
                .KeyColumn("Id")
                .Table("OrderDetailsProductCodes")
                .Element("Code", x => x.Length(4001))
                .Not.LazyLoad(); 
        }
    }
}
