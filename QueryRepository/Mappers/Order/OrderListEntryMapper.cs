﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Order;

namespace QueryRepository.Mappers.Order
{
    class OrderListEntryMapper : ClassMap<OrderListEntry>
    {
        public OrderListEntryMapper()
        {
            Table("OrderListEntries");

            Id(m => m.Id);
            Map(m => m.OrderGuid);
            Map(m => m.OrderTotalPrice);
            Map(m => m.Status);
            Map(m => m.UserId);
            Map(m => m.DatePlaced).Nullable();
            Map(m => m.DateCompleted).Nullable();
            Map(m => m.IsCancelled);

            HasMany<OrderListEntryProduct>(m => m.OrderListEntryProducts)
                .Cascade.All();
        }
    }
}
