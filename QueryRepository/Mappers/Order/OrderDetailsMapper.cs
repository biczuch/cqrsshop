﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Order;

namespace QueryRepository.Mappers.Order
{
    class OrderDetailsMapper : ClassMap<OrderDetails>
    {
        public OrderDetailsMapper()
        {
            Id(m => m.Id);
            Map(m => m.OrderGuid);
            Map(m => m.UserId);
            Map(m => m.TotalPrice);
            Map(m => m.Status);
            Map(m => m.DateCompleted).Nullable();
            Map(m => m.DatePlaced).Nullable(); ;

            HasMany<OrderDetailsProduct>(m => m.OrderDetailsProducts)
                .Cascade.All();
        }
    }
}
