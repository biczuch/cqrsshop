﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Product;

namespace QueryRepository.Mappers.Product
{
    public class ProductDetailsMapper : ClassMap<ProductDetails>
    {
        public ProductDetailsMapper()
        {
            Table("ProductDetails");

            Id(m => m.Id);
            Map(m => m.ProductGuid);
            Map(m => m.ProductName);
            Map(m => m.Description).Length(4001).Nullable();
            Map(m => m.SalePlatform);
            Map(m => m.GameGenre);
            Map(m => m.Price);
            
            HasMany<string>(m => m.ImageUris)
                .KeyColumn("ImageId")
                .Table("ImageUri")
                .Element("Uri", x => x.Length(4001))
                .Not.LazyLoad();
        }
    }
}
