﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Product;
using SharedKernel;

namespace QueryRepository.Mappers.Product
{
    public class ProductListEntryMapper : ClassMap<ProductListEntry>
    {
        public ProductListEntryMapper()
        {
            Table("ProductList");

            Id(m => m.ProductListEnryId);
            Map(m => m.ProductGuid);
            Map(m => m.Name);
            Map(m => m.ShortDescription).Length(4001).Nullable();
            Map(m => m.SalePlatform).CustomType(typeof(SalePlatform));
            Map(m => m.GameGenre).CustomType(typeof(GameGenre));
            Map(m => m.Price);
            Map(m => m.IsActive);
            Map(m => m.ThumbnailUri).Length(4001).Nullable();
        }
    }
}
