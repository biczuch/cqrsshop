﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Cart;

namespace QueryRepository.Mappers.Cart
{
    class CartItemMapper : ClassMap<CartItem>
    {
        public CartItemMapper()
        {
            Id(m => m.Id);
            Map(m => m.ProductGuid);
            Map(m => m.ProductName).Length(4001);
            Map(m => m.Quantity);
            Map(m => m.Price);
        }
    }
}
