﻿using FluentNHibernate.Mapping;
using QueryRepository.Model.Cart;

namespace QueryRepository.Mappers.Cart
{
    class CartDetailsMapper : ClassMap<CartDetails>
    {
        public CartDetailsMapper()
        {
            Id(m => m.Id);
            Map(m => m.CartGuid);
            Map(m => m.UserGuid);

            HasMany(x => x.CartItems)
                .Cascade.All()
                .Not.LazyLoad();
        }
    }
}
