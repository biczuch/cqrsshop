﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.SqlCommand;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Diagnostics;
using System.Reflection;

namespace QueryRepository
{
    public class QueryRepositorySessionFactory
    {
        static readonly object factoryLock = new object();

        private static ISessionFactory _factory;

        public static ISessionFactory GetInstance(string connectionString = null)
        {
            if (_factory == null)
            {
                lock (factoryLock)
                {
                    if (_factory == null)
                    {
                        if (connectionString == null)
                            throw new ArgumentException("Connection string passed to GetInstance method " +
                                "should be not null when initializing SessionFactory for the first time.");
                        _factory = BuildSessionFactory(connectionString);
                    }
                }
            }

            return _factory;
        }

        private static ISessionFactory BuildSessionFactory(string connectionString)
        {
            FluentConfiguration configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(connectionString).ShowSql())
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())
                    .Conventions.Add(
                            ForeignKey.EndsWith("ID"),
                            ConventionBuilder.Property
                                .When(criteria => criteria
                                    .Expect(x => x.Nullable, Is.Not.Set), x => x.Not.Nullable()))
                )
                .ExposeConfiguration(cfg =>
                {
                    cfg.SetInterceptor(new SqlInterceptor());
                    new SchemaUpdate(cfg).Execute(false, true);
                });

            return configuration.BuildSessionFactory();
        }

        public class SqlInterceptor : EmptyInterceptor
        {
            public override SqlString OnPrepareStatement(SqlString sql)
            {
                Trace.WriteLine(sql.ToString());
                return sql;
            }
        }
    }
}
