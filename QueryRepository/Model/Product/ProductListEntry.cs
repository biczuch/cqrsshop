﻿using SharedKernel;
using System;

namespace QueryRepository.Model.Product
{
    public class ProductListEntry
    {
        public virtual int ProductListEnryId { get; set; }
        public virtual Guid ProductGuid { get; set; }
        public virtual string Name { get; set; }
        public virtual string ShortDescription { get; set; }
        public virtual GameGenre GameGenre { get; set; }
        public virtual SalePlatform SalePlatform { get; set; }
        public virtual decimal Price { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string ThumbnailUri { get; set; }
    }
}
