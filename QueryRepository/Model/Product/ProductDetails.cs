﻿using SharedKernel;
using System;
using System.Collections.Generic;

namespace QueryRepository.Model.Product
{
    public class ProductDetails
    {
        public virtual int Id { get; set; }
        public virtual Guid ProductGuid { get; set; }
        public virtual string ProductName { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Price { get; set; }
        public virtual GameGenre GameGenre { get; set; }
        public virtual SalePlatform SalePlatform { get; set; }
        public virtual ICollection<string> ImageUris { get; set; }
    }
}
