﻿using System;

namespace QueryRepository.Model.Cart
{
    public class CartItem
    {
        public virtual int Id { get; set; }
        public virtual string ProductName { get; set; }
        public virtual Guid ProductGuid { get; set; }
        public virtual decimal Price { get; set; }
        public virtual int Quantity { get; set; }
    }
}
