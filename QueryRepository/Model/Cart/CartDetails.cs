﻿using System;
using System.Collections.Generic;

namespace QueryRepository.Model.Cart
{
    public class CartDetails
    {
        public virtual int Id { get; set; }
        public virtual Guid CartGuid { get; set; }
        public virtual Guid UserGuid { get; set; }
        public virtual ICollection<CartItem> CartItems { get; set; }
    }
}
