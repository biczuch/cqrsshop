﻿using System;
using System.Collections.Generic;

namespace QueryRepository.Model.Order
{
    public class OrderListEntry
    {
        public virtual int Id { get; set; }

        public virtual Guid OrderGuid { get; set; }

        public virtual DateTime? DatePlaced { get; set; }

        public virtual DateTime? DateCompleted { get; set; }

        public virtual decimal OrderTotalPrice {get;set;}

        public virtual string Status { get; set; }

        public virtual Guid UserId { get; set; }

        public virtual bool IsCancelled { get; set; }

        public virtual IList<OrderListEntryProduct> OrderListEntryProducts { get; set; }
    }
}
