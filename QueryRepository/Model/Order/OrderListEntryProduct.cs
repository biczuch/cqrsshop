﻿using System;

namespace QueryRepository.Model.Order
{
    public class OrderListEntryProduct
    {
        public virtual int Id { get; set; }

        public virtual Guid ProductGuid { get; set; }
        public virtual int Quantity { get; set; }
        public virtual string ProductName { get; set; }

        public virtual OrderListEntry OrderListEntry { get; set; }
    }
}
