﻿using System;
using System.Collections.Generic;

namespace QueryRepository.Model.Order
{
    public class OrderDetailsProduct
    {
        public virtual int Id { get; set; }

        public virtual Guid ProductGuid { get; set; }

        public virtual string ProductName { get; set; }

        public virtual IList<string> Codes { get; set; }

        public virtual string ThumbnailUri { get; set; }

        public virtual OrderDetails OrderDetails { get; set; }
    }
}
