﻿using MassTransit;
using Orders.Contracts.Order;
using Orders.Core.Services;
using System.Threading.Tasks;

namespace OrdersManagementWriteService.EventHandlers.OrdersEventHandlers
{
    class OrderExpiredEventHandler : IConsumer<OrderExpiredEvent>
    {
        OrderService _orderService = new OrderService();

        public Task Consume(ConsumeContext<OrderExpiredEvent> context)
        {
            var message = context.Message;

            _orderService.CancelOrder(message.OrderId);

            return Task.CompletedTask;
        }
    }
}
