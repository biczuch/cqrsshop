﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Contracts.Order;
using Orders.Core.Services;

namespace OrdersManagementWriteService.EventHandlers.OrdersEventHandlers
{
    public class OrderFullyPaidEventHandler : IConsumer<OrderFullyPaidEvent>
    {
        OrderService _orderService = new OrderService();

        public Task Consume(ConsumeContext<OrderFullyPaidEvent> context)
        {
            _orderService.CompleteOrder(context.Message.OrderId);

            return Task.CompletedTask;
        }
    }
}
