﻿using MassTransit;
using Orders.Core.Repositories;
using ProductManagement.Contracts.Product;
using System;
using System.Threading.Tasks;

namespace OrdersManagementWriteService.EventHandlers.ProductEventHandlers
{
    public class ProductDeactivatedEventHandler : IConsumer<ProductDeactivatedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductDeactivatedEvent> context)
        {
            var message = context.Message;

            var product = productRepository.GetById(message.ProductId);
            product.DeactivateProduct();

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}