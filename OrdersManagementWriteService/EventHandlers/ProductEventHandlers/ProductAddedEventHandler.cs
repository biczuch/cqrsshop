﻿using System.Threading.Tasks;
using ProductManagement.Contracts.Product;
using Orders.Core;
using Orders.Core.Repositories;
using MassTransit;
using System;

namespace OrdersManagementWriteService.EventHandlers.ProductEventHandlers
{
    public class ProductAddedEventHandler : IConsumer<ProductAddedEvent>
    {
        private ProductRepository _productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductAddedEvent> context)
        {
            var message = context.Message;

            Product product = new Product(
                message.ProductId,
                message.ProductName,
                message.SalePlatform,
                message.ShortDescription,
                message.Description,
                message.UnitPrice,
                message.MarginPercentage,
                message.ImageUris,
                message.GameGenre,
                message.IsActive
                );

            _productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}