﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Core;
using Orders.Core.Repositories;
using ProductManagement.Contracts.Product;
using System.Linq;

namespace OrdersManagementWriteService.EventHandlers.ProductEventHandlers
{
    public class NewCodeAddedToProductEventHandler : IConsumer<NewCodeAddedToProductEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<NewCodeAddedToProductEvent> context)
        {
            var message = context.Message;
            
            Product product = productRepository.GetById(message.ProductId);
            var newCodes = message.NewCodes
                .Select(c => new Orders.Core.Model.ProductCode(c.CodeId, c.Code))
                .ToArray();

            product.AddNewCodes(newCodes);

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}