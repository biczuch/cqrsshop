﻿using System.Threading.Tasks;
using ProductManagement.Contracts.Product;
using Orders.Core.Repositories;
using DddCommon.CommonValueObjects;
using MassTransit;

namespace OrdersManagementWriteService.EventHandlers.ProductEventHandlers
{
    public class ProductPriceChangedEventHandler : IConsumer<ProductPriceChangedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductPriceChangedEvent> context)
        {
            var message = context.Message;

            var product = productRepository.GetById(message.ProductId);
            product.ChangePrice(new Money(message.UnitPrice), message.MarginPercentage);

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}