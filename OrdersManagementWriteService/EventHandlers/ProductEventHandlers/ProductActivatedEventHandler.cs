﻿
using MassTransit;
using Orders.Core.Repositories;
using ProductManagement.Contracts.Product;
using System;
using System.Threading.Tasks;

namespace OrdersManagementWriteService.EventHandlers.ProductEventHandlers
{
    public class ProductActivatedEventHandler : IConsumer<ProductActivatedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductActivatedEvent> context)
        {
            var message = context.Message;

            var product = productRepository.GetById(message.ProductId);
            product.ActivateProduct();

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}