﻿using System.Threading.Tasks;
using ProductManagement.Contracts.Product;
using Orders.Core.Repositories;
using MassTransit;

namespace OrdersManagementWriteService.EventHandlers.ProductEventHandlers
{
    public class ProductInformationChangedEventHandler : IConsumer<ProductInformationChangedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductInformationChangedEvent> context)
        {
            var message = context.Message;

            var product = productRepository.GetById(message.ProductId);
            product.ChangeInformation(
                message.ProductName,
                message.GameGenre,
                message.Description,
                message.ShortDescription,
                message.ImageUris
                );

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}