﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Core.Services;
using Orders.Commands.Cart;

namespace OrdersManagementWriteService.CommandHandlers.CartCommandHandlers
{
    class ChangeItemQuantityCommandHandler : IConsumer<ChangeProductQuantity>
    {
        CartService _cartService = new CartService();

        public Task Consume(ConsumeContext<ChangeProductQuantity> context)
        {
            var message = context.Message;

            _cartService.ChangeProductQuantityInCart(message.CartId, message.ProductId, message.NewQuantity);

            return Task.CompletedTask;
        }
    }
}
