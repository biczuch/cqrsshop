﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Commands.Cart;
using Orders.Core.Services;

namespace OrdersManagementWriteService.CommandHandlers.CartCommandHandlers
{
    class CheckoutCommandHandler : IConsumer<CheckoutCart>
    {
        OrderService _orderService = new OrderService();

        public Task Consume(ConsumeContext<CheckoutCart> context)
        {
            var message = context.Message;

            _orderService.CheckoutCart(message.CartId);

            return Task.CompletedTask;
        }
    }
}
