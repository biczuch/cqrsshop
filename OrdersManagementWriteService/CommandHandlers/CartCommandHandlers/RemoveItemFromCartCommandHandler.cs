﻿using MassTransit;
using Orders.Commands.Cart;
using Orders.Core.Services;
using System.Threading.Tasks;

namespace OrdersManagementWriteService.CommandHandlers.CartCommandHandlers
{
    class RemoveItemFromCartCommandHandler : IConsumer<RemoveItemFromCart>
    {
        CartService _cartService = new CartService();

        public Task Consume(ConsumeContext<RemoveItemFromCart> context)
        {
            var message = context.Message;

            _cartService.RemoveItemFromCart(message.CartGuid, context.Message.ProductId);

            return Task.CompletedTask;
        }
    }
}
