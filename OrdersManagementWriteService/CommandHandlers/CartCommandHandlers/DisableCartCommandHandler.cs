﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Commands.Cart;
using Orders.Core.Services;

namespace OrdersManagementWriteService.CommandHandlers.CartCommandHandlers
{
    class DisableCartCommandHandler : IConsumer<DisableCart>
    {
        CartService _cartService = new CartService();

        public Task Consume(ConsumeContext<DisableCart> context)
        {
            var message = context.Message;

            _cartService.DisableCart(message.CartId);

            return Task.CompletedTask;
        }
    }
}
