﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Commands.Cart;
using Orders.Core.Repositories;
using Orders.Core.Services;
using System;

namespace OrdersManagementWriteService.CommandHandlers.CartCommandHandlers
{
    public class AddItemToCartCommandHandler : IConsumer<AddItemToCart>
    {
        CartRepository _cartRepository = new CartRepository();
        CartService _cartService = new CartService();

        public Task Consume(ConsumeContext<AddItemToCart> context)
        {
            var message = context.Message;

            _cartService.AddProductToCart(message.CartId, message.UserId, message.ProductId, message.Quantity);

            return Task.CompletedTask;
        }
    }
}
