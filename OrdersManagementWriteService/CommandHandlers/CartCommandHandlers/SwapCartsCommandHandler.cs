﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Commands.Cart;
using Orders.Core.Services;

namespace OrdersManagementWriteService.CommandHandlers.CartCommandHandlers
{
    class SwapCartsCommandHandler : IConsumer<SwapCarts>
    {
        CartService _cartService = new CartService();

        public Task Consume(ConsumeContext<SwapCarts> context)
        {
            var message = context.Message;

            _cartService.SwapCarts(message.CartToRemove, 
                message.CartToAssign, 
                message.NewUserId);

            return Task.CompletedTask;
        }
    }
}
