﻿using DddCommon;
using GreenPipes;
using MassTransit;
using OrdersManagementWriteService.CommandHandlers.CartCommandHandlers;
using OrdersManagementWriteService.EventHandlers.OrdersEventHandlers;
using OrdersManagementWriteService.EventHandlers.ProductEventHandlers;
using SharedKernel;
using System;
using Topshelf;

namespace OrdersManagementWriteService
{
    class Program
    {
        static int Main(string[] args)
        {
            TopshelfExitCode exitCode = HostFactory.Run(cfg => cfg.Service(x => new EventConsumerService()));
            return (int)exitCode;
        }
    }

    public class EventConsumerService : ServiceControl
    {
        IBusControl _bus;

        public bool Start(HostControl hostControl)
        {
            _bus = ConfigureBus();
            
            _bus.Start();
            DomainEvent.Dispatcher = new OrdersManagementWriteServiceEventDispatcher(_bus);
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _bus?.Stop(TimeSpan.FromSeconds(30));
            return true;
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
               
                cfg.ReceiveEndpoint(host, EndpointNames.ORDERS_MANAGEMENT_WRITE, e =>
                {
                    e.UseConcurrencyLimit(1);
                    e.PrefetchCount = 1;

                    e.Consumer<NewCodeAddedToProductEventHandler>();
                    e.Consumer<ProductActivatedEventHandler>();
                    e.Consumer<ProductAddedEventHandler>();
                    e.Consumer<ProductDeactivatedEventHandler>();
                    e.Consumer<ProductInformationChangedEventHandler>();
                    e.Consumer<ProductPriceChangedEventHandler>();

                    e.Consumer<AddItemToCartCommandHandler>();
                    e.Consumer<RemoveItemFromCartCommandHandler>();
                    e.Consumer<ChangeItemQuantityCommandHandler>();
                    e.Consumer<DisableCartCommandHandler>();
                    e.Consumer<SwapCartsCommandHandler>();
                    e.Consumer<CheckoutCommandHandler>();

                    e.Consumer<OrderFullyPaidEventHandler>();
                    e.Consumer<OrderExpiredEventHandler>();
                });
            });
        }
    }
}
