﻿using System;

namespace DddCommon.CommonValueObjects
{
    public class Money : ValueObject<Money>
    {
        public const Currency DEFAULT_CURRENCY = Currency.EUR;

        public decimal Amount { private set; get; }
        public Currency Currency { private set; get; }

        private Money()
        {
            Amount = decimal.Zero;
            Currency = DEFAULT_CURRENCY;
        }

        public Money(decimal amount) : this()
        {
            if (amount < 0)
                throw new ArgumentException("Amount of money can't be negative.");

            Amount = RoundMoney(amount);
        }

        public Money(decimal amount, Currency currency) : this(amount)
        {
            Currency = currency;
        }

        protected override bool EqualsCore(Money other)
        {
            if (Currency == other.Currency)
                return Amount == other.Amount;

            return false;
        }

        public static Money operator +(Money money1, Money money2)
        {
            if(money1.Currency == money2.Currency)
                return new Money(money1.Amount + money2.Amount, money1.Currency);

            throw new InvalidOperationException("Can't add Money of different currencies.");
        }

        public static Money operator +(Money money1, decimal money2)
        {
            var amount = RoundMoney(money1.Amount + money2);
            return new Money(amount, money1.Currency);
        }

        public static Money operator -(Money money1, Money money2)
        {
            if (money1.Currency == money2.Currency)
            {
                var amount = money1.Amount - money2.Amount;
                if (amount < 0)
                    throw new InvalidOperationException("The result of Money subtraction is negative.");
                return new Money(amount, money1.Currency);
            }

            throw new InvalidOperationException("Can't add Money of different currencies.");
        }

        public static Money operator *(Money money1, int x)
        {
            var amount = decimal.Round(money1.Amount * x);
            return new Money(amount, money1.Currency);
        }

        private static decimal RoundMoney(decimal amount)
        {
            return decimal.Round(amount, 2, MidpointRounding.AwayFromZero);
        }

        protected override int GetHashCodeCore()
        {
            throw new NotImplementedException();
        }
    }
}
