﻿namespace DddCommon.CommonValueObjects
{
    public enum Currency
    {
        EUR,
        USD,
        PLN,
        GBP
    }
}
