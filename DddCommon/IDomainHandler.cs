﻿namespace DddCommon
{
    public interface IDomainHandler<T> where T:IDomainEvent
    {
        void Handle(T @event);
    }
}
