﻿namespace DddCommon
{
    public interface IRootAware
    {
        Entity RootEntity { get; set; }
    }
}
