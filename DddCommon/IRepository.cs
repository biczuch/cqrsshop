﻿using System;

namespace DddCommon
{
    public interface IRepository<T> where T: AggregateRoot
    {
        T GetById(Guid guid);
        void Save(T aggregateRoot);
        
    }
}