﻿using NHibernate;
using System;

namespace DddCommon
{
    public abstract class Repository<T> : IRepository<T>
        where T : AggregateRoot
    {
        protected ISessionFactory _sessionFactory;

        protected Repository()
        {
            _sessionFactory = GetSessionFactory();
            if (_sessionFactory == null)
                throw new NotImplementedException("You need to implement GetSessionFactory" +
                    "before using repository.");
        }

        protected abstract ISessionFactory GetSessionFactory();

        public T GetById(Guid guid)
        {
            using (ISession session = _sessionFactory.OpenSession())
            {
                return session.Get<T>(guid);
            }
        }

        public T GetByIdManually(Guid guid, ISession session)
        {
            return session.Get<T>(guid);
        }

        public ISession GetOpenedSession()
        {
            return _sessionFactory.OpenSession();
        }

        public void Save(T aggregateRoot)
        {
            using (ISession session = _sessionFactory.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.SaveOrUpdate(aggregateRoot);
                transaction.Commit();
            }
        }

        public void SaveManually(T aggregateRoot, ISession session)
        {
            session.SaveOrUpdate(aggregateRoot);
            session.Flush();
        }
    }
}
