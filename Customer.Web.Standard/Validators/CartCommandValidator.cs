﻿using System.Linq;
using System;
using NHibernate;
using Orders.Core.Persistance;
using Orders.Core;
using Orders.Core.Model;

namespace Customer.Web.Standard.Validators
{
    public class CartValidator
    {
        ISessionFactory _queryRepository = OrdersSessionFactory.GetInstance();

        internal ValidationResult ValidateAddProductToCart(Guid productId)
        {
            Product product;
            using (var session = _queryRepository.OpenSession())
            {
                product = session.QueryOver<Product>()
                    .Where(p => p.Id == productId)
                    .List()
                    .First();
            }

            var commandValidatorResult = new ValidationResult()
            {
                IsValid = true
            };

            if (product == null)
            {
                commandValidatorResult.IsValid = false;
                commandValidatorResult.ErrorMessage = "Product not found.";
            }

            if (product.IsActive == false)
            {
                commandValidatorResult.IsValid = false;
                commandValidatorResult.ErrorMessage = "Product can't be sold.";
            }

            return commandValidatorResult;
        }

        internal ValidationResult ValidateRemoveFromCart(Guid cartId, Guid productId)
        {
            using (var session = _queryRepository.OpenSession())
            {
                var cart = session.QueryOver<Cart>()
                    .Where(c => c.Id == cartId)
                    .SingleOrDefault();

                if (cart == null)
                {
                    return new ValidationResult()
                    {
                        IsValid = false,
                        ErrorMessage = "Cart not found"
                    };
                }

                if (!cart.Items.Any(c => c.ProductId == productId))
                {
                    return new ValidationResult()
                    {
                        IsValid = false,
                        ErrorMessage = "Product is not in cart."
                    };
                }
            }

            return ValidationResult.Success;
        }

        internal ValidationResult ValidateRemoveCart(Guid cartId, Guid userGuid)
        {

            using (var session = _queryRepository.OpenSession())
            {
                var cart = session.QueryOver<Cart>()
                    .Where(c => c.Id == cartId)
                    .SingleOrDefault();

                if (cart == null)
                {
                    return new ValidationResult()
                    {
                        ErrorMessage = "Cart not found"
                    };
                }

                if(cart.UserId != userGuid)
                {
                    return new ValidationResult()
                    {
                        ErrorMessage = "Bad user"
                    };
                }
            };

            return ValidationResult.Success;
        }

        internal ValidationResult ValidateCheckOut(Guid cartId, Guid userId)
        {
            using (var session = _queryRepository.OpenSession())
            {
                var cart = session.QueryOver<Cart>()
                    .Where(c => c.Id == cartId)
                    .SingleOrDefault();

                if (cart == null)
                {
                    return new ValidationResult()
                    {
                        ErrorMessage = "Cart not found"
                    };
                }

                if (cart.UserId != userId)
                {
                    return new ValidationResult()
                    {
                        ErrorMessage = "Bad user"
                    };
                }
            };

            return ValidationResult.Success;
        }
    }
}