﻿namespace Customer.Web.Standard.Validators
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }

        public static ValidationResult Success
        {
            get
            {
                return new ValidationResult()
                {
                    IsValid = true
                };
            }
        }
    }
}