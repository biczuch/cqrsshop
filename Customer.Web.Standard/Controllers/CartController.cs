﻿using System;
using System.Web.Mvc;
using Customer.Web.Standard.Models;
using Customer.Web.Standard.Models.Cart;
using System.Threading.Tasks;
using Customer.Web.Standard.ViewModelFactories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using Orders.Core.Persistance;
using Orders.Core.Services.Standard;
using Orders.Core.Model;
using Customer.Web.Standard.Validators;
using Customer.Web.Standard;

namespace Customer.Web.Controllers
{
    public class CartController : Controller
    {
        private const string userIdGuidCookieName = "CqrsShopCustomerGuid";

        CartViewModelFactory _viewModelFactory = new CartViewModelFactory();
        CartValidator _cartValidator = new CartValidator();
        CartService _cartService = new CartService();
        OrderService _orderService = new OrderService();

        [HttpPost]
        public ActionResult AddProductToCart(Guid productId)
        {
            Guid userGuid = GetUserId();
            var cartGuid = GetActiveCartGuid(userGuid);

            var commandValidatorResult = _cartValidator.ValidateAddProductToCart(productId);
            if (!commandValidatorResult.IsValid)
            {
                return View("SystemError", new SystemErrorViewModel()
                {
                    ErrorMessage = commandValidatorResult.ErrorMessage
                });
            }

            _cartService.AddProductToCart(cartGuid, userGuid, productId, 1);

            ProductAddedViewModel viewModel = new ProductAddedViewModel()
            {
                ProductId = productId,
                UserId = userGuid
            };

            return View("ProductInCart", viewModel);
        }

        public ActionResult CartDetails()
        {
            string error = (string)TempData["Error"];
            if (!string.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("", error);
            }

            var cookie = HttpContext.Request.Cookies.Get(userIdGuidCookieName);

            if (User.Identity.IsAuthenticated && cookie != null)
            {
                return RedirectToAction("SwapCarts");
            }

            Guid userGuid = GetUserId();
            var viewModel = _viewModelFactory.CreateCartDetailsViewModel(userGuid);

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult SwapCarts()
        {
            var cookie = HttpContext.Request.Cookies.Get(userIdGuidCookieName);
            Guid cookieUserGuid = new Guid(cookie.Value);
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);
            HttpContext.Request.Cookies.Remove(userIdGuidCookieName);

            using (var session = OrdersSessionFactory.GetInstance().OpenSession())
            {
                Cart cart = session.QueryOver<Cart>()
                    .Where(c => c.UserId == GetUserId())
                    .SingleOrDefault();

                Cart cookieUserCartDetails = session.QueryOver<Cart>()
                    .Where(c => c.UserId == cookieUserGuid)
                    .SingleOrDefault();

                _cartService.SwapCarts(cart?.Id ?? Guid.Empty, cookieUserCartDetails.Id, GetUserId());
            }

            return RedirectToAction("CartsSwapped");
        }

        [HttpGet]
        public ActionResult CartsSwapped()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RemoveCartItem(Guid productGuid)
        {
            Guid userGuid = GetUserId();
            var cartGuid = GetActiveCartGuid(userGuid);

            var validationResult = _cartValidator.ValidateRemoveFromCart(cartGuid, productGuid);
            if (!validationResult.IsValid)
            {
                TempData["Error"] = "Couldn't remove product from cart.";
                return RedirectToAction("CartDetails");
            }

            _cartService.RemoveItemFromCart(cartGuid, productGuid);

            return RedirectToAction("CartItemRemoved", new { cartGuid, productGuid });
        }

        [HttpGet]
        public ActionResult CartItemRemoved(Guid cartGuid, Guid productGuid)
        {
            CartItemRemovedViewModel viewModel = _viewModelFactory
                .CreateCartDetailsRemovedViewModel(cartGuid, productGuid);

            return View("CartItemRemoved", viewModel);
        }

        [HttpGet]
        public ActionResult ChangeCartItemQuantity(Guid cartGuid, Guid productGuid)
        {
            var viewModel = _viewModelFactory.CreateChangeCartItemQuantity(cartGuid, productGuid);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ChangeCartItemQuantity(ChangeCartItemQuantityViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = _viewModelFactory
                    .CreateChangeCartItemQuantity(model.CartGuid.Value, model.ProductGuid.Value);
                viewModel.NewQuantity = model.NewQuantity;

                return View(viewModel);
            }

            _cartService.ChangeProductQuantityInCart(
                model.CartGuid.Value, 
                model.ProductGuid.Value, 
                model.NewQuantity.Value);

            return RedirectToAction("CartItemQuantityChanged",
                new { cartGuid = model.CartGuid, productGuid = model.ProductGuid });
        }

        [HttpGet]
        public ActionResult CartItemQuantityChanged(Guid cartGuid, Guid productGuid)
        {
            CartItemQuantityChangedViewModel viewModel = _viewModelFactory
                .CreateCartItemQuantityChangedViewModel(cartGuid, productGuid);
            return View("CartItemQuantityChanged", viewModel);
        }

        [HttpPost]
        public ActionResult RemoveCart(Guid cartGuid)
        {
            var validationResult = _cartValidator.ValidateRemoveCart(cartGuid, GetUserId());

            if (!validationResult.IsValid)
            {
                return View("SystemError", new SystemErrorViewModel
                {
                    ErrorMessage = validationResult.ErrorMessage
                });
            }

            _cartService.DisableCart(cartGuid);

            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        public ActionResult CheckOut(Guid cartGuid)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account",
                    new { returnUrl = Url.Action("CartDetails", "Cart", null) });
            }

            Guid userId = GetUserId();

            var validationResult = _cartValidator.ValidateCheckOut(cartGuid, userId);

            if (!validationResult.IsValid)
            {
                return View("SystemError", new SystemErrorViewModel()
                {
                    ErrorMessage = validationResult.ErrorMessage
                });
            }

            _orderService.CheckoutCart(cartGuid);

            return RedirectToAction("CheckOutCompleted", "Cart");
        }

        [HttpGet]
        public ActionResult CheckOutCompleted()
        {
            return View();
        }

        private Guid GetUserId()
        {
            Guid userGuid;
            if (User.Identity.IsAuthenticated)
            {
                ApplicationUser user = System.Web.HttpContext.Current
                    .GetOwinContext()
                    .GetUserManager<ApplicationUserManager>()
                    .FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

                userGuid = user.UserId;
            }
            else
            {
                var cookie = HttpContext.Request.Cookies.Get(userIdGuidCookieName);

                if (cookie == null)
                {
                    userGuid = Guid.NewGuid();
                    HttpContext.Response.SetCookie(new System.Web.HttpCookie(userIdGuidCookieName, userGuid.ToString()));
                }
                else
                {
                    userGuid = new Guid(cookie.Value);
                }
            }

            return userGuid;
        }

        private Guid GetActiveCartGuid(Guid userGuid)
        {
            using (var session = OrdersSessionFactory.GetInstance().OpenSession())
            {
                var cart = session.QueryOver<Cart>()
                    .Where(c => c.UserId == userGuid)
                    .SingleOrDefault();

                return cart != null
                    ? cart.Id
                    : Guid.Empty;
            }
        }
    }
}