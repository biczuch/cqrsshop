﻿using Customer.Web.Standard.Models.Product;
using Customer.Web.Standard.ViewModelFactories;
using System;
using System.Web.Mvc;

namespace Customer.Web.Standard.Controllers
{
    public class ProductController : Controller
    {
        int itemsPerPage = 10;

        ProductViewModelFactory _productViewModelFactory = new ProductViewModelFactory();

        public ActionResult Index(ProductSearchFilterViewModel filterViewModel = null, int page = 1)
        {
            var viewModel = _productViewModelFactory.CreteProductListViewModel(filterViewModel, page, itemsPerPage);

            return View(viewModel);
        }

        public ActionResult ProductDetails(Guid productGuid)
        {
            var viewModel = _productViewModelFactory.CreateProductDetailsViewModel(productGuid);
            return View(viewModel);
        }
    }
}