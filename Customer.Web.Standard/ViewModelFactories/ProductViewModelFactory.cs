﻿using Customer.Web.Standard.Models.Product;
using NHibernate;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System;
using Orders.Core;

namespace Customer.Web.Standard.ViewModelFactories
{
    public class ProductViewModelFactory
    {
        ISessionFactory _queryRepository = Orders.Core.Persistance.OrdersSessionFactory.GetInstance();

        internal ProductListViewModel CreteProductListViewModel(ProductSearchFilterViewModel filterViewModel,
            int page, int itemsPerPage)
        {
            IList<Product> products;
            using (var session = _queryRepository.OpenSession())
            {
                var productsQuery = session.QueryOver<Product>()
                    .Where(p => p.IsActive);
                if (!string.IsNullOrWhiteSpace(filterViewModel.ProductName))
                {
                    productsQuery.WhereRestrictionOn(p => p.ProductName).IsLike($"%{ filterViewModel.ProductName }%");
                }
                if (filterViewModel.GameGenre.HasValue)
                {
                    productsQuery.Where(p => p.GameGenre == filterViewModel.GameGenre);
                }
                if (filterViewModel.SalePlatform.HasValue)
                {
                    productsQuery.Where(p => p.SalePlatform == filterViewModel.SalePlatform);
                }
                products = productsQuery.OrderBy(p => p.ProductName).Asc.List();

                if (filterViewModel.MaxPrice > 0)
                {
                    products = products.Where(p => p.GetTotalAmount() <= filterViewModel.MaxPrice).ToList();
                }
            }

            ProductListViewModel viewModel = new ProductListViewModel()
            {
                Results = products
                .Select(p => new ProductListEntryViewModel(p))
                .ToPagedList(page, itemsPerPage),
                SearchFilter = filterViewModel ?? new ProductSearchFilterViewModel()
            };

            return viewModel;
        }

        internal ProductDetailsViewModel CreateProductDetailsViewModel(Guid productGuid)
        {
            using (var session = _queryRepository.OpenSession())
            {
                var productDetails = session.QueryOver<Product>()
                    .Where(p => p.Id == productGuid)
                    .SingleOrDefault();


                ProductDetailsViewModel viewModel = new ProductDetailsViewModel()
                {
                    Name = productDetails.ProductName,
                    ProductGuid = productDetails.Id,
                    Description = productDetails.Description,
                    Price = productDetails.GetTotalAmount(),
                    GameGenre = productDetails.GameGenre,
                    SalePlatform = productDetails.SalePlatform,
                    ImageUris = productDetails.ImageUris.Skip(1),
                    ImageThumbnail = productDetails.ImageUris.Any() ? productDetails.ImageUris.First() : null
                };

                return viewModel;
            }
        }
    }
}