﻿using Customer.Web.Standard.Models.Order;
using NHibernate;
using Orders.Common;
using Orders.Core;
using Orders.Core.Model;
using Orders.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Customer.Web.Standard.ViewModelFactories
{
    public class OrderViewModelFactory
    {
        ISessionFactory _sessionFactory = OrdersSessionFactory.GetInstance();

        internal List<OrderListEntryViewModel> CreateOrderListViewModel(Guid userGuid)
        {
            List<OrderListEntryViewModel> orderList = new List<OrderListEntryViewModel>();

            using (var session = _sessionFactory.OpenSession())
            {
                var orders = session.QueryOver<Order>()
                    .Where(o => o.UserId == userGuid)
                    .List();

                foreach (var order in orders)
                {
                    OrderListEntryViewModel orderViewModel = new OrderListEntryViewModel()
                    {
                        OrderGuid = order.Id,
                        Status = order.Status.ToString(),
                        DateCompleted = order.Status == Orders.Common.OrderStatus.Completed 
                            ? order.CompletedDate 
                            : (DateTime?)null,
                        DatePlaced = order.SubmittedDate,
                        IsCancelled = order.Status == OrderStatus.Cancelled,
                        TotalPrice = order.GetTotalAmount()
                    };

                    foreach (var orderItem in order.Items)
                    {

                        var product = session.QueryOver<Product>()
                            .Where(p => p.Id == orderItem.ProductId)
                            .SingleOrDefault();

                        var productViewModel = new OrderListProductViewModel()
                        {
                            ProductName = product.ProductName,
                            Quantity = orderItem.Quantity
                        };

                        orderViewModel.Products.Add(productViewModel);
                    }
                    orderList.Add(orderViewModel);
                }
            }

            return orderList;
        }

        internal OrderDetailsViewModel CreateOrderDetailsViewModel(Guid orderGuid, Guid userGuid)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                var order = session.Get<Order>(orderGuid);

                var products = session.QueryOver<Product>()
                    .WhereRestrictionOn(p => p.Id).IsIn(order.Items.Select(i => i.ProductId).ToList())
                    .List();

                if (order.UserId != userGuid)
                    throw new InvalidOperationException();

                var viewModel = new OrderDetailsViewModel()
                {
                    OrderGuid = order.Id,
                    DateCompleted = order.CompletedDate,
                    DatePlaced = order.SubmittedDate,
                    TotalPrice = order.GetTotalAmount(),
                };

                foreach(var orderItem in order.Items)
                {
                    var product = products.First(p => p.Id == orderItem.ProductId);

                    viewModel.Products = order.Items.Select(oi => new OrderDetailsProductViewModel()
                    {
                        ProductName = product.ProductName,
                        Codes = oi.Codes.ToList(),
                        ImageUri = product.ImageUris.FirstOrDefault()
                    }).ToList();
                }

                return viewModel;
            }
        }
    }
}