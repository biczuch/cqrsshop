﻿using Customer.Web.Standard.Models.Cart;
using Customer.Web.Standard.Models.Product;
using NHibernate;
using NHibernate.Criterion;
using Orders.Core;
using Orders.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Customer.Web.Standard.ViewModelFactories
{
    public class CartViewModelFactory
    {
        ISessionFactory _sessionFactory = Orders.Core.Persistance.OrdersSessionFactory.GetInstance();

        internal CartDetailsViewModel CreateCartDetailsViewModel(Guid userGuid)
        {
            Cart cartDetails;
            List<Product> products = new List<Product>();
            using (var session = _sessionFactory.OpenSession())
            {
                cartDetails = session.QueryOver<Cart>()
                    .Where(c => c.UserId == userGuid)
                    .SingleOrDefault();

                if (cartDetails == null)
                {
                    return new CartDetailsViewModel();
                }

                var productIds = cartDetails.Items
                    .Select(i => i.ProductId)
                    .ToList();

                if(cartDetails != null)
                {
                    products = session.QueryOver<Product>()
                        .WhereRestrictionOn(x => x.Id).IsIn(productIds)
                        .List()
                        .ToList();
                }
            }

            CartDetailsViewModel viewModel = new CartDetailsViewModel()
            {
                CartGuid = cartDetails.Id,
                CartItems = cartDetails.Items
                    .Select(i =>
                    {
                        Product product = products.First(p => p.Id == i.ProductId);
                        return new CartItemDetailsViewModel()
                        {
                            Price = product.GetTotalAmount(),
                            ProductGuid = product.Id,
                            ProductName = product.ProductName,
                            Quantity = i.Quantity
                        };
                    })
                    .ToArray()
            };

            return viewModel;
        }

        internal CartItemRemovedViewModel CreateCartDetailsRemovedViewModel(Guid cartGuid, Guid productGuid)
        {
            CartItemRemovedViewModel viewModel = new CartItemRemovedViewModel();

            using (var session = _sessionFactory.OpenSession())
            {
                var removedProduct = session.QueryOver<Product>()
                    .Where(c => c.Id == productGuid)
                    .SingleOrDefault();

                var suggestedProducts = session.QueryOver<Product>()
                    .Where(c => c.SalePlatform == removedProduct.SalePlatform)
                    .Where(c => c.GameGenre == removedProduct.GameGenre)
                    .Where(c => c.Id != removedProduct.Id)
                    .Take(5)
                    .List();

                viewModel.ProductsSuggestion = suggestedProducts
                    .Select(p => new ProductListEntryViewModel(p))
                    .ToArray();

                var cartDetails = session.QueryOver<Cart>()
                    .Where(c => c.Id == cartGuid)
                    .SingleOrDefault();

                if(cartDetails != null)
                {
                    viewModel.NumberOfProductsInCart = cartDetails.Items.Count();
                }
            }

            return viewModel;
        }

        internal ChangeCartItemQuantityViewModel CreateChangeCartItemQuantity(Guid cartGuid, Guid productGuid)
        {
            ChangeCartItemQuantityViewModel viewModel = new ChangeCartItemQuantityViewModel()
            {
                CartGuid = cartGuid,
                ProductGuid = productGuid
            };

            using (var session = _sessionFactory.OpenSession())
            {
                var product = session.QueryOver<Product>()
                    .Where(p => p.Id == productGuid)
                    .SingleOrDefault();

                Cart cart = session.QueryOver<Cart>()
                    .Where(c => c.Id == cartGuid)
                    .SingleOrDefault();

                CartItem productItem = cart.Items
                    .SingleOrDefault(i => i.ProductId == productGuid);

                viewModel.NewQuantity = productItem.Quantity;
                viewModel.ProductName = product.ProductName;
                viewModel.Thumbnail = product.ImageUris.FirstOrDefault();
            }

            return viewModel;
        }

        internal CartItemQuantityChangedViewModel CreateCartItemQuantityChangedViewModel(Guid cartGuid, Guid productGuid)
        {
            CartItemQuantityChangedViewModel viewModel = new CartItemQuantityChangedViewModel();

            using (var session = _sessionFactory.OpenSession())
            {
                var product = session.QueryOver<Product>()
                    .Where(c => c.Id == productGuid)
                    .SingleOrDefault();

                var suggestedProducts = session.QueryOver<Product>()
                    .Where(c => c.SalePlatform == product.SalePlatform)
                    .Where(c => c.GameGenre == product.GameGenre)
                    .Where(c => c.Id != product.Id)
                    .Take(5)
                    .List();

                viewModel.ProductsSuggestion = suggestedProducts
                    .Select(p => new ProductListEntryViewModel(p))
                    .ToArray();

                var cartDetails = session.QueryOver<Cart>()
                    .Where(c => c.Id == cartGuid)
                    .SingleOrDefault();

                viewModel.NumberOfProductsInCart = cartDetails.Items.Count;
            }

            return viewModel;
        }
    }
}