﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Customer.Web.Standard.Models.Order
{
    public class OrderDetailsViewModel
    {
        public Guid OrderGuid { get; set; }
        public DateTime DateCompleted { get; set; }
        public DateTime DatePlaced { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalPrice { get; set; }
        public List<OrderDetailsProductViewModel> Products = new List<OrderDetailsProductViewModel>();
    }

    public class OrderDetailsProductViewModel
    {
        public string ProductName { get; set; }
        public List<string> Codes { get; set; }
        public string ImageUri { get; set; }
    }
}