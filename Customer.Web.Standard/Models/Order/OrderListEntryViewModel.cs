﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Customer.Web.Standard.Models.Order
{
    public class OrderListEntryViewModel
    {
        public Guid OrderGuid { get; set; }
        public string Status { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? DatePlaced { get; set; }
        public bool IsCancelled { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalPrice { get; set; }

        public List<OrderListProductViewModel> Products { get; set; } = new List<OrderListProductViewModel>();

        public bool IsCompleted
        {
            get
            {
                return DateCompleted != null;
            }
        }
    }
}