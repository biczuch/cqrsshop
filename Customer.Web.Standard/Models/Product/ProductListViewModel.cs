﻿using PagedList;

namespace Customer.Web.Standard.Models.Product
{
    public class ProductListViewModel
    {
        public ProductSearchFilterViewModel SearchFilter { get; set; }
        public IPagedList<ProductListEntryViewModel> Results { get; set; }
    }
}