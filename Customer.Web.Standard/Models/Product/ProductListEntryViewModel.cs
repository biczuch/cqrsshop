﻿using SharedKernel;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Customer.Web.Standard.Models.Product
{
    public class ProductListEntryViewModel
    {
        public Guid ProductGuid { get; set; }
        public string ProductName { get; set; }
        public GameGenre GameGenre { get; set; }
        public SalePlatform SalePlatform { get; set; }
        public string ShortDescription { get; set; }

        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public string ImageUri { get; set; }

        public ProductListEntryViewModel(Orders.Core.Product product)
        {
            this.ProductGuid = product.Id;
            this.ProductName = product.ProductName;
            this.GameGenre = product.GameGenre;
            this.SalePlatform = product.SalePlatform;
            this.ShortDescription = product.ShortDescription;
            this.Price = product.GetTotalAmount();
            this.ImageUri = product.ImageUris.FirstOrDefault();
        }
    }
}