﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Customer.Web.Standard.Models
{
    public class SystemErrorViewModel
    {
        public string ErrorMessage { get; set; }
    }
}