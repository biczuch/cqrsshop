﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Customer.Web.Standard.Models.Cart
{
    public class ChangeCartItemQuantityViewModel : IValidatableObject
    {
        [Required]
        public Guid? CartGuid { get; set; }
        [Required]
        public Guid? ProductGuid { get; set; }

        [DisplayName("New quantity")]
        [Required]
        public int? NewQuantity { get; set; }

        [DisplayName("ProducName")]
        public string ProductName { get; set; }

        public string Thumbnail { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (NewQuantity < 0)
                yield return new ValidationResult("New quantity must have positive value.", new string[] { "NewQuantity" });
        }
    }
}