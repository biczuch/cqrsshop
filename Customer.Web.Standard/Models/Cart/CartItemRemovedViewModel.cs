﻿using Customer.Web.Standard.Models.Product;

namespace Customer.Web.Standard.Models.Cart
{
    public class CartItemRemovedViewModel
    {
        public int NumberOfProductsInCart { get; set; }
        public ProductListEntryViewModel[] ProductsSuggestion { get; set; }
    }
}