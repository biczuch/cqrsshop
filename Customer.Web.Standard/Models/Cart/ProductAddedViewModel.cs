﻿using System;

namespace Customer.Web.Standard.Models.Cart
{
    public class ProductAddedViewModel
    {
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
    }
}