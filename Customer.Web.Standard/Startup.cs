﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Customer.Web.Standard.Startup))]
namespace Customer.Web.Standard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
