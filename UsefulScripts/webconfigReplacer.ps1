﻿#ścieżka do głównego katalogu solucji - CqrsShop
$rootPath = "C:\Users\tomasz\Desktop\CqrsShop\"
$sqlServerName = "DESKTOP-TCEEN2K\SQLEXPRESS"

$solutionConfigs = @{
    #webApps
    "CqrsShop.Web\Web.config" = @(
            @("DefaultConnection", "CqrsShopUsers"),
            @("ProductManagement", "CqrsShop")
        );
    "Customer.Web\Web.config" = @( 
            @("ReadServiceDbConnection","CqrsShopOrdersRead"),
            @("CustomersDbConnection", "Customers")
        );
    "Customer.Web.Standard\Web.config" = @( 
            @("CqrsShopStandard","CqrsShopStandard"),
            @("CustomersDbConnection","Customers")
        );
    #common
    "ProductManagementService\app.config"= @( 
            @("ProductManagement","CqrsShop"),
            @("", "")
        );
    "OrderSagasManager\app.config"= @( 
            @("OrderSagas","OrderSagas"),
            @("", "")
        );
    #standard
    "OrdersEventReceiver\app.config"= @( 
           @("CqrsShopStandard","CqrsShopStandard"),
           @("", "")
        );
        
    #cqrs
    "OrdersManagementReadService\app.config"= @( 
            @("ReadServiceDbConnection","CqrsShopOrdersRead"),
            @("", "")
        );
    "OrdersManagementWriteService\app.config"= @( 
            @("OrdersES","CqrsShopOrdersWrite"),
            @("", "")
        )
}

$myConnectionStringTemplate = "Data Source=" + $sqlServerName +";Initial Catalog=<Database>;Integrated Security=True";

Function clearConnectionStrings($config){
    $doc = (Get-Content $config) -as [Xml]
    $root = $doc.get_DocumentElement();
    
    while($root.connectionStrings.ChildNodes.Count -gt 0)
    {
        $node = $root.connectionStrings.ChildNodes[0];
        $root.connectionStrings.RemoveChild($node);
    }

    $doc.Save($config)
}

Function updateConnectionString($config, $name, $catalog) 
{ 
    Write-Host $config
    $doc = (Get-Content $config) -as [Xml]
    $root = $doc.get_DocumentElement();
    Write-Host $root.connectionStrings.
    $connectionStringNode = $doc.CreateElement("add");
  
    $nameAttribute = $doc.CreateAttribute("name");
    $nameAttribute.Value = $name;
    $connectionStringNode.Attributes.Append($nameAttribute);

    $connectionStringAttribute = $doc.CreateAttribute("connectionString");
    $connectionStringAttribute.Value = $myConnectionStringTemplate.Replace("<Database>", $catalog);
    $connectionStringNode.Attributes.Append($connectionStringAttribute);

    $providerAttribute = $doc.CreateAttribute("providerName");
    $providerAttribute.Value = "System.Data.SqlClient";
    $connectionStringNode.Attributes.Append($providerAttribute);

    $doc.SelectSingleNode('//configuration/connectionStrings').AppendChild($connectionStringNode);
    $doc.Save($config)
} 


ForEach($configFile in $solutionConfigs.Keys)
{
    $fullPath = Join-Path $rootPath $configFile;

    clearConnectionStrings($fullPath);
    foreach($connectionstringconfig in $solutionconfigs.get_item($configfile))
    {
        if($connectionstringconfig[0] -ne "")
        {
            $name = $connectionstringconfig[0];
            $catalog = $connectionstringconfig[1]
            updateconnectionstring $fullpath $name $catalog;
        }
    }
}