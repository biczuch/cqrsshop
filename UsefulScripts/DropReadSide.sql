DROP TABLE CqrsShopOrdersRead.dbo.ProductList
DROP TABLE CqrsShopOrdersRead.dbo.ImageUri
DROP TABLE CqrsShopOrdersRead.dbo.ProductDetails

DROP TABLE CqrsShopOrdersRead.dbo.CartItem
DROP TABLE CqrsShopOrdersRead.dbo.CartDetails

DROP TABLE CqrsShopOrdersRead.dbo.OrderDetailsProductCodes
DROP TABLE CqrsShopOrdersRead.dbo.OrderDetailsProduct
DROP TABLE CqrsShopOrdersRead.dbo.OrderDetails

DROP TABLE CqrsShopOrdersRead.dbo.OrderListEntryProducts
DROP TABLE CqrsShopOrdersRead.dbo.OrderListEntries