﻿#ścieżka do głównego katalogu solucji - CqrsShop
$rootPath = "C:\Users\tomasz\Desktop\CqrsShop"
#nazwa i hasło użytkownika systemowego, na którym postawione będą usługi Windows
$username = "" #domena\uzytkownik
$password = ""

$action = Read-Host "Action? (Install or Uninstall)"

$topShelfProjects = @(
    #common
    "ProductManagementService",
    "OrderSagasManager",
    #standard
    "OrdersEventReceiver",
    #cqrs
    "OrdersManagementReadService",
    "OrdersManagementWriteService"
)

ForEach($project in $topShelfProjects)
{
    $fullPath = $rootPath;
    $fullPath = Join-Path $fullPath $project
    $fullPath = Join-Path $fullPath "bin\release"
    $fullPath = Join-Path $fullPath ($project + ".exe")

    switch($action)
    {
        "Install" 
        {        
            & $fullPath install -username:$username -password:$password
            & $fullPath @("start")
        }
        "Uninstall"
        {
            & $fullPath @("uninstall")
        }
        default { Write-Host "Invalid action" }

    }
    
}