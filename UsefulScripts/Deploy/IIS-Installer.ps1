﻿#################################################################

#nazwa i hasło użytkownika systemowego
$username = "desktop-tceen2k\tomasz"
$password = "k99kz2sc"

#################################################################

$releaseBinaries = Join-Path $PSScriptRoot "ApplicationsBinaries\Web"

Import-Module "WebAdministration"
$appPool = "CqrsSample"
if(!(Test-Path IIS:\AppPools\CqrsSample))
{
    New-Item IIS:\AppPools\CqrsSample
}

Set-ItemProperty IIS:\AppPools\CqrsSample -name processModel -value @{userName=$username;password=$password;identitytype=3}

$webAppsDirectories = @(
        "CustomerWebCQRS",
        "CustomerWebStandard",
        "EmployeeWeb",
        "TestPaymentPortal");

$virtualPathRoot = 'IIS:\Sites\CqrsSampleSites'

$virtualDirectories = @(
    "EmployeeWeb",
    "CustomerCqrs",
    "CustomerStandard",
    "TestPayments"
);

New-Item $virtualPathRoot -physicalPath  $releaseBinaries -bindings @{protocol="http";bindingInformation=":8080:"}
Set-ItemProperty $virtualPathRoot -name applicationPool -value $appPool

For($i = 0; $i -lt $webAppsDirectories.Count; $i++)
{
    $virtualDirectory = Join-Path $virtualPathRoot $virtualDirectories[$i];
    $physicalPath = Join-Path $releaseBinaries $webAppsDirectories[$i];

    New-Item $virtualDirectory -physicalPath $physicalPath -type Application
    Set-ItemProperty $virtualDirectory -name applicationPool -value $appPool
}

