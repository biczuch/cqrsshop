﻿#################################################################

#nazwa serwera SQL
$sqlServerName = "TOMASZ-PC\SQLEXPRESS01"

#################################################################

$rootPath = Join-Path $PSScriptRoot "ApplicationsBinaries"

$solutionWebConfigs = @{
    #webApps
    "EmployeeWeb" = @(
            @("DefaultConnection", "CqrsShopUsers"),
            @("ProductManagement", "CqrsShop")
        );
    "CustomerWebCQRS" = @( 
            @("ReadServiceDbConnection","CqrsShopOrdersRead"),
            @("CustomersDbConnection", "Customers")
        );
    "CustomerWebStandard" = @( 
            @("CqrsShopStandard","CqrsShopStandard"),
            @("CustomersDbConnection","Customers")
        );
};

$solutionServiceConfigs = @{
    #common
    "ProductManagementService"= @( 
            @("ProductManagement","CqrsShop"),
            @("", "")
        );
    "OrderSagasManager"= @( 
            @("OrderSagas","OrderSagas"),
            @("", "")
        );
    #standard
    "OrdersEventReceiver"= @( 
           @("CqrsShopStandard","CqrsShopStandard"),
            @("", "")
        );
        
    #cqrs
    "OrdersManagementReadService"= @( 
            @("ReadServiceDbConnection","CqrsShopOrdersRead"),
            @("", "")
        );
    "OrdersManagementWriteService"= @( 
            @("OrdersES","CqrsShopOrdersWrite"),
            @("", "")
        )
}

$myConnectionStringTemplate = "Data Source=" + $sqlServerName +";Initial Catalog=<Database>;Integrated Security=True";

Function clearConnectionStrings($config){
    $doc = (Get-Content $config) -as [Xml]
    $root = $doc.get_DocumentElement();
    
    while($root.connectionStrings.ChildNodes.Count -gt 0)
    {
        $node = $root.connectionStrings.ChildNodes[0];
        $root.connectionStrings.RemoveChild($node);
    }

    $doc.Save($config)
}

Function updateConnectionString($config, $name, $catalog) 
{ 

    $doc = (Get-Content $config) -as [Xml]
    $root = $doc.get_DocumentElement();

    $connectionStringNode = $doc.CreateElement("add");
  
    $nameAttribute = $doc.CreateAttribute("name");
    $nameAttribute.Value = $name;
    $connectionStringNode.Attributes.Append($nameAttribute);

    $connectionStringAttribute = $doc.CreateAttribute("connectionString");
    $connectionStringAttribute.Value = $myConnectionStringTemplate.Replace("<Database>", $catalog);
    $connectionStringNode.Attributes.Append($connectionStringAttribute);

    $providerAttribute = $doc.CreateAttribute("providerName");
    $providerAttribute.Value = "System.Data.SqlClient";
    $connectionStringNode.Attributes.Append($providerAttribute);

    $doc.SelectSingleNode('//configuration/connectionStrings').AppendChild($connectionStringNode);
    $doc.Save($config)
} 


ForEach($configFile in $solutionWebConfigs.Keys)
{
    $fullPath = [io.path]::Combine($rootPath, "Web", $configFile, "Web.config");

    clearConnectionStrings($fullPath);
    foreach($connectionstringconfig in $solutionWebConfigs.get_item($configfile))
    {
        if($connectionstringconfig[0] -ne "")
        {
            $name = $connectionstringconfig[0];
            $catalog = $connectionstringconfig[1]
            updateconnectionstring $fullpath $name $catalog;
        }
    }
}

ForEach($configFile in $solutionServiceConfigs.Keys)
{
    $fullPath = [io.path]::Combine($rootPath, "Services", $configFile, $configFile + ".exe.config");

    clearConnectionStrings($fullPath);
    foreach($connectionstringconfig in $solutionServiceConfigs.get_item($configfile))
    {
        if($connectionstringconfig[0] -ne "")
        {
            $name = $connectionstringconfig[0];
            $catalog = $connectionstringconfig[1]
            updateconnectionstring $fullpath $name $catalog;
        }
    }
}