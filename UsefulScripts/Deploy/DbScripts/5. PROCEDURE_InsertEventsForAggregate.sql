CREATE TYPE EventList AS TABLE(
	EventData VARCHAR(MAX)
)

GO

CREATE PROCEDURE InsertEventsForAggregate
    @aggregateGuid uniqueidentifier,
    @aggregateType nvarchar(100),
	@events EventList READONLY,
    @expectedVersion int = 0
AS 
    DECLARE @versionNumber int = null
    DECLARE @eventData nvarchar(MAX) = null
    
    SELECT @versionNumber = Version
    FROM Aggregates
    WHERE AggregateGuid = @aggregateGuid

    BEGIN Transaction

    if @versionNumber IS NULL
    BEGIN
        INSERT INTO Aggregates (AggregateGuid, Version, Type)
        VALUES (@aggregateGuid, 0, @aggregateType)

        SET @versionNumber = 0
    END

    if @versionNumber != @expectedVersion
    BEGIN
		DECLARE @msg nvarchar(MAX) = 'Concurrency problem while inserting events for aggregate '
				+ @aggregateType+' (aggregateGuid = '+ CONVERT(VARCHAR(36),@aggregateGuid) +')'
        ;THROW 99001, @msg, 16;
        ROLLBACK
    END

    DECLARE EVENT_CURSOR CURSOR 
    LOCAL STATIC READ_ONLY FORWARD_ONLY
    FOR 
    SELECT eventData 
    FROM @events

    OPEN EVENT_CURSOR
    FETCH NEXT FROM EVENT_CURSOR INTO @eventData
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @versionNumber = @versionNumber + 1; 
        
        INSERT INTO EVENTS(AggregateGuid, Data, Version, DateProcessed)
        VALUES(@aggregateGuid, @eventData, @versionNumber, getDate())

        UPDATE AGGREGATES
        SET Version = @versionNumber
        WHERE AggregateGuid = @aggregateGuid

		FETCH NEXT FROM EVENT_CURSOR INTO @eventData
    END
    CLOSE EVENT_CURSOR
    DEALLOCATE EVENT_CURSOR

    COMMIT    
