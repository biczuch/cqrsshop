﻿#################################################################

#nazwa i hasło użytkownika systemowego, na którym postawione będą usługi Windows
$username = "desktop-tceen2k\tomasz" #domena\uzytkownik
$password = "k99kz2sc"

#################################################################

$rootPath = Join-Path $PSScriptRoot "ApplicationsBinaries\Services"

$action = Read-Host "Action? (Install or Uninstall)"

$topShelfProjects = @(
    #common
    "ProductManagementService",
    "OrderSagasManager",
    #standard
    "OrdersEventReceiver",
    #cqrs
    "OrdersManagementReadService",
    "OrdersManagementWriteService"
)

ForEach($project in $topShelfProjects)
{
    $fullPath = $rootPath;
    $fullPath = Join-Path $fullPath $project
    $fullPath = Join-Path $fullPath ($project + ".exe")

    switch($action)
    {
        "Install" 
        {        
            & $fullPath install -username:$username -password:$password
            & $fullPath @("start")
        }
        "Uninstall"
        {
            & $fullPath @("uninstall")
        }
        default { Write-Host "Invalid action" }

    }
    
}