--CLEAR ALL--
--PRODUCT MANAGEMENT--
DELETE FROM CqrsShop.[dbo].ImageUri
DELETE FROM CqrsShop.[dbo].ProductCode
DELETE FROM CqrsShop.[dbo].Product

--CQRS WRITE--
DELETE FROM CqrsShopOrdersWrite.dbo.Events
DELETE FROM CqrsShopOrdersWrite.dbo.Aggregates
DELETE FROM CqrsShopOrdersWrite.dbo.Snapshots

--CQRS READ--
DELETE FROM CqrsShopOrdersRead.dbo.ProductList
DELETE FROM CqrsShopOrdersRead.dbo.ImageUri
DELETE FROM CqrsShopOrdersRead.dbo.ProductDetails

DELETE FROM CqrsShopOrdersRead.dbo.CartItem
DELETE FROM CqrsShopOrdersRead.dbo.CartDetails

DELETE FROM CqrsShopOrdersRead.dbo.OrderDetailsProductCodes
DELETE FROM CqrsShopOrdersRead.dbo.OrderDetailsProduct
DELETE FROM CqrsShopOrdersRead.dbo.OrderDetails

DELETE FROM CqrsShopOrdersRead.dbo.OrderListEntryProducts
DELETE FROM CqrsShopOrdersRead.dbo.OrderListEntries

--Standard--
DELETE FROM CqrsShopStandard.dbo.[CartItem]
DELETE FROM CqrsShopStandard.dbo.[Cart]

DELETE FROM CqrsShopStandard.dbo.[ImageUri]
DELETE FROM CqrsShopStandard.dbo.[ProductCode]
DELETE FROM CqrsShopStandard. dbo.[Product]

DELETE FROM CqrsShopStandard.dbo.[OrderItemCodes]
DELETE FROM CqrsShopStandard.dbo.[OrderItem]
DELETE FROM CqrsShopStandard.dbo.[Order]

DELETE FROM CqrsShopStandard.dbo.[ProductCode]
DELETE FROM CqrsShopStandard.dbo.[Product]

--Standard--
DELETE FROM CqrsShopStandard.dbo.[CartItem]
DELETE FROM CqrsShopStandard.dbo.[Cart]

DELETE FROM CqrsShopStandard.dbo.[ImageUri]
DELETE FROM CqrsShopStandard.dbo.[ProductCode]
DELETE FROM CqrsShopStandard. dbo.[Product]

DELETE FROM CqrsShopStandard.dbo.[OrderItemCodes]
DELETE FROM CqrsShopStandard.dbo.[OrderItem]
DELETE FROM CqrsShopStandard.dbo.[Order]

DELETE FROM CqrsShopStandard.dbo.[ProductCode]
DELETE FROM CqrsShopStandard.dbo.[Product]
