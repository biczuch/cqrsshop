﻿using System;

namespace Orders.Contracts.Order
{
    public class CodesAssignedEvent : BaseOrderEvent
    {
        public Guid ProductId { get; set; }
        public string[] Codes { get; set; }
    }
}
