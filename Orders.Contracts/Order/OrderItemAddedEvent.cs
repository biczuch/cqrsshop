﻿using System;

namespace Orders.Contracts.Order
{
    public class OrderItemAddedEvent : BaseOrderEvent
    {
        public Guid ProductId { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal MarginPercentage { get; set; }
        public int Quantity { get; set; }
    }
}
