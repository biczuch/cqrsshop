﻿using System;

namespace Orders.Contracts.Order
{
    public class OrderSubmittedEvent : BaseOrderEvent
    {
        public DateTime SubmittedDate { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
