﻿using System;

namespace Orders.Contracts.Order
{
    public class OrderCancelledEvent : BaseOrderEvent
    {
        public DateTime CancelledDate { get; set; }
        public string Status { get; set; }
    }
}
