﻿using System;

namespace Orders.Contracts.Order
{
    public class OrderCompletedEvent : BaseOrderEvent
    {
        public DateTime CompletedDate { get; set; }
        public string Status { get; set; }
    }
}
