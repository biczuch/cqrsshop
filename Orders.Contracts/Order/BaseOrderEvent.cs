﻿using DddCommon;
using System;

namespace Orders.Contracts.Order
{
    public abstract class BaseOrderEvent : IDomainEvent
    {
        public Guid OrderId { get; set; }
    }
}
