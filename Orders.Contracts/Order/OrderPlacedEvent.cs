﻿using Orders.Common;
using System;

namespace Orders.Contracts.Order
{
    public class OrderPlacedEvent : BaseOrderEvent
    {
        public OrderStatus OrderStatus { get; set; }
        public Guid UserId { get; set; }
        public bool IsActive { get; set; }
        public string Status { get; set; }
    }
}
