﻿namespace Orders.Contracts.Order
{
    public class PaymentMadeEvent : BaseOrderEvent
    {
        public decimal PaymentAmount { get; set; }
    }
}
