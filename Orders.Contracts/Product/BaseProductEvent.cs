﻿using DddCommon;
using System;

namespace Orders.Contracts.Product
{
    public class BaseProductEvent : IDomainEvent
    {
        public Guid ProductId { get; set; }
    }
}
