﻿using System.Collections.Generic;
using SharedKernel;

namespace Orders.Contracts.Product
{
    public class ProductInformationChangedEvent : BaseProductEvent
    {
        public GameGenre GameGenre { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public ICollection<string> ImageUris { get; set; }
    }
}
