﻿using DddCommon.CommonValueObjects;

namespace Orders.Contracts.Product
{
    public class ProductPriceChangedEvent : BaseProductEvent
    {
        public decimal ProductPrice { get; set; }
        public decimal MarginPercentage { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
