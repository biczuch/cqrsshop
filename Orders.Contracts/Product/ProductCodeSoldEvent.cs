﻿using System;

namespace Orders.Contracts.Product
{
    public class ProductCodeSoldEvent : BaseProductEvent
    {
        public Guid CodeId { get; set; }
        public decimal SoldPrice { get; set; }
        public decimal MarginPercentage { get; set; }
    }
}
