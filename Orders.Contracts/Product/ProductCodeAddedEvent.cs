﻿using DddCommon;
using System;

namespace Orders.Contracts.Product
{
    public class ProductCode
    {
        public Guid CodeId { get; set; }
        public string Code { get; set; }
    }

    public class ProductCodeAddedEvent : BaseProductEvent
    {
        public ProductCode[] ProductCodes { get; set; }
    }
}
