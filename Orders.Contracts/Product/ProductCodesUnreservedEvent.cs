﻿namespace Orders.Contracts.Product
{
    public class ProductCodesUnreservedEvent : BaseProductEvent
    {
        public int Quantity { get; set; }
    }
}
