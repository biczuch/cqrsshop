﻿namespace Orders.Contracts.Product
{
    public class ProductCodesReservedEvent : BaseProductEvent
    { 
        public int Quantity { get; set; }
    }
}
