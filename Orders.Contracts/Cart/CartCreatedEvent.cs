﻿using DddCommon;
using System;

namespace Orders.Contracts.Cart
{
    public class CartCreatedEvent : BaseCartEvent
    {
        public Guid UserId { get; set; }
    }
}
