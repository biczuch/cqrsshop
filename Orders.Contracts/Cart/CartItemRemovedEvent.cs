﻿using DddCommon;
using System;

namespace Orders.Contracts.Cart
{
    public class CartItemRemovedEvent : BaseCartEvent
    {
        public Guid ProductId { get; set; }
    }
}
