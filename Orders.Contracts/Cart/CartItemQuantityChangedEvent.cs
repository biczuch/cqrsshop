﻿using DddCommon;
using System;

namespace Orders.Contracts.Cart
{
    public class CartItemQuantityChangedEvent : BaseCartEvent
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
