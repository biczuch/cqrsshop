﻿using System;

namespace Orders.Contracts.Cart
{
    public class CartUserAssignedEvent : BaseCartEvent
    {
        public Guid UserId {get;set;}
    }
}
