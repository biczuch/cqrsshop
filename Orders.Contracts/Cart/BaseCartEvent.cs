﻿using DddCommon;
using System;

namespace Orders.Contracts.Cart
{
    public abstract class BaseCartEvent : IDomainEvent
    {
        public Guid CartId { get; set; }
    }
}
