﻿using MassTransit;
using Orders.Commands;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace Customer.Web.Infrastructure
{
    public class CommandSender
    {
        IBusControl _busControl;
        private readonly ISendEndpoint _sendEndpoint;

        public CommandSender()
        {
            _busControl = MvcApplication.BusControl;
            string endpointAddress = ConfigurationManager
                .AppSettings["OrdersManagementWriteServiceMessageQueueAddress"];


            Task<ISendEndpoint> task = _busControl.GetSendEndpoint(new Uri(endpointAddress));
            task.Wait();
            _sendEndpoint = task.Result;
        }

        public async Task SendCommand<T>(T command) where T : Command
        {
            await _sendEndpoint.Send<T>(command);
        }
    }
}