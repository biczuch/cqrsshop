﻿using MassTransit;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Customer.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IBusControl BusControl;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var connectionString = ConfigurationManager.ConnectionStrings["ReadServiceDbConnection"].ConnectionString;
            QueryRepository.QueryRepositorySessionFactory.GetInstance(connectionString);

            BusControl = ConfigureBus();
            BusControl.Start();
        }

        protected void Application_End()
        {
            BusControl.Stop(TimeSpan.FromSeconds(30));
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });
        }
    }
}
