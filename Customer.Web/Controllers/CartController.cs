﻿using System;
using System.Web.Mvc;
using Customer.Web.Models;
using Customer.Web.Infrastructure;
using Orders.Commands.Cart;
using Customer.Web.CommandValidators;
using Customer.Web.Models.Cart;
using System.Threading.Tasks;
using Customer.Web.ViewModelFactories;
using QueryRepository;
using NHibernate;
using QueryRepository.Model.Cart;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using Customer.Web.CommandBuilders;

namespace Customer.Web.Controllers
{
    public class CartController : Controller
    {
        private const string userIdGuidCookieName = "CqrsShopCustomerGuid";

        CommandSender _commandSender = new CommandSender();
        CartCommandValidator _validator = new CartCommandValidator();
        CartViewModelFactory _viewModelFactory = new CartViewModelFactory();
        CartCommandBuilder _commandBuilder = new CartCommandBuilder();

        ISessionFactory _sessionFactory = QueryRepositorySessionFactory.GetInstance();

        [HttpPost]
        public async Task<ActionResult> AddProductToCart(Guid productId)
        {
            var commandValidatorResult = _validator.ValidateAddProductToCart(productId);
            if (!commandValidatorResult.IsValid)
            {
                return View("SystemError", new SystemErrorViewModel()
                {
                    ErrorMessage = commandValidatorResult.ErrorMessage
                });
            }

            Guid userGuid = GetUserId();
            var cartGuid = GetActiveCartGuid(userGuid);

            var command = new AddItemToCart()
            {
                ProductId = productId,
                Quantity = 1,
                UserId = userGuid,
                CartId = cartGuid
            };

            await _commandSender.SendCommand(command);

            ProductAddedViewModel viewModel = new ProductAddedViewModel()
            {
                ProductId = productId,
                UserId = userGuid
            };

            return View("ProductInCart", viewModel);
        }

        public ActionResult CartDetails()
        {
            string error = (string)TempData["Error"];
            if (!string.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("", error);
            }

            var cookie = HttpContext.Request.Cookies.Get(userIdGuidCookieName);

            if (User.Identity.IsAuthenticated && cookie != null)
            {
                return RedirectToAction("SwapCarts");
            }

            Guid userGuid = GetUserId();
            var viewModel = _viewModelFactory.CreateCartDetailsViewModel(userGuid);

            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> SwapCarts()
        {
            var cookie = HttpContext.Request.Cookies.Get(userIdGuidCookieName);
            Guid cookieUserGuid = new Guid(cookie.Value);
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);
            HttpContext.Request.Cookies.Remove(userIdGuidCookieName);
            SwapCarts command = _commandBuilder.BuidSwapCartsCommand(GetUserId(), cookieUserGuid);

            await _commandSender.SendCommand(command);

            return RedirectToAction("CartsSwapped");
        }

        [HttpGet]
        public ActionResult CartsSwapped()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RemoveCartItem(Guid productGuid)
        {
            Guid userGuid = GetUserId();
            var cartGuid = GetActiveCartGuid(userGuid);

            var commandValidatorResult = _validator.ValidateRemoveFromCart(cartGuid, productGuid);
            if (!commandValidatorResult.IsValid)
            {
                TempData["Error"] = "Couldn't remove product from cart.";
                return RedirectToAction("CartDetails");
            }

            var command = new RemoveItemFromCart()
            {
                CartGuid = cartGuid,
                ProductId = productGuid
            };

            await _commandSender.SendCommand(command);

            return RedirectToAction("CartItemRemoved", new { cartGuid, productGuid });
        }

        [HttpGet]
        public ActionResult CartItemRemoved(Guid cartGuid, Guid productGuid)
        {
            CartItemRemovedViewModel viewModel = _viewModelFactory
                .CreateCartDetailsRemovedViewModel(cartGuid, productGuid);

            return View("CartItemRemoved", viewModel);
        }

        [HttpGet]
        public ActionResult ChangeCartItemQuantity(Guid cartGuid, Guid productGuid)
        {
            var viewModel = _viewModelFactory.CreateChangeCartItemQuantity(cartGuid, productGuid);
            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> ChangeCartItemQuantity(ChangeCartItemQuantityViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = _viewModelFactory
                    .CreateChangeCartItemQuantity(model.CartGuid.Value, model.ProductGuid.Value);
                viewModel.NewQuantity = model.NewQuantity;

                return View(viewModel);
            }

            await _commandSender.SendCommand(new ChangeProductQuantity()
            {
                CartId = model.CartGuid.Value,
                ProductId = model.ProductGuid.Value,
                NewQuantity = model.NewQuantity.Value
            });

            return RedirectToAction("CartItemQuantityChanged",
                new { cartGuid = model.CartGuid, productGuid = model.ProductGuid });
        }

        [HttpGet]
        public ActionResult CartItemQuantityChanged(Guid cartGuid, Guid productGuid)
        {
            CartItemQuantityChangedViewModel viewModel = _viewModelFactory
                .CreateCartItemQuantityChangedViewModel(cartGuid, productGuid);
            return View("CartItemQuantityChanged", viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> RemoveCart(Guid cartGuid)
        {
            var validationResult = _validator.ValidateRemoveCart(cartGuid, GetUserId());

            if (!validationResult.IsValid)
            {
                return View("SystemError", new SystemErrorViewModel
                {
                    ErrorMessage = validationResult.ErrorMessage
                });
            }

            await _commandSender.SendCommand(new DisableCart()
            {
                CartId = cartGuid
            });

            return RedirectToAction("Index", "Product");
        }

        [HttpPost]
        public async Task<ActionResult> CheckOut(Guid cartGuid)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account",
                    new { returnUrl = Url.Action("CartDetails", "Cart", null) });
            }

            Guid userId = GetUserId();

            var validationResult = _validator.ValidateCheckOut(cartGuid, userId);

            if (!validationResult.IsValid)
            {
                return View("SystemError", new SystemErrorViewModel()
                {
                    ErrorMessage = validationResult.ErrorMessage
                });
            }

            await _commandSender.SendCommand(new CheckoutCart()
            {
                CartId = cartGuid
            });

            return RedirectToAction("CheckOutCompleted", "Cart");
        }

        [HttpGet]
        public ActionResult CheckOutCompleted()
        {
            return View();
        }

        private Guid GetUserId()
        {
            Guid userGuid;
            if (User.Identity.IsAuthenticated)
            {
                ApplicationUser user = System.Web.HttpContext.Current
                    .GetOwinContext()
                    .GetUserManager<ApplicationUserManager>()
                    .FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

                userGuid = user.UserId;
            }
            else
            {
                var cookie = HttpContext.Request.Cookies.Get(userIdGuidCookieName);

                if (cookie == null)
                {
                    userGuid = Guid.NewGuid();
                    HttpContext.Response.SetCookie(new System.Web.HttpCookie(userIdGuidCookieName, userGuid.ToString()));
                }
                else
                {
                    userGuid = new Guid(cookie.Value);
                }
            }

            return userGuid;
        }

        private Guid GetActiveCartGuid(Guid userGuid)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                var cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.UserGuid == userGuid)
                    .SingleOrDefault();

                return cartDetails != null
                    ? cartDetails.CartGuid
                    : Guid.Empty;
            }
        }
    }
}