﻿using Customer.Web.Models;
using Customer.Web.ViewModelFactories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Web;
using System.Web.Mvc;

namespace Customer.Web.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        OrderViewModelFactory _orderViewModelFactory = new OrderViewModelFactory();

        public ActionResult OrderList()
        {
            var userGuid = GetUserGuid();

            var viewModel = _orderViewModelFactory.CreateOrderListViewModel(userGuid);
            
            return View(viewModel);
        }

        public ActionResult OrderDetails(Guid orderGuid)
        {
            var userGuid = GetUserGuid();

            var viewModel = _orderViewModelFactory.CreateOrderDetailsViewModel(orderGuid, userGuid);

            return View(viewModel);
        }

        private Guid GetUserGuid()
        {
            ApplicationUser user = System.Web.HttpContext.Current
                .GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                .FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            var userGuid = user.UserId;

            return userGuid;
        }
    }
}