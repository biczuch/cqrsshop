﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Customer.Web.Startup))]
namespace Customer.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
