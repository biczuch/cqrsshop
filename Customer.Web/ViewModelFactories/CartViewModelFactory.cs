﻿using Customer.Web.Models.Cart;
using Customer.Web.Models.Product;
using NHibernate;
using QueryRepository;
using QueryRepository.Model.Cart;
using QueryRepository.Model.Product;
using System;
using System.Linq;

namespace Customer.Web.ViewModelFactories
{
    public class CartViewModelFactory
    {
        ISessionFactory _sessionFactory = QueryRepositorySessionFactory.GetInstance();

        internal CartDetailsViewModel CreateCartDetailsViewModel(Guid userGuid)
        {
            CartDetails cartDetails;
            using (var session = _sessionFactory.OpenSession())
            {
                cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.UserGuid == userGuid)
                    .SingleOrDefault();
            }

            if (cartDetails == null)
            {
                return new CartDetailsViewModel();
            }

            CartDetailsViewModel viewModel = new CartDetailsViewModel()
            {
                CartGuid = cartDetails.CartGuid,
                CartItems = cartDetails.CartItems
                    .Select(i => new CartItemDetailsViewModel()
                    {
                        Price = i.Price,
                        ProductGuid = i.ProductGuid,
                        ProductName = i.ProductName,
                        Quantity = i.Quantity
                    })
                    .ToArray()
            };

            return viewModel;
        }

        internal CartItemRemovedViewModel CreateCartDetailsRemovedViewModel(Guid cartGuid, Guid productGuid)
        {
            CartItemRemovedViewModel viewModel = new CartItemRemovedViewModel();

            using (var session = _sessionFactory.OpenSession())
            {
                var removedProduct = session.QueryOver<ProductDetails>()
                    .Where(c => c.ProductGuid == productGuid)
                    .SingleOrDefault();

                var suggestedProducts = session.QueryOver<ProductListEntry>()
                    .Where(c => c.SalePlatform == removedProduct.SalePlatform)
                    .Where(c => c.GameGenre == removedProduct.GameGenre)
                    .Where(c => c.ProductGuid != removedProduct.ProductGuid)
                    .Take(5)
                    .List();

                viewModel.ProductsSuggestion = suggestedProducts
                    .Select(p => new ProductListEntryViewModel(p))
                    .ToArray();

                var cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == cartGuid)
                    .SingleOrDefault();

                if(cartDetails != null)
                {
                    viewModel.NumberOfProductsInCart = cartDetails.CartItems
                        .Where(p => p.ProductGuid != productGuid)
                        .Count();
                }
            }

            return viewModel;
        }

        internal ChangeCartItemQuantityViewModel CreateChangeCartItemQuantity(Guid cartGuid, Guid productGuid)
        {
            ChangeCartItemQuantityViewModel viewModel = new ChangeCartItemQuantityViewModel()
            {
                CartGuid = cartGuid,
                ProductGuid = productGuid
            };

            using (var session = _sessionFactory.OpenSession())
            {
                var product = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == productGuid)
                    .SingleOrDefault();

                CartDetails cart = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == cartGuid)
                    .SingleOrDefault();

                CartItem productItem = cart.CartItems
                    .SingleOrDefault(i => i.ProductGuid == productGuid);

                viewModel.NewQuantity = productItem.Quantity;
                viewModel.ProductName = product.Name;
                viewModel.Thumbnail = product.ThumbnailUri;
            }

            return viewModel;
        }

        internal CartItemQuantityChangedViewModel CreateCartItemQuantityChangedViewModel(Guid cartGuid, Guid productGuid)
        {
            CartItemQuantityChangedViewModel viewModel = new CartItemQuantityChangedViewModel();

            using (var session = _sessionFactory.OpenSession())
            {
                var product = session.QueryOver<ProductDetails>()
                    .Where(c => c.ProductGuid == productGuid)
                    .SingleOrDefault();

                var suggestedProducts = session.QueryOver<ProductListEntry>()
                    .Where(c => c.SalePlatform == product.SalePlatform)
                    .Where(c => c.GameGenre == product.GameGenre)
                    .Where(c => c.ProductGuid != product.ProductGuid)
                    .Take(5)
                    .List();

                viewModel.ProductsSuggestion = suggestedProducts
                    .Select(p => new ProductListEntryViewModel(p))
                    .ToArray();

                var cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == cartGuid)
                    .SingleOrDefault();

                viewModel.NumberOfProductsInCart = cartDetails.CartItems.Count;
            }

            return viewModel;
        }
    }
}