﻿using Customer.Web.Models.Product;
using NHibernate;
using PagedList;
using QueryRepository;
using QueryRepository.Model.Product;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Customer.Web.ViewModelFactories
{
    public class ProductViewModelFactory
    {
        ISessionFactory _queryRepository = QueryRepositorySessionFactory.GetInstance();

        internal ProductListViewModel CreteProductListViewModel(ProductSearchFilterViewModel filterViewModel,
            int page, int itemsPerPage)
        {
            IList<ProductListEntry> products;
            using (var session = _queryRepository.OpenSession())
            {
                var productsQuery = session.QueryOver<ProductListEntry>()
                    .Where(p => p.IsActive);
                if (!string.IsNullOrWhiteSpace(filterViewModel.ProductName))
                {
                    productsQuery.WhereRestrictionOn(p => p.Name).IsLike($"%{ filterViewModel.ProductName }%");
                }
                if (filterViewModel.GameGenre.HasValue)
                {
                    productsQuery.Where(p => p.GameGenre == filterViewModel.GameGenre);
                }
                if (filterViewModel.SalePlatform.HasValue)
                {
                    productsQuery.Where(p => p.SalePlatform == filterViewModel.SalePlatform);
                }
                if (filterViewModel.MaxPrice > 0)
                {
                    productsQuery.Where(p => p.Price <= filterViewModel.MaxPrice);
                }
                products = productsQuery.OrderBy(p => p.Name).Asc.List();
            }

            ProductListViewModel viewModel = new ProductListViewModel()
            {
                Results = products
                .Select(p => new ProductListEntryViewModel(p))
                .ToPagedList(page, itemsPerPage),
                SearchFilter = filterViewModel ?? new ProductSearchFilterViewModel()
            };

            return viewModel;
        }

        internal ProductDetailsViewModel CreateProductDetailsViewModel(Guid productGuid)
        {
            using (var session = _queryRepository.OpenSession())
            {
                var productDetails = session.QueryOver<ProductDetails>()
                    .Where(p => p.ProductGuid == productGuid)
                    .SingleOrDefault();


                ProductDetailsViewModel viewModel = new ProductDetailsViewModel()
                {
                    Name = productDetails.ProductName,
                    ProductGuid = productDetails.ProductGuid,
                    Description = productDetails.Description,
                    Price = productDetails.Price,
                    GameGenre = productDetails.GameGenre,
                    SalePlatform = productDetails.SalePlatform,
                    ImageUris = productDetails.ImageUris.Skip(1),
                    ImageThumbnail = productDetails.ImageUris.Any() ? productDetails.ImageUris.First() : null
                };

                return viewModel;
            }
        }
    }
}