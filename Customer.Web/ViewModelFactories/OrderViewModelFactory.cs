﻿using Customer.Web.Models.Order;
using NHibernate;
using QueryRepository;
using QueryRepository.Model.Order;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Customer.Web.ViewModelFactories
{
    public class OrderViewModelFactory
    {
        ISessionFactory _sessionFactory = QueryRepositorySessionFactory.GetInstance();

        internal List<OrderListEntryViewModel> CreateOrderListViewModel(Guid userGuid)
        {
            List<OrderListEntryViewModel> orderList = new List<OrderListEntryViewModel>();

            using (var session = _sessionFactory.OpenSession())
            {
                var orders = session.QueryOver<OrderListEntry>()
                    .Where(o => o.UserId == userGuid)
                    .List();

                foreach (var order in orders)
                {
                    OrderListEntryViewModel orderViewModel = new OrderListEntryViewModel()
                    {
                        OrderGuid = order.OrderGuid,
                        Status = order.Status,
                        DateCompleted = order.DateCompleted,
                        DatePlaced = order.DatePlaced,
                        IsCancelled = order.IsCancelled,
                        TotalPrice = order.OrderTotalPrice
                    };

                    foreach (var product in order.OrderListEntryProducts)
                    {
                        var productViewModel = new OrderListProductViewModel()
                        {
                            ProductName = product.ProductName,
                            Quantity = product.Quantity
                        };

                        orderViewModel.Products.Add(productViewModel);
                    }
                    orderList.Add(orderViewModel);
                }
            }

            return orderList;
        }

        internal OrderDetailsViewModel CreateOrderDetailsViewModel(Guid orderGuid, Guid userGuid)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                var order = session.QueryOver<OrderDetails>()
                    .Where(o => o.OrderGuid == orderGuid)
                    .SingleOrDefault();

                if (order.UserId != userGuid)
                    throw new InvalidOperationException();

                var viewModel = new OrderDetailsViewModel()
                {
                    OrderGuid = order.OrderGuid,
                    DateCompleted = order.DateCompleted.Value,
                    DatePlaced = order.DatePlaced.Value,
                    TotalPrice = order.TotalPrice,
                    Products = order.OrderDetailsProducts.Select(p => new OrderDetailsProductViewModel()
                    {
                        ProductName = p.ProductName,
                        Codes = p.Codes.ToList(),
                        ImageUri = p.ThumbnailUri
                    }).ToList()
                };

                return viewModel;
            }
        }
    }
}