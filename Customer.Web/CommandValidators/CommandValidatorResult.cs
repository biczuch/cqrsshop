﻿namespace Customer.Web.CommandValidators
{
    public class CommandValidatorResult
    {
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }

        public static CommandValidatorResult Success
        {
            get
            {
                return new CommandValidatorResult()
                {
                    IsValid = true
                };
            }
        }
    }
}