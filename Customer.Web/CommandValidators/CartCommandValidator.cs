﻿using System.Linq;
using QueryRepository;
using QueryRepository.Model.Product;
using System;
using NHibernate;
using QueryRepository.Model.Cart;

namespace Customer.Web.CommandValidators
{
    public class CartCommandValidator
    {
        ISessionFactory _queryRepository = QueryRepositorySessionFactory.GetInstance();

        internal CommandValidatorResult ValidateAddProductToCart(Guid productId)
        {
            ProductListEntry product;
            using (var session = _queryRepository.OpenSession())
            {
                product = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == productId)
                    .List()
                    .First();
            }

            var commandValidatorResult = new CommandValidatorResult()
            {
                IsValid = true
            };

            if (product == null)
            {
                commandValidatorResult.IsValid = false;
                commandValidatorResult.ErrorMessage = "Product not found.";
            }

            if (product.IsActive == false)
            {
                commandValidatorResult.IsValid = false;
                commandValidatorResult.ErrorMessage = "Product can't be sold.";
            }

            return commandValidatorResult;
        }

        internal CommandValidatorResult ValidateRemoveFromCart(Guid cartId, Guid productId)
        {
            using (var session = _queryRepository.OpenSession())
            {
                var cart = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == cartId)
                    .SingleOrDefault();

                if (cart == null)
                {
                    return new CommandValidatorResult()
                    {
                        IsValid = false,
                        ErrorMessage = "Cart not found"
                    };
                }

                if (!cart.CartItems.Any(c => c.ProductGuid == productId))
                {
                    return new CommandValidatorResult()
                    {
                        IsValid = false,
                        ErrorMessage = "Product is not in cart."
                    };
                }
            }

            return CommandValidatorResult.Success;
        }

        internal CommandValidatorResult ValidateRemoveCart(Guid cartId, Guid userGuid)
        {

            using (var session = _queryRepository.OpenSession())
            {
                var cart = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == cartId)
                    .SingleOrDefault();

                if (cart == null)
                {
                    return new CommandValidatorResult()
                    {
                        ErrorMessage = "Cart not found"
                    };
                }

                if(cart.UserGuid != userGuid)
                {
                    return new CommandValidatorResult()
                    {
                        ErrorMessage = "Bad user"
                    };
                }
            };

            return CommandValidatorResult.Success;
        }

        internal CommandValidatorResult ValidateCheckOut(Guid cartId, Guid userId)
        {
            using (var session = _queryRepository.OpenSession())
            {
                var cart = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == cartId)
                    .SingleOrDefault();

                if (cart == null)
                {
                    return new CommandValidatorResult()
                    {
                        ErrorMessage = "Cart not found"
                    };
                }

                if (cart.UserGuid != userId)
                {
                    return new CommandValidatorResult()
                    {
                        ErrorMessage = "Bad user"
                    };
                }
            };

            return CommandValidatorResult.Success;
        }
    }
}