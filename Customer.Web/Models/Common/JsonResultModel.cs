﻿namespace Customer.Web.Models
{
    public class JsonResultModel
    {
        public bool Success { get; set; } = false;
        public string ErrorMsg { get; set; }
        public object Data { get; set; }
    }
}