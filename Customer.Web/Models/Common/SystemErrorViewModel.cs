﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Customer.Web.Models
{
    public class SystemErrorViewModel
    {
        public string ErrorMessage { get; set; }
    }
}