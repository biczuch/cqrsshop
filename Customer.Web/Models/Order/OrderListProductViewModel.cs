﻿namespace Customer.Web.Models.Order
{
    public class OrderListProductViewModel
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }
}