﻿using SharedKernel;
using System.ComponentModel;

namespace Customer.Web.Models.Product
{
    public class ProductSearchFilterViewModel
    {
        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [DisplayName("Game genre")]
        public GameGenre? GameGenre { get; set; }

        [DisplayName("Sale platform")]
        public SalePlatform? SalePlatform { get; set; }

        [DisplayName("Maximum price")]
        public decimal? MaxPrice { get; set; }
    }
}