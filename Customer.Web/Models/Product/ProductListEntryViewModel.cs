﻿using QueryRepository.Model.Product;
using SharedKernel;
using System;
using System.ComponentModel.DataAnnotations;

namespace Customer.Web.Models.Product
{
    public class ProductListEntryViewModel
    {
        public Guid ProductGuid { get; set; }
        public string ProductName { get; set; }
        public GameGenre GameGenre { get; set; }
        public SalePlatform SalePlatform { get; set; }
        public string ShortDescription { get; set; }

        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public string ImageUri { get; set; }


        public ProductListEntryViewModel(ProductListEntry p)
        {
            ProductGuid = p.ProductGuid;
            ProductName = p.Name;
            SalePlatform = p.SalePlatform;
            GameGenre = p.GameGenre;
            Price = p.Price;
            ShortDescription = p.ShortDescription;
            ImageUri = p.ThumbnailUri;
        }
    }
}