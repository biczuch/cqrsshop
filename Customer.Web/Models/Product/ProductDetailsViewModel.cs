﻿using SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Customer.Web.Models.Product
{
    public class ProductDetailsViewModel
    {
        public Guid ProductGuid { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public GameGenre GameGenre { get; set; }

        public SalePlatform SalePlatform { get; set; }

        public IEnumerable<string> ImageUris { get; set; }

        public string ImageThumbnail { get; set; }
    }
}