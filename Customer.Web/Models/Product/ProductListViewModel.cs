﻿using PagedList;

namespace Customer.Web.Models.Product
{
    public class ProductListViewModel
    {
        public ProductSearchFilterViewModel SearchFilter { get; set; }
        public IPagedList<ProductListEntryViewModel> Results { get; set; }
    }
}