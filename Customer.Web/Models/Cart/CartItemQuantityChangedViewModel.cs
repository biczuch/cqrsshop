﻿using Customer.Web.Models.Product;

namespace Customer.Web.Models.Cart
{
    public class CartItemQuantityChangedViewModel
    {
        public int NumberOfProductsInCart { get; set; }
        public ProductListEntryViewModel[] ProductsSuggestion { get; set; }
    }
}