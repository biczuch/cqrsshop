﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Customer.Web.Models.Cart
{
    public class CartDetailsViewModel
    {
        public Guid CartId { get; set; }
        public Guid CartGuid { get; set; }
        public CartItemDetailsViewModel[] CartItems { get; set; }

        public decimal TotalQuantity
        {
            get
            {
                if (CartItems == null)
                    return 0;

                return CartItems.Sum(c => c.Quantity);
            }
        }

        [DataType(DataType.Currency)]
        public decimal TotalPrice
        {
            get
            {
                if (CartItems == null)
                    return 0;

                return CartItems.Sum(c => c.Price * c.Quantity);
            }
        }
    }
}