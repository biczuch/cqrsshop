﻿using System;

namespace Customer.Web.Models.Cart
{
    public class ProductAddedViewModel
    {
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
    }
}