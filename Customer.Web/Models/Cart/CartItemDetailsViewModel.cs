﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Customer.Web.Models.Cart
{
    public class CartItemDetailsViewModel
    {
        public Guid ProductGuid { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalPrice
        {
            get
            {
                return Quantity * Price;
            }
        }
    }
}