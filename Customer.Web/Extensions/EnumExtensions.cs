﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Customer.Web.Extensions
{
    public static class EnumExtensions
    {
        public static string GetEnumDescription(this Enum enumValue)
        {
            FieldInfo fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
            DescriptionAttribute descriptionAttr = (DescriptionAttribute)fieldInfo
                .GetCustomAttribute(typeof(DescriptionAttribute));

            if (descriptionAttr != null)
            {
                return descriptionAttr.Description;
            }

            return enumValue.ToString();
        }
    }
}