﻿using NHibernate;
using Orders.Commands.Cart;
using QueryRepository;
using QueryRepository.Model.Cart;
using System;

namespace Customer.Web.CommandBuilders
{
    public class CartCommandBuilder
    {
        ISessionFactory _sessionFactory = QueryRepositorySessionFactory.GetInstance();

        internal SwapCarts BuidSwapCartsCommand(Guid userId, Guid cookieUserId)
        {
            SwapCarts command = new SwapCarts();

            using (var session = _sessionFactory.OpenSession())
            {
                CartDetails cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.UserGuid == userId)
                    .SingleOrDefault();

                CartDetails cookieUserCartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.UserGuid == cookieUserId)
                    .SingleOrDefault();

                command.CartToAssign = cookieUserCartDetails.CartGuid;
                command.CartToRemove = cartDetails?.CartGuid ?? Guid.Empty;
                command.NewUserId = userId;
            }

            return command;
        }
    }
}