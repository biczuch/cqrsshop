﻿using DddCommon;
using ProductManagement.Contracts.Product;
using ProductManagement.Core.Repositories;

namespace ProductManagement.Core.Services
{
    public class ProductService
    {
        ProductRepository _repository;
        public ProductService()
        {
            _repository = new ProductRepository();
        }

        public void AddProduct(Product product)
        {
            _repository.Save(product);

            ProductAddedEvent productAddedEvent = new ProductAddedEvent()
            {
                Description = product.Description,
                ShortDescription = product.ShortDescription,
                ProductId = product.Id,
                ProductName = product.ProductName,
                SalePlatform = product.SalePlatform,
                UnitPrice = product.UnitPrice.Amount,
                MarginPercentage = product.MarginPercentage,
                ImageUris = product.GetImagesUris(),
                GameGenre = product.GameGenre,
                IsActive = product.IsActive
            };

            DomainEvent.Raise(productAddedEvent);
        }
    }
}
