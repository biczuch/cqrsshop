﻿using DddCommon;
using NHibernate;
using ProductManagement.Core.Persistance;
using System.Collections.Generic;

namespace ProductManagement.Core.Repositories
{
    public class ProductRepository : Repository<Product>
    {
        public IList<Product> GetAllProducts()
        {
            using (ISession session = _sessionFactory.OpenSession())
            {
                return session.QueryOver<Product>().List();
            }
        }

        protected override ISessionFactory GetSessionFactory()
        {
            return ProductManagementSessionFactory.GetInstance();
        }
    }
}
