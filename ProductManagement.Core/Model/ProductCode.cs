﻿using DddCommon;
using DddCommon.CommonValueObjects;
using SharedKernel;

namespace ProductManagement.Core
{
    public class ProductCode : Entity
    {
        public virtual string Code { get; protected set; }
        public virtual SalePlatform SalePlatform { get; protected set; }
        public virtual bool Sold { get; protected set; } = false;

        public virtual Money SoldPrice { get; protected set; }
        public virtual decimal SoldMarginPercentage { get; protected set; }
        protected ProductCode() { }

        public ProductCode(string code, SalePlatform salePlatform)
        {
            Code = code;
            SalePlatform = salePlatform;
        }

        public virtual void MarkAsSold(Money soldPrice, decimal marginPercentage)
        {
            Sold = true;
            SoldPrice = soldPrice;
            SoldMarginPercentage = marginPercentage;
        }

        public virtual Money GetEarnings()
        {
            return new Money(SoldPrice.Amount * SoldMarginPercentage);
        }
    }
}
