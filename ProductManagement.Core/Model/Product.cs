﻿using DddCommon;
using System;
using System.Linq;
using System.Collections.Generic;
using SharedKernel;
using ProductManagement.Contracts.Product;
using DddCommon.CommonValueObjects;

namespace ProductManagement.Core
{
    public class Product : AggregateRoot
    {
        public virtual string ProductName { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual string ShortDescription { get; protected set; }
        public virtual SalePlatform SalePlatform { get; protected set; }
        public virtual GameGenre GameGenre { get; protected set; }

        public virtual Money UnitPrice { get; protected set; }
        public virtual decimal MarginPercentage { get; protected set; }

        public virtual bool IsActive { get; protected set; }

        protected virtual ICollection<ProductCode> Codes { get; set; }

        protected virtual ICollection<string> ImageUriCollection { get; set; }

        public virtual Money TotalPrice
        {
            get
            {
                return UnitPrice + UnitPrice.Amount * MarginPercentage;
            }
        }
        protected Product() { }

        public Product(string productName, SalePlatform salePlatform, Money totalPrice, GameGenre gameGenre)
        {
            ProductName = productName;
            SalePlatform = salePlatform;
            UnitPrice = GetUnitPrice(totalPrice);
            MarginPercentage = GetStandardMarginPercentage(UnitPrice, totalPrice);
            Codes = new HashSet<ProductCode>();
            GameGenre = gameGenre;
            ImageUriCollection = new List<string>();
        }

        public Product(string productName, SalePlatform salePlatform, Money unitPrice, GameGenre gameGenre,
            string shortDescription, string description, IEnumerable<string> imageUris)
            : this(productName, salePlatform, unitPrice, gameGenre)
        {
            Description = description;
            ShortDescription = shortDescription;
            foreach (var imageUri in imageUris)
            {
                ImageUriCollection.Add(imageUri);
            }
        }

        public virtual void AddCodes(ProductCode[] productCodes)
        {
            List<NewCode> newCodes = new List<NewCode>();
            foreach (ProductCode productCode in productCodes)
            {
                if (productCode.SalePlatform != SalePlatform)
                    throw new ArgumentException("Can't add product code when Sale Platform does not match.");

                Codes.Add(productCode);

                newCodes.Add(new NewCode()
                {
                    Code = productCode.Code,
                    CodeId = productCode.Id
                });
            }

            AddDomainEvent(new NewCodeAddedToProductEvent()
            {
                NewCodes = newCodes.ToArray(),
                ProductId = this.Id
            });
        }

        public virtual void AddCode(ProductCode[] productCodes)
        {
            AddCode(productCodes);
        }

        public virtual int GetStockAmount()
        {
            return Codes.Count;
        }

        public virtual int GetSoldAmount()
        {
            return Codes.Count(c => c.Sold);
        }

        public virtual int GetAvailableAmount()
        {
            return Codes.Count(c => c.Sold == false);
        }

        public virtual void ActivateProduct()
        {
            if (IsActive == true)
                throw new InvalidOperationException("Product is already active.");

            IsActive = true;

            AddDomainEvent(new ProductActivatedEvent()
            {
                ProductId = Id
            });
        }

        public virtual void DeactivateProduct()
        {
            if (IsActive == false)
                throw new InvalidOperationException("Product is already inactive.");

            IsActive = false;

            AddDomainEvent(new ProductDeactivatedEvent()
            {
                ProductId = Id
            });
        }

        public virtual decimal GetMarginPercentageBasedOnExpectedIncome(Money unitPrice, Money expectedIncome)
        {
            var priceTotal = unitPrice + expectedIncome;

            return GetStandardMarginPercentage(unitPrice, priceTotal);
        }

        public virtual Money GetEarnings()
        {
            IEnumerable<ProductCode> soldCodes = Codes.Where(c => c.Sold);
            decimal earnings = soldCodes.Sum(c => c.GetEarnings().Amount);

            return new Money(earnings);
        }

        public virtual Money GetMarginInMoney()
        {
            var marginInMoney = UnitPrice.Amount * MarginPercentage;

            return new Money(marginInMoney);
        }

        public virtual Money GetFutureEarnings()
        {
            int availableAmount = GetAvailableAmount();

            var futureEarnings = availableAmount * MarginPercentage;

            return new Money(futureEarnings);
        }

        public virtual string[] GetImagesUris()
        {
            return ImageUriCollection.ToArray();
        }

        public virtual IEnumerable<ProductCode> GetCodes()
        {
            return Codes.AsEnumerable();
        }

        public virtual void ChangeInformation(string productName, GameGenre gameGenre,
            string description, string shortDescription, ICollection<string> imageUris)
        {
            ProductName = productName;
            GameGenre = gameGenre;
            Description = description;
            ShortDescription = shortDescription;

            ImageUriCollection.Clear();
            foreach (var imageUri in imageUris)
            {
                ImageUriCollection.Add(imageUri);
            }

            AddDomainEvent(new ProductInformationChangedEvent()
            {
                ProductId = Id,
                Description = Description,
                ProductName = ProductName,
                ShortDescription = ShortDescription,
                GameGenre = GameGenre,
                ImageUris = ImageUriCollection.ToList()
            });
        }

        public virtual void ChangePrice(Money unitPrice, decimal marginPercentage)
        {
            UnitPrice = unitPrice;
            MarginPercentage = marginPercentage;

            AddDomainEvent(new ProductPriceChangedEvent()
            {
                ProductId = Id,
                UnitPrice = UnitPrice.Amount,
                MarginPercentage = MarginPercentage
            });
        }

        public virtual void MarkCodeAsSold(Guid codeId, decimal soldPrice, decimal marginPercentage)
        {
            var code = this.Codes.First(c => c.Id == codeId);
            code.MarkAsSold(new Money(soldPrice), marginPercentage);
        }

        private Money GetUnitPrice(Money totalPrice)
        {
            const decimal standardPercentage = 0.1m;
            decimal unitPrice = totalPrice.Amount - standardPercentage * totalPrice.Amount;
            return new Money(unitPrice);
        }

        private decimal GetStandardMarginPercentage(Money unitPrice, Money totalPrice)
        {
            decimal expectedIncome = (totalPrice - unitPrice).Amount;

            decimal marginPercentage = (expectedIncome / unitPrice.Amount);

            return marginPercentage;
        }
    }
}