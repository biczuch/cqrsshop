﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using SharedKernel;

namespace ProductManagement.Core.Persistance
{
    public class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Id(x => x.Id).GeneratedBy.Assigned();

            Map(x => x.ProductName);
            Map(x => x.SalePlatform);
            Map(x => x.GameGenre)
                .CustomType(typeof(GameGenre))
                .Not.Nullable();
            Map(x => x.Description).Length(4001).Nullable();
            Map(x => x.ShortDescription).Length(4001).Nullable();
            Map(x => x.IsActive);
            
            Map(x => x.MarginPercentage);

            Component(x => x.UnitPrice, y =>
            {
                y.Map(x => x.Amount);
                y.Map(x => x.Currency);
            }); 

            HasMany<ProductCode>(Reveal.Member<Product>("Codes")).Cascade.All().Not.LazyLoad();
            HasMany<string>(Reveal.Member<Product>("ImageUriCollection"))
                .KeyColumn("ImageId")
                .Table("ImageUri")
                .Element("Uri", x => x.Length(4001))
                .Not.LazyLoad();
        }
    }
}
