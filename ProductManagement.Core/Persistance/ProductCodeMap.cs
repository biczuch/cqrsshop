﻿using FluentNHibernate.Mapping;

namespace ProductManagement.Core.Persistance
{
    public class ProductCodeMap : ClassMap<ProductCode>
    {
        public ProductCodeMap()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Code);
            Map(x => x.Sold);
            Map(x => x.SalePlatform);
            Map(x => x.SoldMarginPercentage);

            Component(x => x.SoldPrice, y =>
            {
                y.Map(x => x.Amount).Nullable();
                y.Map(x => x.Currency).Nullable();
            });
        }
    }
}
