﻿using DddCommon;
using NHibernate.Event;

namespace ProductManagement.Core.Persistance
{
    public class EventListener :
        IPostInsertEventListener,
        IPostDeleteEventListener,
        IPostUpdateEventListener,
        IPostCollectionUpdateEventListener
    {
        public void OnPostDelete(PostDeleteEvent @event)
        {
            AggregateRoot aggregateRoot = @event.Entity as AggregateRoot;
            if (aggregateRoot != null)
            {
                DispatchEvents(aggregateRoot);
            }
        }

        public void OnPostInsert(PostInsertEvent @event)
        {
            AggregateRoot aggregateRoot = @event.Entity as AggregateRoot;
            if (aggregateRoot != null)
            {
                DispatchEvents(aggregateRoot);
            }
        }

        public void OnPostUpdate(PostUpdateEvent @event)
        {
            AggregateRoot aggregateRoot = @event.Entity as AggregateRoot;

            if (aggregateRoot == null && @event.Entity is IRootAware)
            {
                IRootAware rootAware = @event.Entity as IRootAware;
                aggregateRoot = rootAware.RootEntity as AggregateRoot;
            }
            if (aggregateRoot != null)
            {
                DispatchEvents(aggregateRoot);
            }
        }

        public void OnPostUpdateCollection(PostCollectionUpdateEvent @event)
        {
            AggregateRoot aggregateRoot = @event.AffectedOwnerOrNull as AggregateRoot;
            if (aggregateRoot != null)
            {
                DispatchEvents(aggregateRoot);
            }
        }

        private void DispatchEvents(AggregateRoot aggregateRoot)
        {
            foreach (IDomainEvent @event in aggregateRoot.DomainEvents)
            {
                DomainEvent.Raise(@event);
            }

            aggregateRoot.ClearEvents();
        }
    }
}
