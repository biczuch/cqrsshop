﻿using System.ComponentModel;

namespace Orders.Common
{
    public enum OrderStatus
    {
        [Description("Pending")]
        Pending,
        [Description("Submitted")]
        Submitted,
        [Description("Completed")]
        Completed,
        [Description("Cancelled")]
        Cancelled,
    }
}
