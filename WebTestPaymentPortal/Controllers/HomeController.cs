﻿using MassTransit;
using Orders.Contracts.Order;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebTestPaymentPortal.Models;

namespace WebTestPaymentPortal.Controllers
{
    public class HomeController : Controller
    {
        IBusControl _busControl;

        public HomeController()
        {
            _busControl = MvcApplication.BusControl;
        }

        public ActionResult Index(bool success = false)
        {
            if(success)
            {
                TempData["success"] = true;
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> MakePayment(PaymentViewModel model)
        {
            if(!ModelState.IsValid)
            {
                return View("Index", model);
            }

            await _busControl.Publish(new PaymentMadeEvent()
            {
                OrderId = model.CartId.Value,
                PaymentAmount = model.Amount.Value
            });

            return RedirectToAction("Index", new { success = true });
            
        }
    }
}