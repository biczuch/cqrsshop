﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebTestPaymentPortal.Models
{
    public class PaymentViewModel : IValidatableObject
    {
        [Required]
        public Guid? CartId { get; set; }
        [Required]
        public decimal? Amount { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(Amount <= 0)
            {
                yield return new ValidationResult("Amount must have positive value.");
            }

            yield return ValidationResult.Success;
        }
    }
}