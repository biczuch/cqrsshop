﻿namespace OrdersManagementService.App_Code
{
    public class Startup
    {
        public static void AppInitialize()
        {
            NServiceBusConfig.Configure();
        }
    }
}