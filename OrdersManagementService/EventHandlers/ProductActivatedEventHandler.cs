﻿using NServiceBus;
using Orders.Core.Repositories;
using ProductManagement.Contracts.Product;
using System;
using System.Threading.Tasks;

namespace OrdersManagementService.EventHandlers
{
    public class ProductActivatedEventHandler : IHandleMessages<ProductActivatedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Handle(ProductActivatedEvent message, IMessageHandlerContext context)
        {
            var product = productRepository.GetById(message.ProductId);
            product.ActivateProduct();

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}