﻿using System;
using System.Threading.Tasks;
using NServiceBus;
using ProductManagement.Contracts.Product;
using Orders.Core.Repositories;
using DddCommon.CommonValueObjects;

namespace OrdersManagementService.EventHandlers
{
    public class ProductPriceChangedEventHandler : IHandleMessages<ProductPriceChangedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Handle(ProductPriceChangedEvent message, IMessageHandlerContext context)
        {
            var product = productRepository.GetById(message.ProductId);
            product.ChangePrice(new Money(message.TotalPrice));

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}