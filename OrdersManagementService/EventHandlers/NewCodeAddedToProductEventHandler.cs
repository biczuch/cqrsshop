﻿using NServiceBus;
using Orders.Core;
using Orders.Core.Repositories;
using ProductManagement.Contracts.Product;
using System;
using System.Threading.Tasks;

namespace OrdersManagementService.EventHandlers
{
    public class NewCodeAddedToProductEventHandler : IHandleMessages<NewCodeAddedToProductEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Handle(NewCodeAddedToProductEvent message, IMessageHandlerContext context)
        {
            Product product = productRepository.GetById(message.ProductId);
            product.AddNewCode(message.CodeId, message.Code);

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}