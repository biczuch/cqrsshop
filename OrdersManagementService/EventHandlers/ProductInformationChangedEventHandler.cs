﻿using System;
using System.Threading.Tasks;
using NServiceBus;
using ProductManagement.Contracts.Product;
using Orders.Core.Repositories;

namespace OrdersManagementService.EventHandlers
{
    public class ProductInformationChangedEventHandler : IHandleMessages<ProductInformationChangedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Handle(ProductInformationChangedEvent message, IMessageHandlerContext context)
        {
            var product = productRepository.GetById(message.ProductId);
            product.ChangeInformation(
                message.ProductName,
                message.GameGenre,
                message.Description,
                message.ShortDescription,
                message.ImageUris
                );

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}