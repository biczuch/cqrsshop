﻿using System.Threading.Tasks;
using NServiceBus;
using ProductManagement.Contracts.Product;
using Orders.Core;
using Orders.Core.Repositories;

namespace OrdersManagementService.EventHandlers
{
    public class ProductAddedEventHandler : IHandleMessages<ProductAddedEvent>
    {
        private ProductRepository _productRepository = new ProductRepository();

        public Task Handle(ProductAddedEvent message, IMessageHandlerContext context)
        {
            Product product = new Product(
                message.ProductId,
                message.ProductName,
                message.SalePlatform,
                message.ShortDescription,
                message.Description,
                message.UnitPrice,
                message.ImageUris,
                message.GameGenre,
                message.IsActive
                );

            _productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}