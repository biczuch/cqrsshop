﻿using NServiceBus;
using Orders.Core.Repositories;
using ProductManagement.Contracts.Product;
using System;
using System.Threading.Tasks;

namespace OrdersManagementService.EventHandlers
{
    public class ProductDeactivatedEventHandler : IHandleMessages<ProductDeactivatedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Handle(ProductDeactivatedEvent message, IMessageHandlerContext context)
        {
            var product = productRepository.GetById(message.ProductId);
            product.DeactivateProduct();

            productRepository.Save(product);

            return Task.CompletedTask;
        }
    }
}