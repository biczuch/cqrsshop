﻿using MassTransit;
using NHibernate;
using Orders.Contracts.Product;
using QueryRepository;
using QueryRepository.Model.Product;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OrdersManagementReadService.EventHandlers.ProductProductEventHandlers
{
    public class ProductListEventHandler :
            IConsumer<ProductCreatedEvent>,
            IConsumer<ProductActivatedEvent>,
            IConsumer<ProductDeactivatedEvent>,
            IConsumer<ProductInformationChangedEvent>,
            IConsumer<ProductPriceChangedEvent>
    {
        ISessionFactory _sessionFactory;

        public ProductListEventHandler()
        {
            _sessionFactory = QueryRepositorySessionFactory.GetInstance(ConfigurationReader.ReadServiceDbConnection);
        }

        public Task Consume(ConsumeContext<ProductDeactivatedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var productEntry = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();

                if (productEntry == null)
                    throw new InvalidOperationException($"Product with GUID = {message.ProductId} not found!");

                productEntry.IsActive = false;

                session.Update(productEntry);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductActivatedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var productEntry = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();

                if (productEntry == null)
                    throw new InvalidOperationException($"Product with GUID = {message.ProductId} not found!");

                productEntry.IsActive = true;

                session.Update(productEntry);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductInformationChangedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var productEntry = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();

                if (productEntry == null)
                    throw new InvalidOperationException($"Product with GUID = {message.ProductId} not found!");

                productEntry.Name = message.ProductName;
                productEntry.ShortDescription = message.ShortDescription;
                productEntry.GameGenre = message.GameGenre;
                productEntry.ThumbnailUri = message.ImageUris.FirstOrDefault();

                session.Update(productEntry);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductPriceChangedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var productEntry = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();

                if (productEntry == null)
                    throw new InvalidOperationException($"Product with GUID = {message.ProductId} not found!");

                productEntry.Price = message.TotalPrice;

                session.Update(productEntry);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductCreatedEvent> context)
        {
            var message = context.Message;

            ProductListEntry listEntry = new ProductListEntry()
            {
                ProductGuid = message.ProductId,
                Name = message.ProductName,
                ShortDescription = message.ShortDescription,
                GameGenre = message.GameGenre,
                SalePlatform = message.SalePlatform,
                Price = message.TotalAmount,
                IsActive = message.IsActive,
                ThumbnailUri = context.Message.ImageUris.FirstOrDefault()
            };

            using (var session = _sessionFactory.OpenSession())
            {
                session.Save(listEntry);
                session.Flush();
            }

            return Task.CompletedTask;
        }
    }
}
