﻿using MassTransit;
using NHibernate;
using Orders.Contracts.Product;
using QueryRepository;
using QueryRepository.Model.Product;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OrdersManagementReadService.EventHandlers.ProductEventHandlers
{
    public class ProductDetailsEventHandler :
            IConsumer<ProductCreatedEvent>,
            IConsumer<ProductInformationChangedEvent>,
            IConsumer<ProductPriceChangedEvent>
    {
        ISessionFactory _sessionFactory;

        public ProductDetailsEventHandler()
        {
            _sessionFactory = QueryRepositorySessionFactory.GetInstance(ConfigurationReader.ReadServiceDbConnection);
        }

        public Task Consume(ConsumeContext<ProductPriceChangedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var productDetails = session.QueryOver<ProductDetails>()
                    .Where(p => p.ProductGuid == message.ProductId).SingleOrDefault();

                if (productDetails == null)
                    throw new InvalidOperationException($"Product with GUID = {message.ProductId} not found!");

                productDetails.Price = message.TotalPrice;

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductInformationChangedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var productDetails = session.QueryOver<ProductDetails>()
                    .Where(p => p.ProductGuid == message.ProductId).SingleOrDefault();

                if (productDetails == null)
                    throw new InvalidOperationException($"Product with GUID = {message.ProductId} not found!");

                productDetails.ImageUris = message.ImageUris;
                productDetails.ProductName = message.ProductName;
                productDetails.Description = message.Description;
                productDetails.GameGenre = message.GameGenre;

                session.SaveOrUpdate(productDetails);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductCreatedEvent> context)
        {
            var message = context.Message;

            ProductDetails productDetails = new ProductDetails()
            {
                ProductGuid = message.ProductId,
                Description = message.Description,
                Price = message.TotalAmount,
                ProductName = message.ProductName,
                GameGenre = message.GameGenre,
                SalePlatform = message.SalePlatform,
                ImageUris = message.ImageUris.ToList()
            };

            using (var session = _sessionFactory.OpenSession())
            {
                session.Save(productDetails);
                session.Flush();
            }
            return Task.CompletedTask;
        }
    }
}
