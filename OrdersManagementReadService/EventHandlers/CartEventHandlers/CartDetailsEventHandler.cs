﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Contracts.Cart;
using Orders.Contracts.Product;
using NHibernate;
using QueryRepository;
using QueryRepository.Model.Cart;
using QueryRepository.Model.Product;
using System.Linq;

namespace OrdersManagementReadService.EventHandlers.CartEventHandlers
{
    public class CartDetailsEventHandler :
            IConsumer<CartCreatedEvent>,
            IConsumer<CartItemAddedEvent>,
            IConsumer<CartItemRemovedEvent>,
            IConsumer<CartItemQuantityChangedEvent>,
            IConsumer<CartDisabledEvent>,
            IConsumer<ProductPriceChangedEvent>,
            IConsumer<CartUserAssignedEvent>
    {
        ISessionFactory _sessionFactory;

        public CartDetailsEventHandler()
        {
            _sessionFactory = QueryRepositorySessionFactory.GetInstance(ConfigurationReader.ReadServiceDbConnection);
        }

        public Task Consume(ConsumeContext<CartCreatedEvent> context)
        {
            var message = context.Message;

            CartDetails cartDetails = new CartDetails()
            {
                CartGuid = message.CartId,
                UserGuid = message.UserId
            };

            using (var session = _sessionFactory.OpenSession())
            {
                session.Save(cartDetails);
                session.Flush();
            }
            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CartItemAddedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                CartDetails cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == message.CartId)
                    .SingleOrDefault();

                var product = session.QueryOver<ProductDetails>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();

                if (cartDetails == null || product == null)
                    return Task.CompletedTask;

                CartItem cartItem = new CartItem()
                {
                    ProductGuid = message.ProductId,
                    Quantity = message.Quantity,
                    ProductName = product.ProductName,
                    Price = product.Price
                };

                cartDetails.CartItems.Add(cartItem);

                session.SaveOrUpdate(cartDetails);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CartItemRemovedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                CartDetails cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == message.CartId)
                    .SingleOrDefault();

                if (cartDetails == null)
                    return Task.CompletedTask;

                CartItem cartItem = cartDetails.CartItems.FirstOrDefault(c => c.ProductGuid == message.ProductId);

                if (cartItem != null)
                {
                    cartDetails.CartItems.Remove(cartItem);
                }

                session.SaveOrUpdate(cartDetails);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CartItemQuantityChangedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                CartDetails cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == message.CartId)
                    .SingleOrDefault();

                if (cartDetails == null)
                    return Task.CompletedTask;

                var product = cartDetails
                    .CartItems
                    .FirstOrDefault(c => c.ProductGuid == message.ProductId);

                if(product != null)
                {
                    product.Quantity = message.Quantity;
                }

                session.SaveOrUpdate(cartDetails);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ProductPriceChangedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                var cartItems = session.QueryOver<CartItem>()
                    .Where(c => c.ProductGuid == message.ProductId)
                    .List();

                foreach(var cartItem in cartItems)
                {
                    cartItem.Price = message.TotalPrice;
                }

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CartDisabledEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                CartDetails cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == message.CartId)
                    .SingleOrDefault();

                if (cartDetails != null)
                    session.Delete(cartDetails);

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CartUserAssignedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                CartDetails cartDetails = session.QueryOver<CartDetails>()
                    .Where(c => c.CartGuid == message.CartId)
                    .SingleOrDefault();

                if (cartDetails != null)
                    cartDetails.UserGuid = message.UserId;

                session.Update(cartDetails);
                session.Flush();
            }

            return Task.CompletedTask;
        }
    }
}
