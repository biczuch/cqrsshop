﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Contracts.Order;
using NHibernate;
using QueryRepository;
using QueryRepository.Model.Order;
using QueryRepository.Model.Product;

namespace OrdersManagementReadService.EventHandlers.OrderEventHandlers
{
    class OrderListEventHandler :
        IConsumer<OrderPlacedEvent>,
        IConsumer<OrderCompletedEvent>,
        IConsumer<OrderCancelledEvent>,
        IConsumer<OrderSubmittedEvent>,
        IConsumer<OrderItemAddedEvent>
    {

        ISessionFactory _sessionFactory;

        public OrderListEventHandler()
        {
            _sessionFactory = QueryRepositorySessionFactory.GetInstance(ConfigurationReader.ReadServiceDbConnection);
        }

        public Task Consume(ConsumeContext<OrderPlacedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderListEntry order = new OrderListEntry()
                {
                    OrderGuid = message.OrderId,
                    Status = message.Status,
                    UserId = message.UserId,
                };

                session.Save(order);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderSubmittedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderListEntry order = session.QueryOver<OrderListEntry>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                order.DatePlaced = message.SubmittedDate;
                order.OrderTotalPrice = message.TotalAmount;

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderCancelledEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderListEntry order = session.QueryOver<OrderListEntry>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                order.Status = message.Status;
                order.IsCancelled = true;

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderItemAddedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderListEntry order = session.QueryOver<OrderListEntry>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                ProductListEntry product = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();


                OrderListEntryProduct orderProduct = new OrderListEntryProduct()
                {
                    ProductGuid = product.ProductGuid,
                    ProductName = product.Name,
                    Quantity = message.Quantity
                };

                order.OrderListEntryProducts.Add(orderProduct);

                session.SaveOrUpdate(order);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderCompletedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderListEntry order = session.QueryOver<OrderListEntry>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                order.Status = message.Status;
                order.DateCompleted = message.CompletedDate;

                session.Flush();
            }

            return Task.CompletedTask;
        }
    }
}
