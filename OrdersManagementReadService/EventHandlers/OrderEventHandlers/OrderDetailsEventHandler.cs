﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Contracts.Order;
using NHibernate;
using QueryRepository;
using QueryRepository.Model.Order;
using QueryRepository.Model.Product;
using System.Linq;

namespace OrdersManagementReadService.EventHandlers.OrderEventHandlers
{
    class OrderDetailsEventHandler : 
        IConsumer<OrderPlacedEvent>,
        IConsumer<OrderCompletedEvent>,
        IConsumer<OrderCancelledEvent>,
        IConsumer<OrderSubmittedEvent>,
        IConsumer<OrderItemAddedEvent>,
        IConsumer<CodesAssignedEvent>
    {

        ISessionFactory _sessionFactory;

        public OrderDetailsEventHandler()
        {
            _sessionFactory = QueryRepositorySessionFactory.GetInstance(ConfigurationReader.ReadServiceDbConnection);
        }


        public Task Consume(ConsumeContext<OrderPlacedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderDetails order = new OrderDetails()
                {
                    OrderGuid = message.OrderId,
                    Status = message.Status,
                    UserId = message.UserId,
                };

                session.Save(order);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderSubmittedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderDetails order = session.QueryOver<OrderDetails>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                order.DatePlaced = message.SubmittedDate;
                order.TotalPrice = message.TotalAmount;

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderCancelledEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderDetails order = session.QueryOver<OrderDetails>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                order.Status = message.Status;

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderItemAddedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderDetails order = session.QueryOver<OrderDetails>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                ProductListEntry product = session.QueryOver<ProductListEntry>()
                    .Where(p => p.ProductGuid == message.ProductId)
                    .SingleOrDefault();

                OrderDetailsProduct orderProduct = new OrderDetailsProduct()
                {
                    ProductGuid = product.ProductGuid,
                    ProductName = product.Name,
                    ThumbnailUri = product.ThumbnailUri
                };

                order.OrderDetailsProducts.Add(orderProduct);

                session.SaveOrUpdate(order);
                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<OrderCompletedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderDetails order = session.QueryOver<OrderDetails>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                order.Status = message.Status;
                order.DateCompleted = message.CompletedDate;

                session.Flush();
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CodesAssignedEvent> context)
        {
            var message = context.Message;

            using (var session = _sessionFactory.OpenSession())
            {
                OrderDetails order = session.QueryOver<OrderDetails>()
                    .Where(o => o.OrderGuid == message.OrderId)
                    .SingleOrDefault();

                var product = order.OrderDetailsProducts.First(o => o.ProductGuid == message.ProductId);

                product.Codes = message.Codes;

                session.SaveOrUpdate(order);
                session.Flush();
            }

            return Task.CompletedTask;
        }
    }
}
