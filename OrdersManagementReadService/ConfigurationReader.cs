﻿using System.Configuration;

namespace OrdersManagementReadService
{
    public static class ConfigurationReader
    {
        public static string ReadServiceDbConnection
        {
            get
            {
                string key = "ReadServiceDbConnection";
                if (ConfigurationManager.ConnectionStrings[key] != null)
                {
                    return ConfigurationManager.ConnectionStrings[key].ConnectionString;
                }
                throw new ConfigurationErrorsException($"{key} connectionString not provided.");

            }
        }
    }
}