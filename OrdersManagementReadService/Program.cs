﻿using GreenPipes;
using MassTransit;
using OrdersManagementReadService.EventHandlers.CartEventHandlers;
using OrdersManagementReadService.EventHandlers.OrderEventHandlers;
using OrdersManagementReadService.EventHandlers.ProductEventHandlers;
using OrdersManagementReadService.EventHandlers.ProductProductEventHandlers;
using SharedKernel;
using System;
using Topshelf;

namespace OrdersManagementReadService
{
    class Program
    {
        static int Main(string[] args)
        {
            TopshelfExitCode exitCode = HostFactory.Run(cfg => cfg.Service(x => new EventConsumerService()));
            return (int)exitCode;
        }
    }

    public class EventConsumerService : ServiceControl
    {
        IBusControl _bus;

        public bool Start(HostControl hostControl)
        {
            _bus = ConfigureBus();
            _bus.Start();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _bus?.Stop(TimeSpan.FromSeconds(30));
            return true;
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.ReceiveEndpoint(host, EndpointNames.ORDERS_MANAGMENT_READ, e =>
                {
                    e.UseConcurrencyLimit(1);
                    e.PrefetchCount = 1;

                    e.Consumer<ProductListEventHandler>();
                    e.Consumer<ProductDetailsEventHandler>();

                    e.Consumer<CartDetailsEventHandler>();

                    e.Consumer<OrderDetailsEventHandler>();
                    e.Consumer<OrderListEventHandler>();
                });
            });
        }
    }
}
