﻿using DddCommon;
using MassTransit;
using Orders.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orders.WriteSideEventRepeater
{
    class Program
    {
        public static int IDomainEvent { get; private set; }

        static void Main(string[] args)
        {
            var bus = ConfigureBus();

            var dispatcher = new WriteSideEventRepeaterEventDispatcher(bus);
            DomainEvent.Dispatcher = dispatcher;

            bus.Start();

            ProductRepository eventsRepo = new ProductRepository();

            IReadOnlyList<IDomainEvent> domainEvents = eventsRepo.GetAllEvents();
            eventsRepo.PushEventsToReadSide(domainEvents);

            bus.Stop();

        }

        static IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });
        }
    }
}
