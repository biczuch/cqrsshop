﻿using DddCommon;
using MassTransit;
using MassTransit.Util;

namespace Orders.WriteSideEventRepeater
{
    public class WriteSideEventRepeaterEventDispatcher : IEventDispatcher
    {
        private IBusControl _bus;
        public WriteSideEventRepeaterEventDispatcher(IBusControl bus)
        {
            Bus = bus;
        }

        public IBusControl Bus { get => _bus; set => _bus = value; }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            TaskUtil.Await(() => Bus.Publish(eventToDispatch, eventToDispatch.GetType()));
        }
    }
}
