﻿using DddCommon;
using MassTransit;
using MassTransit.Util;
using ProductManagement.Core.Persistance;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CqrsShop.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        static IBusControl _busControl;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var productManagementConfig = ConfigurationManager.ConnectionStrings["ProductManagement"];

            ProductManagementSessionFactory.GetInstance(productManagementConfig.ConnectionString);

            _busControl = ConfigureBus();
            _busControl.Start();

            DomainEvent.Dispatcher = new MassTransitDispatcher(_busControl);
        }

        protected void Application_End()
        {
            _busControl.Stop(TimeSpan.FromSeconds(30));
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });
        }
    }

    public class MassTransitDispatcher : IEventDispatcher
    {
        IBusControl _bus;
        public MassTransitDispatcher(IBusControl bus)
        {
            _bus = bus;
        }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            TaskUtil.Await(() => _bus.Publish(eventToDispatch, eventToDispatch.GetType()));
        }
    }
}
