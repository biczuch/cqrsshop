namespace CqrsShop.Web.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CqrsShop.Web.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CqrsShop.Web.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            
            if (!context.Users.Any(u => u.Email == "employee@test.com"))
            {
                using (var userStore = new UserStore<ApplicationUser>(context))
                using (var userManager = new UserManager<ApplicationUser>(userStore))
                {
                    const string password = "Qwerty!123";

                    var employeeUser = new ApplicationUser
                    {
                        UserName = "employee@test.com",
                        Email = "employee@test.com",
                    };
                    userManager.Create(employeeUser, password);
                }
            }
        }
    }
}
