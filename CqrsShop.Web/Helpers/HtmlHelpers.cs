﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Web.Mvc;

namespace CqrsShop.Web.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString FormatCurrency(this HtmlHelper htmlHelper, decimal value)
        {
            return new MvcHtmlString($"${value.ToString("0.00")}");
        }
        public static MvcHtmlString GetEnumDescription<T>(this HtmlHelper htmlHelper, T value) where T: struct, IConvertible
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("Argument passed to GetEnumDescription is not enum type!");

            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return new MvcHtmlString(attributes[0].Description);
            else
                return new MvcHtmlString(value.ToString());
        }
    }
}