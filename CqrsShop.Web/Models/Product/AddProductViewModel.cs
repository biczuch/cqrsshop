﻿using ProductManagement.Core;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;
using SharedKernel;

namespace CqrsShop.Web.Models.Product
{
    public class AddProductViewModel : IValidatableObject
    {
        [Required]
        [DisplayName("Product name")]
        public string ProductName { get; set; }
        
        [Required]
        [DisplayName("Sale platform")]
        public SalePlatform? SalePlatform { get; set; }

        [Required]
        [DisplayName("Game genre")]
        public GameGenre? GameGenre { get; set; }

        [Required]
        public decimal? Price { get; set; }

        public string Description { get; set; }

        [DisplayName("Short description")]
        public string ShortDescription { get; set; }

        [DisplayName("URI to images")]
        public string ImageUris { get; set; }

        public IEnumerable<string> ParseImageUris()
        {
            var uriList = new List<string>();
            return uriList;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(ImageUris != null)
            {
                var splitted = ImageUris.Split('\n');
                for (int i = 0; i < splitted.Length; i++)
                {
                    var splittedCode = splitted[i];
                    var trimmedUriString = splittedCode.Trim();

                    Uri uriResult;
                    bool isValidUri = Uri.TryCreate(trimmedUriString, UriKind.Absolute, out uriResult)
                        && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                    if (!isValidUri)
                    {
                        yield return new ValidationResult($"Invalid URI in line {i + 1}", new[] { "ImageUris" });
                    }
                }
            }
        }

        public IEnumerable<string> GetUris()
        {
            if (ImageUris != null)
            {

                return ImageUris.Split('\n');
            }
            return Enumerable.Empty<string>();
        }
    }
}