﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CqrsShop.Web.Models.Product
{
    public class ChangePriceViewModel : IValidatableObject
    {
        public ChangePriceViewModel()
        { }

        public ChangePriceViewModel(ProductManagement.Core.Product product)
        {
            ProductId = product.Id;
            CurrentMarginPercentage = product.MarginPercentage;
            CurrentTotalPrice = product.TotalPrice.Amount;
            CurrentUnitPrice = product.UnitPrice.Amount;
            CurrentMarginAmount = product.GetMarginInMoney().Amount;
        }

        public Guid ProductId { get; set; }

        [DisplayName("Total Price")]
        [DataType(DataType.Currency)]
        public decimal CurrentTotalPrice { get; set; }

        [DisplayName("Unit Price")]
        [DataType(DataType.Currency)]
        public decimal CurrentUnitPrice { get; set; }

        [DisplayName("Margin percentage")]
        public decimal CurrentMarginPercentage { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Margin amount")]
        public decimal CurrentMarginAmount { get; set; }


        [Required]
        [DisplayName("Unit Price")]
        public decimal UnitPrice { get; set; }

        [Required]
        [DisplayName("Expected income")]
        public decimal ExpectedIncome { get; set; }

        public ChangePriceOutcomeViewModel ChangePriceOutcomeViewModel { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ExpectedIncome == 0)
                yield return new ValidationResult("Expected income can't be 0.");
        }
    }
}