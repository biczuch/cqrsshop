﻿using ProductManagement.Core;
using SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CqrsShop.Web.Models.Product
{
    public class CodeViewModel
    {
        public Guid CodeId { get; set; }
        public string Code { get; set; }
        public bool AlreadySold { get; set; }
    }

    public class ProductInfoViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public SalePlatform SalePlatform { get; set; }
    }

    public class ManageCodesViewModel : IValidatableObject
    {
        public ProductInfoViewModel ProductInfo { get; set; }
        public List<CodeViewModel> Codes { get; set; } = new List<CodeViewModel>();

        [DisplayName("New codes")]
        [Required]
        public string NewCodes { get; set; }

        public ManageCodesViewModel()
        { }

        public ManageCodesViewModel(ProductManagement.Core.Product product)
        {
            Codes = product.GetCodes().Select(c => new CodeViewModel()
            {
                CodeId = c.Id,
                Code = c.Code,
                AlreadySold = c.Sold
            }).ToList();

            ProductInfo = new ProductInfoViewModel
            {
                ProductId = product.Id,
                ProductName = product.ProductName,
                SalePlatform = product.SalePlatform
            };
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var splittedCodes = NewCodes.Split('\n');
            for (int i = 0; i < splittedCodes.Length; i++)
            {
                var code = splittedCodes[i];
                if (code.Length < 10)
                    yield return new ValidationResult($"Invalid code in line { i + 1}");
            }

            yield return ValidationResult.Success;
        }
    }

}