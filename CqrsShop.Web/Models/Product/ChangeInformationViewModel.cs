﻿
using SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CqrsShop.Web.Models.Product
{
    public class ChangeInformationViewModel : IValidatableObject
    {
        public ChangeInformationViewModel()
        { }

        public ChangeInformationViewModel(ProductManagement.Core.Product product)
        {
            ProductId = product.Id;
            ProductName = product.ProductName;
            ShortDescription = product.ShortDescription;
            Description = product.Description;
            GameGenre = product.GameGenre;
            ImageUris = string.Join("\n", product.GetImagesUris());
        }

        [Required]
        public Guid ProductId { get; set; }

        [Required]
        [DisplayName("Product name")]
        public string ProductName { get; set; }
        
        [Required]
        [DisplayName("Game genre")]
        public GameGenre? GameGenre { get; set; }

        [DisplayName("URI to images")]
        public string ImageUris { get; set; }

        [DisplayName("Short description")]
        public string ShortDescription { get; set; }

        public string Description { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ImageUris != null)
            {
                var splitted = ImageUris.Split('\n');
                for (int i = 0; i < splitted.Length; i++)
                {
                    var splittedCode = splitted[i];
                    var trimmedUriString = splittedCode.Trim();

                    Uri uriResult;
                    bool isValidUri = Uri.TryCreate(trimmedUriString, UriKind.Absolute, out uriResult)
                        && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                    if (!isValidUri)
                    {
                        yield return new ValidationResult($"Invalid URI in line {i + 1}", new[] { "ImageUris" });
                    }
                }
            }
        }

        public IEnumerable<string> GetUris()
        {
            if (ImageUris != null)
            {

                return ImageUris.Split('\n');
            }
            return Enumerable.Empty<string>();
        }
    }
}