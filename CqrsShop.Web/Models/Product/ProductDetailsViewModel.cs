﻿using SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CqrsShop.Web.Models.Product
{
    public class ProductDetailsViewModel
    {
        public ProductDetailsViewModel(ProductManagement.Core.Product product)
        {
            Id = product.Id;
            AvailableAmount = product.GetAvailableAmount();
            IsActive = product.IsActive;
            Price = product.UnitPrice.Amount;
            MarginAmount = product.GetMarginInMoney().Amount;
            MarginPercentage = product.MarginPercentage * 100;
            TotalPrice = product.TotalPrice.Amount;
            ProductName = product.ProductName;
            IncomeFromItem = product.TotalPrice.Amount - product.UnitPrice.Amount;
            SalePlatform = product.SalePlatform;
            SoldAmount = product.GetSoldAmount();
            StockAmount = product.GetStockAmount();
            ImgUris = product.GetImagesUris();
            GameGenre = product.GameGenre;
            Earnings = product.GetEarnings().Amount;
        }

        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public SalePlatform SalePlatform { get; set; }
        [DataType(DataType.Currency)]
        [DisplayName("Price")]
        public decimal Price { get; set; }
        [DataType(DataType.Currency)]
        [DisplayName("Margin amount")]

        public decimal MarginAmount { get; set; }
        [DisplayName("Margin percentage")]
        public decimal MarginPercentage { get; set; }
        [DataType(DataType.Currency)]
        [DisplayName("Total price")]
        public decimal TotalPrice { get; set; }
        [DataType(DataType.Currency)]
        [DisplayName("Income from single item")]
        public decimal IncomeFromItem { get; set; }
        public bool IsActive { get; set; }
        [DisplayName("Sold Amount")]
        public int SoldAmount { get; set; }
        [DisplayName("Available Amount")]
        public int AvailableAmount { get; set; }
        [DisplayName("Stock Amount")]
        public int StockAmount { get; set; }
        public IEnumerable<string> ImgUris { get; set; }
        [DisplayName("Game genre")]
        public GameGenre GameGenre { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Earnings from sold codes")]
        public decimal Earnings { get; set; }
    }
}