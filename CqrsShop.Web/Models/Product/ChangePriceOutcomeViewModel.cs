﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CqrsShop.Web.Models.Product
{
    public class ChangePriceOutcomeViewModel
    {
        public Guid ProductId { get; set; }

        [DisplayName("Total Price")]
        [DataType(DataType.Currency)]
        public decimal OutcomeTotalPrice { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Unit Price")]
        [Required]
        public decimal OutcomeUnitPrice { get; set; }

        [DisplayName("Margin percentage")]
        [Required]
        public decimal OutcomeMarginPercentage { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Margin amount")]
        public decimal OutcomeMarginAmount { get; set; }
    }
}