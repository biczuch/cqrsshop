﻿using ProductManagement.Core;
using SharedKernel;
using System;

namespace CqrsShop.Web.Models.Product
{
    public class ProductViewModel
    {
        public ProductViewModel(ProductManagement.Core.Product product)
        {
            Id = product.Id;
            AvailableAmount = product.GetAvailableAmount();
            IsActive = product.IsActive;
            Price = product.UnitPrice.Amount;
            MarginAmount = product.GetMarginInMoney().Amount;
            MarginPercentage = product.MarginPercentage;
            TotalPrice = product.TotalPrice.Amount;
            ProductName = product.ProductName;
            IncomeFromItem = product.TotalPrice.Amount - product.UnitPrice.Amount;
            SalePlatform = product.SalePlatform;
            SoldAmount = product.GetSoldAmount();
            StockAmount = product.GetStockAmount();
        }

        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public SalePlatform SalePlatform { get; set; }
        public decimal Price { get; set; }
        public decimal MarginAmount { get; set; }
        public decimal MarginPercentage { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal IncomeFromItem { get; set; }
        public bool IsActive { get; set; }
        public int SoldAmount { get; set; }
        public int AvailableAmount { get; set; }
        public int StockAmount { get; set; }
    }
}