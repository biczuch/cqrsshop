﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CqrsShop.Web.Startup))]
namespace CqrsShop.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
