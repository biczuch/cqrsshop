﻿using CqrsShop.Web.Models.Product;
using System.Web.Mvc;
using ProductManagement.Core.Repositories;
using ProductManagement.Core;
using System.Linq;
using System.Collections.Generic;
using PagedList;
using System;
using ProductManagement.Core.Services;
using DddCommon.CommonValueObjects;

namespace CqrsShop.Web.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        ProductRepository _repository;
        ProductService _productService;

        public ProductController()
        {
            _repository = new ProductRepository();
            _productService = new ProductService();
        }

        public ActionResult Index(int? page)
        {
            IList<Product> products = _repository.GetAllProducts();
            var productsViewModel = products.Select(p => new ProductViewModel(p));
            var pageNumber = page ?? 1;
            var productsPage = productsViewModel.ToPagedList(pageNumber, 25);

            return View(productsPage);
        }

        [HttpGet]
        public ActionResult AddProduct()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProduct(AddProductViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            Product product = new Product(
                model.ProductName,
                model.SalePlatform.Value,
                new Money(model.Price.Value),
                model.GameGenre.Value,
                model.ShortDescription,
                model.Description, model.GetUris());

            _productService.AddProduct(product);

            return RedirectToAction("ProductDetails", new { productId = product.Id });
        }

        public ActionResult ProductDetails(Guid productId)
        {
            Product product = _repository.GetById(productId);

            if (product == null)
                return new HttpNotFoundResult();

            ProductDetailsViewModel productDetails = new ProductDetailsViewModel(product);

            return View(productDetails);
        }

        public ActionResult DeactivateProduct(Guid productId, string returnUrl)
        {
            Product product = _repository.GetById(productId);

            if (product == null)
                return new HttpNotFoundResult();

            product.DeactivateProduct();

            _repository.Save(product);

            return Redirect(returnUrl);
        }

        public ActionResult ActivateProduct(Guid productId, string returnUrl)
        {
            Product product = _repository.GetById(productId);

            if (product == null)
                return new HttpNotFoundResult();

            product.ActivateProduct();

            _repository.Save(product);

            return Redirect(returnUrl);
        }

        public ActionResult ChangePrice(Guid productId)
        {
            Product product = _repository.GetById(productId);
            if (product == null)
                return new HttpNotFoundResult();

            ChangePriceViewModel viewModel = new ChangePriceViewModel(product);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ChangePrice(ChangePriceOutcomeViewModel model)
        {
            Product product = _repository.GetById(model.ProductId);
            if (product == null)
                return new HttpNotFoundResult();

            var unitPrice = new Money(model.OutcomeUnitPrice);
            var marginPercentage = model.OutcomeMarginPercentage;

            product.ChangePrice(unitPrice, marginPercentage);

            _repository.Save(product);

            return RedirectToAction("ProductDetails", new { productId = model.ProductId });
        }

        public ActionResult PreviewNewPrice(ChangePriceViewModel model)
        {
            Product product = _repository.GetById(model.ProductId);
            if (product == null)
                return new HttpNotFoundResult();

            ChangePriceViewModel viewModel = new ChangePriceViewModel(product);

            var unitPrice = new Money(model.UnitPrice);
            var marginPercentage = product.GetMarginPercentageBasedOnExpectedIncome(unitPrice, new Money(model.ExpectedIncome));

            product.ChangePrice(unitPrice, marginPercentage);

            viewModel.ChangePriceOutcomeViewModel = new ChangePriceOutcomeViewModel()
            {
                OutcomeMarginAmount = product.GetMarginInMoney().Amount,
                OutcomeMarginPercentage = product.MarginPercentage,
                OutcomeTotalPrice = product.TotalPrice.Amount,
                OutcomeUnitPrice = product.UnitPrice.Amount
            };

            return View("ChangePrice", viewModel);
        }

        public ActionResult ManageCodes(Guid productId)
        {
            var product = _repository.GetById(productId);

            if (product == null)
                return new HttpNotFoundResult();

            ManageCodesViewModel viewModel = new ManageCodesViewModel(product);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddCodes(ManageCodesViewModel model)
        {
            var product = _repository.GetById(model.ProductInfo.ProductId);

            if (product == null)
                return new HttpNotFoundResult();

            if (!ModelState.IsValid)
            {
                ManageCodesViewModel viewModel = new ManageCodesViewModel(product)
                {
                    NewCodes = model.NewCodes
                };
                return View("ManageCodes", viewModel);
            }

            var newCodes = model.NewCodes
                .Split('\n')
                .Select(c => new ProductCode(c, product.SalePlatform))
                .ToArray();

                product.AddCodes(newCodes);

            _repository.Save(product);

            return RedirectToAction("ProductDetails", new { productId = model.ProductInfo.ProductId});
        }

        public ActionResult ChangeInformation(Guid productId)
        {
            var product = _repository.GetById(productId);

            if (product == null)
                return new HttpNotFoundResult();

            ChangeInformationViewModel viewModel = new ChangeInformationViewModel(product);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ChangeInformation(ChangeInformationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var product = _repository.GetById(model.ProductId);

            if (product == null)
                return new HttpNotFoundResult();

            product.ChangeInformation(model.ProductName, model.GameGenre.Value,
                model.Description, model.ShortDescription, model.GetUris().ToList());

            _repository.Save(product);

            ChangeInformationViewModel viewModel = new ChangeInformationViewModel(product);

            return RedirectToAction("ProductDetails", new { productId = model.ProductId });
        }
    }
}