﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using System.Reflection;
using FluentNHibernate.Conventions.Instances;
using System;
using NHibernate.Tool.hbm2ddl;
using NHibernate.SqlCommand;
using System.Diagnostics;

namespace Orders.Core.Persistance
{
    public static class OrdersSessionFactory
    {
        static readonly object factoryLock = new object();

        private static ISessionFactory _factory;

        public static ISessionFactory GetInstance(string connectionString = null)
        {
            if (_factory == null)
            {
                lock (factoryLock)
                {
                    if (_factory == null)
                    {
                        if (connectionString == null)
                            throw new ArgumentException("Connection string passed to GetInstance method " +
                                "should be not null when initializing SessionFactory for the first time.");
                        _factory = BuildSessionFactory(connectionString);
                    }
                }
            }

            return _factory;
        }

        private static ISessionFactory BuildSessionFactory(string connectionString)
        {
            FluentConfiguration configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(connectionString))
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())
                    .Conventions.Add(
                            ForeignKey.EndsWith("ID"),
                            ConventionBuilder.Property
                                .When(criteria => criteria.Expect(x => x.Nullable, Is.Not.Set), x => x.Not.Nullable()))
                    .Conventions.Add<TableNameConvention>()
                )
                .ExposeConfiguration(cfg =>
                {
                    cfg.EventListeners.PostCommitInsertEventListeners = 
                        new NHibernate.Event.IPostInsertEventListener[] {
                            new EventListener()
                         };
                    cfg.EventListeners.PostCommitUpdateEventListeners =
                        new NHibernate.Event.IPostUpdateEventListener[] {
                            new EventListener()
                         };
                    cfg.EventListeners.PostCommitDeleteEventListeners =
                        new NHibernate.Event.IPostDeleteEventListener[] {
                            new EventListener()
                         };
                    cfg.EventListeners.PostCollectionUpdateEventListeners =
                        new NHibernate.Event.IPostCollectionUpdateEventListener[] {
                            new EventListener()
                         };
                    //cfg.SetInterceptor(new SqlInterceptor());
                    new SchemaUpdate(cfg).Execute(false, true);
                });

            return configuration.BuildSessionFactory();
        }

        private class TableNameConvention : IClassConvention
        {
            public void Apply(IClassInstance instance)
            {
                instance.Table($"[dbo].[{instance.EntityType.Name}]");
            }
        }
    }

    public class SqlInterceptor : EmptyInterceptor
    {
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            Trace.WriteLine(sql.ToString());
            return sql;
        }
    }
}
