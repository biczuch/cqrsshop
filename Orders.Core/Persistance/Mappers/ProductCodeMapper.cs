﻿using FluentNHibernate.Mapping;
using Orders.Core.Model;

namespace Orders.Core.Persistance.Mappers
{
    class ProductCodeMapper : ClassMap<ProductCode>
    {
        public ProductCodeMapper()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Code);
            Map(x => x.Status);
        }
    }
}
