﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Orders.Core.Model;

namespace Orders.Core.Persistance.Mappers
{
    class OrderMapper : ClassMap<Order>
    {
        public OrderMapper()
        {
            Id(x => x.Id).GeneratedBy.Assigned();

            Map(x => x.IsActive);
            Map(x => x.IsPaid);
            Map(x => x.IsPlaced);
            Map(x => x.IsSubmitted);
            Map(x => x.Status);
            Map(x => x.SubmittedDate);
            Map(x => x.UserId);

            HasMany<OrderItem>(Reveal.Member<Order>("OrderItems"))
                .Cascade.All()
                .Not.LazyLoad();
        }
    }
}
