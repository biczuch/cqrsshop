﻿using FluentNHibernate.Mapping;
using Orders.Core.Model;

namespace Orders.Core.Persistance.Mappers
{
    class CartItemMapper : ClassMap<CartItem>
    {
        public CartItemMapper()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.ProductId);
            Map(x => x.Quantity);
        }
    }
}
