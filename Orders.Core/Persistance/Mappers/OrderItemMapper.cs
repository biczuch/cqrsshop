﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Orders.Core.Model;

namespace Orders.Core.Persistance.Mappers
{
    class OrderItemMapper : ClassMap<OrderItem>
    {
        public OrderItemMapper()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.ProductId);

            Map(x => x.Quantity);
            Map(x => x.UnitPrice);
            Map(x => x.MarginPercentage);

            HasMany<string>(Reveal.Member<OrderItem>("Codes"))
                .KeyColumn("CodeId")
                .Table("OrderItemCodes")
                .Element("Code", x => x.Length(4001))
                .Not.LazyLoad();
        }
    }
}
