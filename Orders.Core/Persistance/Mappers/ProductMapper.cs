﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Orders.Core.Model;
using SharedKernel;

namespace Orders.Core.Persistance.Mappers
{
    class ProductMapper : ClassMap<Product>
    {
        public ProductMapper()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.ProductName);
            Map(x => x.IsActive);

            Map(x => x.ShortDescription)
                .Length(4001)
                .Nullable();

            Map(x => x.Description)
                .Length(4001)
                .Nullable();

            Map(x => x.GameGenre)
                .CustomType(typeof(GameGenre))
                .Not.Nullable();

            Map(x => x.SalePlatform)
                .CustomType(typeof(SalePlatform))
                .Not.Nullable();


            Component(x => x.UnitPrice, y =>
            {
                y.Map(x => x.Amount);
                y.Map(x => x.Currency);
            });
            Map(x => x.MarginPercentage);

            HasMany<string>(Reveal.Member<Product>("ImageUris"))
                .KeyColumn("ImageId")
                .Table("ImageUri")
                .Element("Uri", x => x.Length(4001))
                .Not.LazyLoad();

            HasMany<ProductCode>(x => x.ProductCodes)
                .Cascade.All()
                .Not.LazyLoad();
        }
    }
}
