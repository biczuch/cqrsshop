﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Orders.Core.Model;

namespace Orders.Core.Persistance.Mappers
{
    class CartMapper : ClassMap<Cart>
    {
        public CartMapper()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.UserId);

            HasMany<CartItem>(Reveal.Member<Cart>("CartItems"))
                .Cascade.All()
                .Not.LazyLoad();
        }
    }
}
