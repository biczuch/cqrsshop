﻿using DddCommon;
using DddCommon.CommonValueObjects;
using Orders.Contracts.Product;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orders.Core
{
    public partial class Product
    {
        protected override void ApplyEventChange(IDomainEvent @event)
        {
            Dictionary<Type, Action> eventHandlers = new Dictionary<Type, Action>()
            {
                { typeof(ProductCreatedEvent), () => ApplyProductCreated(@event as ProductCreatedEvent)},
                { typeof(ProductCodeAddedEvent), () => ApplyProductCodeAdded(@event as ProductCodeAddedEvent)},
                { typeof(ProductActivatedEvent), () => ApplyProductActivated(@event as ProductActivatedEvent)},
                { typeof(ProductDeactivatedEvent), () => ApplyProductDeactivated(@event as ProductDeactivatedEvent)},
                { typeof(ProductPriceChangedEvent), () => ApplyProductPriceChanged(@event as ProductPriceChangedEvent)},
                { typeof(ProductInformationChangedEvent), () => ApplyProductInformationChanged(@event as ProductInformationChangedEvent) },
                { typeof(ProductCodesReservedEvent), () => ApplyProductCodesReserved(@event as ProductCodesReservedEvent) },
                { typeof(ProductCodesUnreservedEvent), () => ApplyProductUnreserved(@event as ProductCodesUnreservedEvent) },
                { typeof(ProductCodeSoldEvent), () => ApplyProductSoldEvent(@event as ProductCodeSoldEvent) }
            };

            Action eventHandler = eventHandlers[@event.GetType()];
            eventHandler.Invoke();
        }

        private void ApplyProductCreated(ProductCreatedEvent @event)
        {
            Id = @event.ProductId;
            ProductName = @event.ProductName;
            SalePlatform = @event.SalePlatform;
            Description = @event.Description;
            ShortDescription = @event.ShortDescription;
            UnitPrice = new Money(@event.UnitPrice);
            ImageUris = new List<string>(@event.ImageUris);
            GameGenre = @event.GameGenre;
            MarginPercentage = @event.MarginPercentage;
        }

        private void ApplyProductCodeAdded(ProductCodeAddedEvent @event)
        {
            var newCodes = @event.ProductCodes
                .Select(c => new Model.ProductCode(c.CodeId, c.Code))
                .ToArray();

            this.AddNewCodes(newCodes);
        }

        private void ApplyProductDeactivated(ProductDeactivatedEvent @event)
        {
            DeactivateProduct();
        }

        private void ApplyProductActivated(ProductActivatedEvent @event)
        {
            ActivateProduct();
        }

        private void ApplyProductPriceChanged(ProductPriceChangedEvent @event)
        {
            this.ChangePrice(new Money(@event.ProductPrice), @event.MarginPercentage);
        }

        private void ApplyProductInformationChanged(ProductInformationChangedEvent @event)
        {
            this.ChangeInformation(
                @event.ProductName,
                @event.GameGenre,
                @event.Description,
                @event.ShortDescription,
                @event.ImageUris);
        }

        private void ApplyProductCodesReserved(ProductCodesReservedEvent @event)
        {
            ReserveCodes(@event.Quantity);
        }

        private void ApplyProductUnreserved(ProductCodesUnreservedEvent @event)
        {
            UnreserveCodes(@event.Quantity);
        }

        private void ApplyProductSoldEvent(ProductCodeSoldEvent @event)
        {
            this.ChangeCodeStatusToSold(@event.CodeId, @event.SoldPrice, @event.MarginPercentage);
        }
    }
}
