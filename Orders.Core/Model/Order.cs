﻿using System;
using System.Collections.Generic;
using DddCommon;
using System.Linq;
using Orders.Contracts.Order;
using Orders.Common;

namespace Orders.Core.Model
{
    public partial class Order : EsDddCommon.AggregateRoot
    {
        protected virtual IList<OrderItem> OrderItems { get; set; } = new List<OrderItem>();
        public virtual bool IsPlaced { get; protected set; }
        public virtual bool IsPaid { get; protected set; }
        public virtual OrderStatus Status { get; protected set; }
        public virtual Guid UserId { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual DateTime SubmittedDate { get; protected set; }
        public virtual DateTime CancelledDate { get; protected set; }
        public virtual DateTime CompletedDate { get; protected set; }

        public virtual bool IsSubmitted { get; protected set; }

        public virtual IReadOnlyCollection<OrderItem> Items
        {
            get
            {
                return OrderItems.ToArray();
            }
        }

        protected Order()
        {
            Status = OrderStatus.Pending;
        }

        public Order(Guid userId) : this()
        {
            UserId = userId;

            AddDomainEvent(new OrderPlacedEvent
            {
                OrderId = Id,
                OrderStatus = Status,
                UserId = UserId,
                IsActive = IsActive,
                Status = Status.ToString()
            });
        }

        public Order(IEnumerable<IDomainEvent> eventStream) : base(eventStream)
        { }

        public virtual void AddOrderItem(Guid productId, decimal unitPrice, decimal marginPercentage, int quantity)
        {
            OrderItem orderItem = new OrderItem(productId, unitPrice, marginPercentage, quantity);
            OrderItems.Add(orderItem);

            AddDomainEvent(new OrderItemAddedEvent()
            {
                OrderId = Id,
                ProductId = orderItem.ProductId,
                Quantity = orderItem.Quantity,
                UnitPrice = orderItem.UnitPrice,
                MarginPercentage = orderItem.MarginPercentage
            });
        }

        public virtual void SubmitOrder()
        {
            if(IsSubmitted == true)
            {
                throw new InvalidOperationException("Order is already submitted");
            }

            IsSubmitted = true;
            SubmittedDate = DateTime.Now;
            Status = OrderStatus.Submitted;

            AddDomainEvent(new OrderSubmittedEvent() {
                OrderId = Id,
                TotalAmount = GetTotalAmount(),
                SubmittedDate = SubmittedDate
            });
        }

        public virtual decimal GetTotalAmount()
        {
            return OrderItems.Sum(i => i.GetOrderItemTotalAmount());
        }

        public virtual void Complete()
        {
            this.IsPaid = true;
            CompletedDate = DateTime.Now;
            Status = OrderStatus.Completed;

            AddDomainEvent(new OrderCompletedEvent()
            {
                OrderId = Id,
                CompletedDate = CompletedDate,
                Status = Status.ToString()
            });
        }

        public virtual void AssignCodesForProduct(Guid productId, string[] codes)
        {
            if(this.IsPaid == false)
            {
                throw new InvalidOperationException("Can't assign codes to incomplete order");
            }

            var item = Items.First(i => i.ProductId == productId);

            item.AssignCodes(codes);

            AddDomainEvent(new CodesAssignedEvent() {
                OrderId = Id,
                ProductId = productId,
                Codes = codes
            });
        }

        public virtual void Cancel()
        {
            this.CancelledDate = DateTime.Now;
            Status = OrderStatus.Cancelled;

            AddDomainEvent(new OrderCancelledEvent()
            {
                OrderId = Id,
                CancelledDate = CancelledDate,
                Status = Status.ToString()
            });
        }
    }
}
