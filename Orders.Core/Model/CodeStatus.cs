﻿namespace Orders.Core.Model
{
    public enum CodeStatus
    {
        Available,
        Reserved,
        Sold
    }
}
