﻿using DddCommon;
using Orders.Contracts.Cart;
using System;
using System.Collections.Generic;

namespace Orders.Core.Model
{
    partial class Cart
    {
        protected override void ApplyEventChange(IDomainEvent @event)
        {
            Dictionary<Type, Action> eventHandlers = new Dictionary<Type, Action>()
            {
                { typeof(CartCreatedEvent), () => ApplyCartCreated(@event as CartCreatedEvent)},
                { typeof(CartItemAddedEvent), () => ApplyCartItemAdded(@event as CartItemAddedEvent)},
                { typeof(CartItemRemovedEvent), () => ApplyCartItemRemoved(@event as CartItemRemovedEvent)},
                { typeof(CartItemQuantityChangedEvent), () => ApplyCartItemQuantityChanged(@event as CartItemQuantityChangedEvent)},
                { typeof(CartDisabledEvent), () => ApplyCartDisabled(@event as CartDisabledEvent)},
                { typeof(CartUserAssignedEvent), () => ApplyCartUserAssigned(@event as CartUserAssignedEvent)},
            };

            Action eventHandler = eventHandlers[@event.GetType()];
            eventHandler.Invoke();
            this.ClearEvents();
        }

        private void ApplyCartDisabled(CartDisabledEvent cartItemsClearedEvent)
        {
            this.CartDisable();
        }

        private void ApplyCartCreated(CartCreatedEvent cartCreatedEvent)
        {
            Id = cartCreatedEvent.CartId;
            UserId = cartCreatedEvent.UserId;
        }

        private void ApplyCartItemAdded(CartItemAddedEvent cartItemAddedEvent)
        {
            AddCartItem(cartItemAddedEvent.ProductId, cartItemAddedEvent.Quantity);
        }

        private void ApplyCartItemRemoved(CartItemRemovedEvent cartItemRemovedEvent)
        {
            RemoveCartItem(cartItemRemovedEvent.ProductId);
        }

        private void ApplyCartItemQuantityChanged(CartItemQuantityChangedEvent cartItemQuantityChangedEvent)
        {
            ChangeQuantityOfItemInCart(cartItemQuantityChangedEvent.ProductId, cartItemQuantityChangedEvent.Quantity);
        }

        private void ApplyCartUserAssigned(CartUserAssignedEvent cartUserAssignedEvent)
        {
            AssignToUser(cartUserAssignedEvent.UserId);
        }
    }
}