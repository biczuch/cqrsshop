﻿using DddCommon;
using System;

namespace Orders.Core.Model
{
    public class CartItem : Entity
    {
        public virtual Guid ProductId { get; protected set; }
        public virtual int Quantity { get; protected set; }

        protected CartItem()
        {

        }

        public CartItem(Guid productId, int quantity)
        {
            if(quantity <= 0)
            {
                throw new ArgumentException("Product's quantity can't be negative or zero.");
            }

            ProductId = productId;
            Quantity = quantity;
        }

        public virtual void ChangeQuantity(int newItemQuantity)
        {
            if (Quantity <= 0)
            {
                throw new ArgumentException("Product's quantity can't be negative or zero.");
            }

            Quantity = newItemQuantity;
        }
    }
}
