﻿using DddCommon;
using Orders.Contracts.Order;
using System;
using System.Collections.Generic;

namespace Orders.Core.Model
{
    public partial class Order
    {
        protected override void ApplyEventChange(IDomainEvent @event)
        {
            Dictionary<Type, Action> eventHandlers = new Dictionary<Type, Action>()
            {
                { typeof(OrderPlacedEvent), () => ApplyOrderPlaced(@event as OrderPlacedEvent)},
                { typeof(OrderItemAddedEvent), () => ApplyOrderItemAdded(@event as OrderItemAddedEvent)},
                { typeof(OrderSubmittedEvent), () => ApplyOrderSubmitted(@event as OrderSubmittedEvent)},
                { typeof(OrderCompletedEvent), () => ApplyOrderCompleted(@event as OrderCompletedEvent)},
                { typeof(OrderCancelledEvent), () => ApplyOrderCancelled(@event as OrderCancelledEvent)}
            };

            Action eventHandler = eventHandlers[@event.GetType()];
            eventHandler.Invoke();
        }

        private void ApplyOrderCompleted(OrderCompletedEvent orderCompletedEvent)
        {
            Complete();
            CompletedDate = orderCompletedEvent.CompletedDate;
        }

        private void ApplyOrderPlaced(OrderPlacedEvent orderCreatedEvent)
        {
            Id = orderCreatedEvent.OrderId;
            Status = orderCreatedEvent.OrderStatus;
        }

        private void ApplyOrderItemAdded(OrderItemAddedEvent orderItemAddedEvent)
        {
            AddOrderItem(orderItemAddedEvent.ProductId,
                orderItemAddedEvent.UnitPrice,
                orderItemAddedEvent.MarginPercentage,
                orderItemAddedEvent.Quantity);
        }

        private void ApplyOrderSubmitted(OrderSubmittedEvent orderSubmittedEvent)
        {
            SubmitOrder();
            SubmittedDate = orderSubmittedEvent.SubmittedDate;
        }

        private void ApplyOrderCancelled(OrderCancelledEvent cancelledEvent)
        {
            Cancel();
            CancelledDate = cancelledEvent.CancelledDate;
        }
    }
}
