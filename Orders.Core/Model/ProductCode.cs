﻿using DddCommon;
using System;

namespace Orders.Core.Model
{
    public class ProductCode : Entity, IRootAware
    {
        public virtual string Code { get; protected set; }
        public virtual CodeStatus Status { get; protected set; }
        public virtual Entity RootEntity { get; set; }

        protected ProductCode()
        {
            Status = CodeStatus.Available;
        }

        public ProductCode(Guid guid, string code)
        {
            this.Id = guid;
            this.Code = code;
        }

        public virtual void Reserve()
        {
            Status = CodeStatus.Reserved;
        }

        public virtual void MakeAvailable()
        {
            Status = CodeStatus.Available;
        }

        public virtual void MarkAsSold()
        {
            Status = CodeStatus.Sold;
        }

        public virtual void Unreserve()
        {
            Status = CodeStatus.Available;
        }
    }
}
