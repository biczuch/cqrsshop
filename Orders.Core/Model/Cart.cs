﻿using DddCommon;
using Orders.Contracts.Cart;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orders.Core.Model
{
    public partial class Cart : EsDddCommon.AggregateRoot
    {
        protected virtual IList<CartItem> CartItems { get; set; } = new List<CartItem>();
        public virtual Guid UserId { get; protected set; }


        public Cart(IEnumerable<IDomainEvent> eventStream) : base(eventStream)
        {

        }

        public virtual IReadOnlyCollection<CartItem> Items
        {
            get
            {
                return CartItems.ToArray();
            }
        }

        protected Cart()
        {
            CartItems = new List<CartItem>();
        }

        public Cart(Guid userGuid) : this()
        {
            UserId = userGuid;

            AddDomainEvent(new CartCreatedEvent()
            {
                CartId = Id,
                UserId = userGuid
            });
        }

        public virtual void AssignToUser(Guid userId)
        {
            UserId = userId;

            AddDomainEvent(new CartUserAssignedEvent()
            {
                CartId = Id,
                UserId = UserId
            });
        }

        public virtual void AddCartItem(Guid productId, int quantity = 1)
        {
            if (quantity <= 0)
                return;

            CartItem cartItem = CartItems.FirstOrDefault(c => c.ProductId == productId);

            if(cartItem != null)
            {
                ChangeQuantityOfItemInCart(cartItem.ProductId, cartItem.Quantity + quantity);
            }
            else
            {
                cartItem = new CartItem(productId, quantity);
                CartItems.Add(cartItem);

                CartItemAddedEvent @event = new CartItemAddedEvent()
                {
                    CartId = Id,
                    ProductId = productId,
                    Quantity = quantity
                };
                AddDomainEvent(@event);
            }
        }

        public virtual void ChangeQuantityOfItemInCart(Guid productId, int quantity)
        {
            var cartItem = CartItems.FirstOrDefault(i => i.ProductId == productId);
            if (cartItem == null)
                throw new ArgumentException("ProductGuid not found in CartItems collection");

            if (quantity == 0)
            {
                RemoveCartItem(productId);
                return;
            }
                
            cartItem.ChangeQuantity(quantity);

            CartItemQuantityChangedEvent @event = new CartItemQuantityChangedEvent()
            {
                CartId = this.Id,
                ProductId = productId,
                Quantity = quantity
            };

            this.AddDomainEvent(@event);
        }

        public virtual void RemoveCartItem(Guid productId)
        {
            var cartItem = CartItems.FirstOrDefault(i => i.ProductId == productId);

            if (cartItem == null)
                throw new ArgumentException("ProductGuid not found in CartItems collection");

            CartItems.Remove(cartItem);

            CartItemRemovedEvent @event = new CartItemRemovedEvent()
            {
                CartId = this.Id,
                ProductId = productId
            };

            AddDomainEvent(@event);

            if(!CartItems.Any())
            {
                CartDisable();
            }
        }

        public virtual void CartDisable()
        {
            CartItems.Clear();

            AddDomainEvent(new CartDisabledEvent()
            {
                CartId = Id
            });
        }
    }
}
