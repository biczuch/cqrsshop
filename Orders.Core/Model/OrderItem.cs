﻿using System;
using DddCommon;
using System.Collections.Generic;

namespace Orders.Core.Model
{
    public class OrderItem : Entity
    {
        public virtual Guid ProductId { get; protected set; }
        public virtual decimal UnitPrice { get; protected set; }
        public virtual decimal MarginPercentage { get; protected set; }
        public virtual int Quantity { get; protected set; }
        public virtual IList<string> Codes { get; protected set; } = new List<string>();

        protected OrderItem()
        { }

        public OrderItem(Guid productId, decimal unitPrice, decimal marginPercentage, int quantity)
        {
            if (quantity <= 0)
            {
                throw new ArgumentException("Product's quantity can't be negative or zero.");
            }

            if (unitPrice < 0)
            {
                throw new ArgumentException("Product's unit price can't be negative.");
            }

            ProductId = productId;
            Quantity = quantity;
            UnitPrice = unitPrice;
            MarginPercentage = marginPercentage;
        }

        public virtual decimal GetOrderItemTotalAmount()
        {
            return Math.Round(UnitPrice + (UnitPrice * MarginPercentage), 2, MidpointRounding.AwayFromZero) * Quantity;
        }

        public virtual void ChangePrice(decimal newUnitPrice)
        {
            if (newUnitPrice < 0)
            {
                throw new ArgumentException("Product's unit price can't be negative.");
            }
            UnitPrice = newUnitPrice;
        }

        public virtual void AssignCodes(string[] codes)
        {
            if(codes != null)
            {
                this.Codes = codes;
            }
        }
    }
}
