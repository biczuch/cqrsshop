﻿using System;
using System.Linq;
using DddCommon.CommonValueObjects;
using Orders.Contracts.Product;
using DddCommon;
using System.Collections.Generic;
using Orders.Core.Model;
using SharedKernel;

namespace Orders.Core
{
    public partial class Product : EsDddCommon.AggregateRoot
    {
        public virtual string ProductName { get; protected set; }
        public virtual SalePlatform SalePlatform { get; protected set; }
        public virtual GameGenre GameGenre { get; protected set; }
        public virtual string ShortDescription { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual Money UnitPrice { get; protected set; }
        public virtual decimal MarginPercentage { get; protected set; }

        public virtual bool IsActive { get; protected set; }

        public virtual ICollection<string> ImageUris { get; protected set; } = new List<string>();
        public virtual ICollection<Model.ProductCode> ProductCodes { get; protected set; } = new List<Model.ProductCode>();

        protected Product()
        {

        }

        public Product(IEnumerable<IDomainEvent> eventStream) : base(eventStream)
        {

        }

        public Product(Guid productId, string productName, SalePlatform salePlatform,
            string shortDescription, string description, decimal unitPrice, decimal marginPercentage,
            IEnumerable<string> imageUris, GameGenre gameGenre, bool isActive)
        {
            Id = productId;
            ProductName = productName;
            SalePlatform = salePlatform;
            ShortDescription = shortDescription;
            Description = description;
            UnitPrice = new Money(unitPrice);
            MarginPercentage = marginPercentage;
            GameGenre = gameGenre;
            IsActive = isActive;

            foreach (var imageUri in imageUris)
            {
                ImageUris.Add(imageUri);
            }

            AddDomainEvent(new ProductCreatedEvent()
            {
                ProductId = Id,
                Description = Description,
                ProductName = ProductName,
                ShortDescription = ShortDescription,
                SalePlatform = SalePlatform,
                UnitPrice = UnitPrice.Amount,
                MarginPercentage = MarginPercentage,
                ImageUris = ImageUris,
                GameGenre = GameGenre,
                IsActive = IsActive,
                TotalAmount = GetTotalAmount()
            });
        }

        public virtual decimal GetTotalAmount()
        {
            return UnitPrice.Amount + UnitPrice.Amount * MarginPercentage;
        }

        public virtual void ChangeCodeStatusToSold(Guid id, decimal soldUnitPrice, decimal soldMarginPercentage)
        {
            var code = ProductCodes.First(c => c.Id == id);
            code.MarkAsSold();
            code.RootEntity = this;

            AddDomainEvent(new ProductCodeSoldEvent() {
                ProductId = Id,
                CodeId = code.Id,
                SoldPrice = soldUnitPrice,
                MarginPercentage = soldMarginPercentage
            });
        }

        public virtual void ActivateProduct()
        {
            if (IsActive == true)
                throw new InvalidOperationException("Product is already active.");

            IsActive = true;

            AddDomainEvent(new ProductActivatedEvent()
            {
                ProductId = Id
            });
        }

        public virtual void DeactivateProduct()
        {
            if (IsActive == false)
                throw new InvalidOperationException("Product is already inactive.");

            IsActive = false;

            AddDomainEvent(new ProductDeactivatedEvent()
            {
                ProductId = Id
            });
        }

        public virtual void AddNewCodes(Model.ProductCode[] newCodes)
        {
            foreach(Model.ProductCode productCode in newCodes)
            {
                ProductCodes.Add(productCode);
            }

            AddDomainEvent(new ProductCodeAddedEvent()
            {
                ProductId = Id,
                ProductCodes = ProductCodes.Select(c => new Contracts.Product.ProductCode()
                {
                    Code = c.Code,
                    CodeId = c.Id
                }).ToArray()
            });
        }

        public virtual void ChangePrice(Money price, decimal marginPercentage)
        {
            this.UnitPrice = price;
            this.MarginPercentage = marginPercentage;

            AddDomainEvent(new ProductPriceChangedEvent()
            {
                ProductId = this.Id,
                ProductPrice = UnitPrice.Amount,
                MarginPercentage = MarginPercentage,
                TotalPrice = GetTotalAmount()
            });
        }

        public virtual void ChangeInformation(string productName, GameGenre gameGenre, string description, string shortDescription, IEnumerable<string> imageUris)
        {
            ProductName = productName;
            GameGenre = gameGenre;
            Description = description;
            ShortDescription = shortDescription;
            ImageUris.Clear();
            foreach(var imgUri in imageUris)
            {
                ImageUris.Add(imgUri);
            }

            this.AddDomainEvent(new ProductInformationChangedEvent()
            {
                ProductId = this.Id,
                ProductName = productName,
                GameGenre = gameGenre,
                Description = Description,
                ShortDescription = ShortDescription,
                ImageUris = ImageUris
            });
        }

        public virtual void ReserveCodes(int quantity)
        {
            if (quantity <= 0)
            {
                throw new ArgumentException("Can't reserve 0 or less codes.");
            }
            var availableCodes = ProductCodes.Count(i => i.Status == CodeStatus.Available);

            if (!IsActive || availableCodes < quantity)
            {
                throw new InvalidOperationException("Couldn't create order. " +
                    "Product is inactive or not enough codes available");
            }

            var productsToReserve = ProductCodes.Where(i => i.Status == CodeStatus.Available).Take(quantity);
            foreach (Model.ProductCode code in productsToReserve)
            {
                code.Reserve();
            }

            AddDomainEvent(new ProductCodesReservedEvent()
            {
                Quantity = quantity
            });
        }

        public virtual void UnreserveCodes(int quantity)
        {
            if (quantity <= 0)
            {
                throw new ArgumentException("Can't unreserve 0 or less codes.");
            }

            var availableCodes = ProductCodes.Count(i => i.Status == CodeStatus.Reserved);

            if (!IsActive || availableCodes < quantity)
            {
                throw new InvalidOperationException("Couldn't create order. " +
                    "Product is inactive or not enough reserved codes");
            }

            var productsToReserve = ProductCodes.Where(i => i.Status == CodeStatus.Reserved).Take(quantity);
            foreach (Model.ProductCode code in productsToReserve)
            {
                code.Unreserve();
            }

            AddDomainEvent(new ProductCodesUnreservedEvent()
            {
                Quantity = quantity
            });
        }
    }
}
