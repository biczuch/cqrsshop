﻿using Orders.Core.Model;
using Orders.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orders.Core.Services
{
    public class OrderService
    {
        private CartRepository _cartRepository = new CartRepository();
        private ProductRepository _productRepository = new ProductRepository();
        private OrderRepository _orderRepository = new OrderRepository();

        public void CompleteOrder(Guid orderId)
        {
            Order order = _orderRepository.GetById(orderId);
            order.Complete();

            List<Product> updatedProducts = new List<Product>();

            foreach(var orderItem in order.Items)
            {
                var product = _productRepository.GetById(orderItem.ProductId);

                List<ProductCode> codes = product.ProductCodes
                    .Where(c => c.Status == CodeStatus.Reserved)
                    .Take(orderItem.Quantity)
                    .ToList();

                order.AssignCodesForProduct(product.Id, codes.Select(i => i.Code).ToArray());

                foreach(var code in codes)
                {
                    product.ChangeCodeStatusToSold(code.Id, orderItem.UnitPrice, orderItem.MarginPercentage);
                }

                updatedProducts.Add(product);
            }

            _orderRepository.Save(order);

            foreach (var product in updatedProducts)
            {
                _productRepository.Save(product);
            }
        }

        public void CancelOrder(Guid orderId)
        {
            Order order = _orderRepository.GetById(orderId);

            order.Cancel();

            var productsToUpdate = new List<Product>();

            foreach(var orderItem in order.Items)
            {
                Product product = _productRepository.GetById(orderItem.ProductId);
                product.UnreserveCodes(orderItem.Quantity);
                productsToUpdate.Add(product);
            }

            _orderRepository.Save(order);
            foreach(Product p in productsToUpdate)
            {
                _productRepository.Save(p);
            }
        }

        public void CheckoutCart(Guid cartId)
        {
            Cart cart = _cartRepository.GetById(cartId);

            Order order = new Order(cart.UserId);

            List<Product> updatedProducts = new List<Product>();

            foreach (CartItem cartItem in cart.Items)
            {
                Product product = _productRepository.GetById(cartItem.ProductId);
                product.ReserveCodes(cartItem.Quantity);
                order.AddOrderItem(product.Id, product.UnitPrice.Amount, product.MarginPercentage, cartItem.Quantity);
                updatedProducts.Add(product);
            };

            cart.CartDisable();

            order.SubmitOrder();

            _cartRepository.Save(cart);

            _orderRepository.Save(order);
            foreach (Product p in updatedProducts)
            {
                _productRepository.Save(p);
            }
        }
    }
}
