﻿using Orders.Core.Model;
using Orders.Core.Repositories;
using System;

namespace Orders.Core.Services
{
    public class CartService
    {
        ProductRepository _productRepository = new ProductRepository();
        CartRepository _cartRepository = new CartRepository();

        public Cart CreateCart(Guid userId)
        {
            Cart cart = new Cart(userId);

            return cart;
        }

        public void SwapCarts(Guid cartIdToRemove, Guid cartIdToAssign, Guid newUserId)
        {
            Cart cartToRemove = null;
            if (cartIdToRemove != Guid.Empty)
            {
                cartToRemove = _cartRepository.GetById(cartIdToRemove);
                cartToRemove.CartDisable();
            }

            var cartToAssignUserTo = _cartRepository.GetById(cartIdToAssign);
            cartToAssignUserTo.AssignToUser(newUserId);

            if (cartToRemove != null)
                _cartRepository.Save(cartToRemove);

            _cartRepository.Save(cartToAssignUserTo);            
        }

        public void DisableCart(Guid guid)
        {
            Cart cart = _cartRepository.GetById(guid);
            cart.CartDisable();
            _cartRepository.Save(cart);
        }

        public void RemoveItemFromCart(Guid cartId, Guid productId)
        {
            var cart = _cartRepository.GetById(cartId);
            cart.RemoveCartItem(productId);
            _cartRepository.Save(cart);
        }

        public void AddProductToCart(Guid cartId, Guid userId, Guid productId, int quantity)
        {
            Cart cart = null;

            if (cartId != Guid.Empty)
            {
                cart = _cartRepository.GetById(cartId);

                if (cart == null)
                    throw new InvalidOperationException("Cart not found.");
            }

            if (cart == null)
            {
                cart = CreateCart(userId);
            }

            var product = _productRepository.GetById(productId);

            if (product == null)
                throw new InvalidOperationException("Product does not exist and can't be added to cart.");

            cart.AddCartItem(product.Id, quantity);

            _cartRepository.Save(cart);
        }

        public void ChangeProductQuantityInCart(Guid cartId, Guid productId, int quantity)
        {
            var product = _productRepository.GetById(productId);

            Cart cart = _cartRepository.GetById(cartId);

            if (cart == null)
                throw new InvalidOperationException("Cart not found.");

            cart.ChangeQuantityOfItemInCart(product.Id, quantity);

            _cartRepository.Save(cart);
        }

        public void RemoveItemsFromCart(Guid cartId)
        {
            Cart cart = _cartRepository.GetById(cartId);

            if (cart == null)
                throw new InvalidOperationException("Cart not found.");

            cart.CartDisable();

            _cartRepository.Save(cart);
        }
    }
}
