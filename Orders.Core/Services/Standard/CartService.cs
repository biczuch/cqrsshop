﻿using NHibernate;
using Orders.Core.Model;
using Orders.Core.StandardRepositories;
using System;
using System.Transactions;

namespace Orders.Core.Services.Standard
{
    public class CartService
    {
        ProductRepository _productRepository = new ProductRepository();
        CartRepository _cartRepository = new CartRepository();

        public Cart CreateCart(Guid userId)
        {
            Cart cart = new Cart(userId);

            return cart;
        }

        public void SwapCarts(Guid cartIdToRemove, Guid cartIdToAssign, Guid newUserId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _cartRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Cart cartToRemove = null;
                    if (cartIdToRemove != Guid.Empty)
                    {
                        cartToRemove = _cartRepository.GetByIdManually(cartIdToRemove, session);
                        cartToRemove.CartDisable();
                    }

                    var cartToAssignUserTo = _cartRepository.GetByIdManually(cartIdToAssign, session);
                    cartToAssignUserTo.AssignToUser(newUserId);

                    if (cartToRemove != null)
                        _cartRepository.SaveManually(cartToRemove, session);

                    _cartRepository.SaveManually(cartToAssignUserTo, session);

                    transaction.Commit();
                }
                transactionScope.Complete();
            }
        }

        public void DisableCart(Guid guid)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _cartRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Cart cart = _cartRepository.GetByIdManually(guid, session);
                    cart.CartDisable();
                    _cartRepository.SaveManually(cart, session);

                    transaction.Commit();
                }
                transactionScope.Complete();
            }
        }

        public void RemoveItemFromCart(Guid cartId, Guid productId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _cartRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    var cart = _cartRepository.GetByIdManually(cartId, session);
                    cart.RemoveCartItem(productId);
                    _cartRepository.SaveManually(cart, session);

                    transaction.Commit();
                }
                transactionScope.Complete();
            }
        }

        public void AddProductToCart(Guid cartId, Guid userId, Guid productId, int quantity)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _cartRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Cart cart = null;

                    if (cartId != Guid.Empty)
                    {
                        cart = _cartRepository.GetByIdManually(cartId, session);

                        if (cart == null)
                            throw new InvalidOperationException("Cart not found.");
                    }

                    if (cart == null)
                    {
                        cart = CreateCart(userId);
                    }

                    var product = _productRepository.GetByIdManually(productId, session);

                    if (product == null)
                        throw new InvalidOperationException("Product does not exist and can't be added to cart.");

                    cart.AddCartItem(product.Id, quantity);

                    _cartRepository.SaveManually(cart, session);

                    transaction.Commit();
                }
                transactionScope.Complete();
            }
        }

        public void ChangeProductQuantityInCart(Guid cartId, Guid productId, int quantity)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _cartRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    var product = _productRepository.GetByIdManually(productId, session);

                    Cart cart = _cartRepository.GetByIdManually(cartId, session);

                    if (cart == null)
                        throw new InvalidOperationException("Cart not found.");

                    cart.ChangeQuantityOfItemInCart(product.Id, quantity);

                    _cartRepository.SaveManually(cart, session);

                    transaction.Commit();
                }
                transactionScope.Complete();
            }
        }

        public void RemoveItemsFromCart(Guid cartId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _cartRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Cart cart = _cartRepository.GetByIdManually(cartId, session);

                    if (cart == null)
                        throw new InvalidOperationException("Cart not found.");

                    cart.CartDisable();

                    _cartRepository.SaveManually(cart, session);

                    transaction.Commit();
                }
                transactionScope.Complete();
            }
        }
    }
}
