﻿using NHibernate;
using Orders.Core.Model;
using Orders.Core.StandardRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Orders.Core.Services.Standard
{
    public class OrderService
    {
        private CartRepository _cartRepository = new CartRepository();
        private ProductRepository _productRepository = new ProductRepository();
        private OrderRepository _orderRepository = new OrderRepository();

        public void CompleteOrder(Guid orderId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _orderRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Order order = _orderRepository.GetByIdManually(orderId, session);
                    order.Complete();

                    List<Product> updatedProducts = new List<Product>();

                    foreach (var orderItem in order.Items)
                    {
                        var product = _productRepository.GetByIdManually(orderItem.ProductId, session);

                        List<ProductCode> codes = product.ProductCodes
                            .Where(c => c.Status == CodeStatus.Reserved)
                            .Take(orderItem.Quantity)
                            .ToList();

                        order.AssignCodesForProduct(product.Id, codes.Select(i => i.Code).ToArray());

                        foreach (var code in codes)
                        {
                            product.ChangeCodeStatusToSold(code.Id, orderItem.UnitPrice, orderItem.MarginPercentage);
                        }

                        updatedProducts.Add(product);
                    }

                    _orderRepository.SaveManually(order, session);

                    foreach (var product in updatedProducts)
                    {
                        _productRepository.SaveManually(product, session);
                    }

                    transaction.Commit();
                }

                transactionScope.Complete();
            }

        }

        public void CancelOrder(Guid orderId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _orderRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Order order = _orderRepository.GetByIdManually(orderId, session);

                    order.Cancel();

                    var productsToUpdate = new List<Product>();

                    foreach (var orderItem in order.Items)
                    {
                        Product product = _productRepository.GetByIdManually(orderItem.ProductId, session);
                        product.UnreserveCodes(orderItem.Quantity);
                        productsToUpdate.Add(product);
                    }

                    _orderRepository.SaveManually(order, session);
                    foreach (Product product in productsToUpdate)
                    {
                        _productRepository.SaveManually(product, session);
                    }

                    transaction.Commit();
                }

                transactionScope.Complete();
            }
        }

        public void CheckoutCart(Guid cartId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                using (ISession session = _orderRepository.GetOpenedSession())
                using (var transaction = session.BeginTransaction())
                {
                    Cart cart = _cartRepository.GetByIdManually(cartId, session);

                    Order order = new Order(cart.UserId);

                    List<Product> updatedProducts = new List<Product>();

                    foreach (CartItem cartItem in cart.Items)
                    {
                        Product product = _productRepository.GetByIdManually(cartItem.ProductId, session);
                        product.ReserveCodes(cartItem.Quantity);
                        order.AddOrderItem(product.Id, product.UnitPrice.Amount, product.MarginPercentage, cartItem.Quantity);
                        updatedProducts.Add(product);
                    };

                    cart.CartDisable();

                    order.SubmitOrder();

                    _cartRepository.SaveManually(cart, session);

                    _orderRepository.SaveManually(order, session);
                    foreach (Product p in updatedProducts)
                    {
                        _productRepository.SaveManually(p, session);
                    }

                    transaction.Commit();
                }

                transactionScope.Complete();
            }
        }
    }
}
