﻿using Orders.Core.Model;
using DddCommon;
using NHibernate;
using Orders.Core.Persistance;

namespace Orders.Core.StandardRepositories
{
    public class OrderRepository : Repository<Order>
    {
        protected override ISessionFactory GetSessionFactory()
        {
            return OrdersSessionFactory.GetInstance();
        }
    }
}
