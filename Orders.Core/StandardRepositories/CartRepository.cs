﻿using Orders.Core.Model;
using DddCommon;
using NHibernate;
using Orders.Core.Persistance;

namespace Orders.Core.StandardRepositories
{
    public class CartRepository : Repository<Cart>
    {
        protected override ISessionFactory GetSessionFactory()
        {
            return OrdersSessionFactory.GetInstance();
        }
    }
}
