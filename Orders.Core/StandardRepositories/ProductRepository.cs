﻿using DddCommon;
using NHibernate;
using Orders.Core.Persistance;

namespace Orders.Core.StandardRepositories
{
    public class ProductRepository : Repository<Product>
    {
        protected override ISessionFactory GetSessionFactory()
        {
            return OrdersSessionFactory.GetInstance();
        }
    }
}
