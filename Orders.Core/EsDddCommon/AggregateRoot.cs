﻿using DddCommon;
using System.Collections.Generic;

namespace Orders.Core.EsDddCommon
{
    public abstract class AggregateRoot : DddCommon.AggregateRoot
    {
        protected AggregateRoot() : base()
        {

        }

        protected AggregateRoot(IEnumerable<IDomainEvent> eventStream)
        {
            foreach(var @event in eventStream)
            {
                ApplyEventChange(@event);
                Version++;
            }

            ClearEvents();
        }

        public virtual int Version { get; set; } = 0;

        protected abstract void ApplyEventChange(IDomainEvent @event);
    }
}
