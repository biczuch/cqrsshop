﻿using Orders.Core.Model;
using System;
using System.Collections.Generic;
using DddCommon;

namespace Orders.Core.Repositories
{
    public class OrderRepository : EventsRepository<Order>
    {
        protected override Order CreateNewObjectFromEvents(IEnumerable<IDomainEvent> eventStream)
        {
            return new Order(eventStream);
        }


    }
}
