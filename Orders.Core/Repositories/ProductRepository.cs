﻿using System.Collections.Generic;
using DddCommon;

namespace Orders.Core.Repositories
{
    public class ProductRepository : EventsRepository<Product>
    {
        protected override Product CreateNewObjectFromEvents(IEnumerable<IDomainEvent> eventStream)
        {
            return new Product(eventStream);
        }
    }
}
