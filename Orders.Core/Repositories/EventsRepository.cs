﻿using System;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Linq;
using DddCommon;
using System.Collections.Generic;

namespace Orders.Core.Repositories
{
    public abstract class EventsRepository<T> : IRepository<T>
        where T : EsDddCommon.AggregateRoot
    {
        protected readonly string connectionString;
        protected JsonSerializerSettings jsonSettings;


        public EventsRepository()
        {
            connectionString = ConfigurationManager.ConnectionStrings["OrdersES"].ConnectionString;
            jsonSettings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };
        }

        public T GetById(Guid guid)
        {
            var eventStream = new List<IDomainEvent>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("GetEventsForAggregate"))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;

                    command.Parameters.AddWithValue("@aggregateGuid", guid);

                    var dataReader = command.ExecuteReader();

                    if(dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var x = (string)dataReader["EventData"];
                            var @event = JsonConvert.DeserializeObject<IDomainEvent>(x, jsonSettings);
                            eventStream.Add(@event);
                        }
                    }

                    dataReader.Close();
                }
            }

            return CreateNewObjectFromEvents(eventStream);
        }

        public IReadOnlyList<IDomainEvent> GetAllEvents()
        {
            var eventStream = new List<IDomainEvent>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("GetAllEvents"))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;

                    var dataReader = command.ExecuteReader();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var x = (string)dataReader["EventData"];
                            var @event = JsonConvert.DeserializeObject<IDomainEvent>(x, jsonSettings);
                            eventStream.Add(@event);
                        }
                    }

                    dataReader.Close();
                }
            }

            return eventStream;
        }

        protected abstract T CreateNewObjectFromEvents(IEnumerable<IDomainEvent> eventStream);

        public void Save(T aggregateRoot)
        {
            var events = aggregateRoot.DomainEvents;

            var serializedEvents = aggregateRoot.DomainEvents
                .Select(e => JsonConvert.SerializeObject(e, jsonSettings))
                .ToArray();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("InsertEventsForAggregate", connection))
                using (var table = new DataTable())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;

                    table.Columns.Add("eventData", typeof(string));
                    foreach (var serializedEvent in serializedEvents)
                    {
                        table.Rows.Add(serializedEvent);
                    }

                    var eventsListParam = new SqlParameter("@events", SqlDbType.Structured)
                    {
                        TypeName = "dbo.EventList",
                        Value = table
                    };
                    command.Parameters.AddWithValue("@aggregateGuid", aggregateRoot.Id);
                    command.Parameters.AddWithValue("@aggregateType", typeof(T).Name);
                    command.Parameters.AddWithValue("@expectedVersion", aggregateRoot.Version);
                    command.Parameters.Add(eventsListParam);
                    
                    var changedrows = command.ExecuteNonQuery();
                }
            }
            PushEventsToReadSide(events);
        }

        public void PushEventsToReadSide(IReadOnlyList<IDomainEvent> events)
        {
            foreach(var @event in events)
            {
                DomainEvent.Dispatcher.Dispatch(@event);
            }
        }
    }
}
