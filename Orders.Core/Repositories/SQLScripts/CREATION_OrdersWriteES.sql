CREATE TABLE Aggregates(
	AggregateGuid UniqueIdentifier PRIMARY KEY,
	Version int,
	Type varchar(100)
)

CREATE INDEX AggregatesGuid
ON Aggregates(AggregateGuid)

CREATE TABLE Events(
	AggregateGuid UniqueIdentifier FOREIGN KEY REFERENCES Aggregates(AggregateGuid),
	Data varchar(max),
	Version int,
	DateProcessed Date
)

CREATE INDEX AggregatesGuid
ON Events(AggregateGuid)

CREATE TABLE Snapshots(
	AggregateGuid UniqueIdentifier FOREIGN KEY REFERENCES Aggregates(AggregateGuid),
	Version int,
	Data Varchar(max)
)

CREATE INDEX AggregatesGuid
ON Snapshots(AggregateGuid)