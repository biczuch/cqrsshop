CREATE PROCEDURE GetEventsForAggregate
	@aggregateGuid uniqueidentifier
AS
BEGIN

	SELECT Data as EventData FROM Events
	WHERE AggregateGuid = @aggregateGuid
	ORDER BY Version asc
	
END