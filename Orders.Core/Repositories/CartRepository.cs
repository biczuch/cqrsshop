﻿using Orders.Core.Model;
using System.Collections.Generic;
using DddCommon;

namespace Orders.Core.Repositories
{
    public class CartRepository : EventsRepository<Cart>
    {
        protected override Cart CreateNewObjectFromEvents(IEnumerable<IDomainEvent> eventStream)
        {
            return new Cart(eventStream);
        }
    }
}
