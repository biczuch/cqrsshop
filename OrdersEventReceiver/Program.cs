﻿using DddCommon;
using GreenPipes;
using MassTransit;
using Orders.Core.Persistance;
using OrdersEventReceiver.EventHandlers.OrdersEventHandlers;
using OrdersEventReceiver.EventHandlers.ProductEventHandlers;
using SharedKernel;
using System;
using System.Configuration;
using Topshelf;
using MassTransit.NLogIntegration;
using NLog.Config;
using NLog.Targets;
using NLog;

namespace OrdersEventReceiver
{
    class Program
    {
        static int Main(string[] args)
        {
            TopshelfExitCode exitCode = HostFactory.Run(cfg => cfg.Service(x => new EventConsumerService()));
            return (int)exitCode;
        }
    }

    public class EventConsumerService : ServiceControl
    {
        IBusControl _bus;

        public bool Start(HostControl hostControl)
        {
            var cqrsShopStandard = ConfigurationManager.ConnectionStrings["CqrsShopStandard"];

            OrdersSessionFactory.GetInstance(cqrsShopStandard.ConnectionString);

            _bus = ConfigureBus();

            _bus.Start();
            DomainEvent.Dispatcher = new OrdersManagementWriteServiceEventDispatcher(_bus);
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _bus?.Stop(TimeSpan.FromSeconds(30));
            return true;
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.ReceiveEndpoint(host, EndpointNames.STANDARD_ARCHITECTURE_EVENT_RECEIVER, e =>
                {
                    e.UseConcurrencyLimit(1);
                    e.PrefetchCount = 1;

                    e.Consumer<NewCodeAddedToProductEventHandler>();
                    e.Consumer<ProductActivatedEventHandler>();
                    e.Consumer<ProductAddedEventHandler>();
                    e.Consumer<ProductDeactivatedEventHandler>();
                    e.Consumer<ProductInformationChangedEventHandler>();
                    e.Consumer<ProductPriceChangedEventHandler>();

                    e.Consumer<OrderFullyPaidEventHandler>();
                    e.Consumer<OrderExpiredEventHandler>();
                });

                LoggingConfiguration logConfig = new LoggingConfiguration();
                var fileTarget = new FileTarget()
                {
                    FileName = @"C:\CqrsShopLog-OrdersEventReceiver.log"
                };
                logConfig.AddTarget("fileTarget", fileTarget);

                var rule = new LoggingRule("*", LogLevel.Debug, fileTarget);
                logConfig.LoggingRules.Add(rule);

                LogFactory logFactory = new LogFactory(logConfig);

                cfg.UseNLog(logFactory);
            });
        }
    }
}
