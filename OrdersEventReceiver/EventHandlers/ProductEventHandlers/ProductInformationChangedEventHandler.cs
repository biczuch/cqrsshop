﻿using System.Threading.Tasks;
using ProductManagement.Contracts.Product;
using Orders.Core.StandardRepositories;
using MassTransit;
using System.Transactions;
using NHibernate;

namespace OrdersEventReceiver.EventHandlers.ProductEventHandlers
{
    public class ProductInformationChangedEventHandler : IConsumer<ProductInformationChangedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductInformationChangedEvent> context)
        {
            var message = context.Message;

            using (TransactionScope scope = new TransactionScope())
            {
                ISession session = productRepository.GetOpenedSession();
                using (var transaction = session.BeginTransaction())
                {
                    var product = productRepository.GetByIdManually(message.ProductId, session);
                    product.ChangeInformation(
                        message.ProductName,
                        message.GameGenre,
                        message.Description,
                        message.ShortDescription,
                        message.ImageUris
                        );

                    productRepository.SaveManually(product, session);

                    transaction.Commit();
                }    

                scope.Complete();
            }

            return Task.CompletedTask;
        }
    }
}