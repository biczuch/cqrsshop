﻿using MassTransit;
using NHibernate;
using Orders.Core.StandardRepositories;
using ProductManagement.Contracts.Product;
using System.Threading.Tasks;
using System.Transactions;

namespace OrdersEventReceiver.EventHandlers.ProductEventHandlers
{
    public class ProductActivatedEventHandler : IConsumer<ProductActivatedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductActivatedEvent> context)
        {
            var message = context.Message;

            using (TransactionScope scope = new TransactionScope())
            {
                ISession session = productRepository.GetOpenedSession();
                using (var transaction = session.BeginTransaction())
                {
                    var product = productRepository.GetByIdManually(message.ProductId, session);
                    product.ActivateProduct();

                    productRepository.SaveManually(product, session);
                    transaction.Commit();
                }
                scope.Complete();
            }
            
            return Task.CompletedTask;
        }
    }
}