﻿using System.Threading.Tasks;
using ProductManagement.Contracts.Product;
using Orders.Core.StandardRepositories;
using DddCommon.CommonValueObjects;
using MassTransit;
using System.Transactions;
using NHibernate;

namespace OrdersEventReceiver.EventHandlers.ProductEventHandlers
{
    public class ProductPriceChangedEventHandler : IConsumer<ProductPriceChangedEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductPriceChangedEvent> context)
        {
            var message = context.Message;

            using (TransactionScope scope = new TransactionScope())
            {
                ISession session = productRepository.GetOpenedSession();

                using (var transaction = session.BeginTransaction())
                {
                    var product = productRepository.GetByIdManually(message.ProductId, session);
                    product.ChangePrice(new Money(message.UnitPrice), message.MarginPercentage);

                    productRepository.SaveManually(product, session);

                    transaction.Commit();
                }

                scope.Complete();
            }

            return Task.CompletedTask;
        }
    }
}