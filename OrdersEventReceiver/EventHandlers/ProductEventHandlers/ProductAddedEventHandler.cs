﻿using System.Threading.Tasks;
using ProductManagement.Contracts.Product;
using Orders.Core;
using Orders.Core.StandardRepositories;
using MassTransit;
using System;
using System.Transactions;

namespace OrdersEventReceiver.EventHandlers.ProductEventHandlers
{
    public class ProductAddedEventHandler : IConsumer<ProductAddedEvent>
    {
        private ProductRepository _productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<ProductAddedEvent> context)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                var message = context.Message;

                Product product = new Product(
                    message.ProductId,
                    message.ProductName,
                    message.SalePlatform,
                    message.ShortDescription,
                    message.Description,
                    message.UnitPrice,
                    message.MarginPercentage,
                    message.ImageUris,
                    message.GameGenre,
                    message.IsActive
                    );

                _productRepository.Save(product);

                scope.Complete();
            }

            return Task.CompletedTask;
        }
    }
}