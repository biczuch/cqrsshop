﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Core;
using Orders.Core.StandardRepositories;
using ProductManagement.Contracts.Product;
using System.Transactions;
using NHibernate;
using System.Linq;
using Orders.Core.Model;

namespace OrdersEventReceiver.EventHandlers.ProductEventHandlers
{
    public class NewCodeAddedToProductEventHandler : IConsumer<NewCodeAddedToProductEvent>
    {
        ProductRepository productRepository = new ProductRepository();

        public Task Consume(ConsumeContext<NewCodeAddedToProductEvent> context)
        {
            var message = context.Message;

            using (TransactionScope scope = new TransactionScope())
            {
                ISession session = productRepository.GetOpenedSession();
                using (var transaction = session.BeginTransaction())
                {
                    Product product = productRepository.GetByIdManually(message.ProductId, session);
                    var newCodes = message.NewCodes.Select(c => new ProductCode(c.CodeId, c.Code)).ToArray();
                    

                    product.AddNewCodes(newCodes);

                    productRepository.SaveManually(product, session);
                    transaction.Commit();
                }

                scope.Complete();
            }

            return Task.CompletedTask;
        }
    }
}