﻿using System.Threading.Tasks;
using MassTransit;
using Orders.Core.Services.Standard;
using Orders.Contracts.Order;

namespace OrdersEventReceiver.EventHandlers.OrdersEventHandlers
{
    public class OrderFullyPaidEventHandler : IConsumer<OrderFullyPaidEvent>
    {
        OrderService _orderService = new OrderService();

        public Task Consume(ConsumeContext<OrderFullyPaidEvent> context)
        {
            _orderService.CompleteOrder(context.Message.OrderId);

            return Task.CompletedTask;
        }
    }
}
