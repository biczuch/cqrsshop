﻿using DddCommon;
using MassTransit;
using MassTransit.Util;

namespace OrdersEventReceiver
{
    internal class OrdersManagementWriteServiceEventDispatcher : IEventDispatcher
    {
        private IBusControl _bus;

        public OrdersManagementWriteServiceEventDispatcher(IBusControl bus)
        {
            _bus = bus;
        }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            TaskUtil.Await(() => _bus.Publish(eventToDispatch, eventToDispatch.GetType()));
        }
    }
}